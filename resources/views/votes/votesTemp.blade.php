@extends('layouts.app', ['page' => 'info'])

@section('content')
    <div class="container container-form">
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("$election->name"),
                    'icon' => "file-text-o"
                ])
            </div>
        </div>

        <form
            method="POST"
            action="{{ route('votes.store',['election_id'=>$election_id,'elector_id'=>$elector_id, 'token_mail'=>$token_mail] )  }}"
            enctype="multipart/form-data"
        >
            @csrf



            <div class="form-group">
                <label for="document_number">Ingrese su documento de identidad para verificar su voto.</label>
                <input
                    type="text"
                    class="form-control document-number-input-vote {{ $errors->has('document_number') ? 'is-invalid': '' }}"
                    name="document_number"
                    id="document_number"
                    placeholder="Ingrese su documento"
                    value="{{ old('document_number') ?: $election->document_number }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('document_number'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('document_number') }}</strong>
                    </span>
                @endif
            </div>
            <div class="questions-wrap mt-4">
                <div class="questions-inn">
                    @forelse($election->questions as $question)
                        <div class="question question-exists question-{{ $question->id }}" data-id="{{ $question->id }}">
                            <div class="question-name-wrap">
                                <div class="question-number d-inline">{{ $question->order }}.</div>
                                <div class="question-name d-inline"> {{ $question->name }}</div>
                            </div>
                            <div class="candidates-wrap">
                                <div class="candidates-inn">
                                    @php $temp_candidate = 0; @endphp
                                    @forelse($question->candidates as $candidate)
                                        <div class="candidate candidate-exists candidate-{{ $candidate->id }}" data-id="{{ $candidate->id }}">
                                            <div class="candidate-number d-inline">
                                                <input
                                                    type="radio"
                                                    class="form-check-input"
                                                    name="question[{{ $question->id }}]"
                                                    value="{{ $candidate->id }}"
                                                    id="candidate_{{ $candidate->id }}"
                                                    placeholder="Agregue un candidato"
                                                    required
                                                    autocomplete="off"
                                                ></div>
                                            <div class="candidate-name d-inline">
                                                <label class="form-check-label" for="candidate_{{ $candidate->id }}">
                                                    @if($candidate->list_number < 250)
                                                        Lista {{ $candidate->list_number }} - Partido {{ $candidate->list_name }} - {{ $candidate->name }}
                                                    @else
                                                        {{ $candidate->name }}
                                                    @endif
                                                </label>
                                            </div>
                                        </div>

                                    @empty

                                    @endforelse


                                </div>
                            </div>
                        </div>
                    @empty

                    @endforelse
                </div>
            </div>

            <div class="row text-center d-block mt-5 mb-4">
                <button
                    class="btn btn-outline-info"
                    type="submit"
                >
                    VOTAR
                </button>
            </div>

        </form>

    </div>
@endsection

@push('scripts')
    <script>


    </script>
@endpush
