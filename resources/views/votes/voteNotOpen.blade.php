@extends('layouts.app', ['page' => 'info'])

@section('content')
    <div class="container container-form">
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("$poll->name"),
                    'icon' => "file-text-o"
                ])
            </div>
        </div>

        <div>
            <h4 class="vno-info">AVISO: La votación no está abierta</h4>
            <h4 class="vno-schedule">Horario de votación</h4>
            <ul class="text-center list-unstyled">
                <li><b>Abre:</b> A las {{ \Carbon\Carbon::parse($poll->start_date )->format('H:i')}} del
                    {{ \Carbon\Carbon::parse($poll->start_date )->format('d/m/Y')}}</li>
                <li><b>Cierra:</b> A las {{ \Carbon\Carbon::parse($poll->end_date )->format('H:i')}} del
                    {{ \Carbon\Carbon::parse($poll->end_date )->format('d/m/Y')}}</li>
            </ul>
        </div>

    </div>
@endsection

@push('scripts')
    <script>


    </script>
@endpush
