@extends('layouts.app', ['page' => 'info'])

@section('content')
    <div class="container container-form">
        <div class="alert alert-warning alert-vote-time">
            <span class="fa fa-info-circle"></span>
            Tiempo restante para emitir su voto: <div class="countdown"></div>
        </div>
        <div class="title-poll-votes-wrapp">
            <h1 class="title-poll-votes">
                {{ $poll->name }}
            </h1>
        </div>


        <form
            method="POST"
            action="{{ route('votes.store',['poll_id_elector_id_token_mail'=>$poll_id_elector_id_token_mail] )  }}"
            enctype="multipart/form-data"
        >
            @csrf

            <div class="form-group d-none">
                <label for="document_number">Documento de identidad</label>
                <input
                    type="text"
                    class="form-control document-number-input-vote {{ $errors->has('document_number') ? 'is-invalid': '' }}"
                    name="document_number"
                    id="document_number"
                    placeholder="Ingrese su documento"
                    value="{{ $document_number }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('document_number'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('document_number') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group d-none">
                <label for="device_uuid">Elector identifier</label>
                <input
                    type="text"
                    class="form-control document-number-input-vote {{ $errors->has('document_number') ? 'is-invalid': '' }}"
                    name="device_uuid"
                    id="device_uuid"
                    placeholder="Ingrese su documento"
                    value=""
                    required
                    autocomplete="off"
                >
                @if($errors->has('device_uuid'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('device_uuid') }}</strong>
                    </span>
                @endif
            </div>
            @foreach($elections as $election)

            <h2 class="text-center title-election-votes">{{ $election->name }}</h2>
            <div class="questions-wrap mt-4">
                <div class="questions-inn">
                    @forelse($election->questions as $question)
                        <div class="row mt-3 mb-3 question question-{{ $question->id }}">
                            <div class="col-12 flex-column align-items-center d-flex mb-4">
                                <h4 class="title-election-results">{{ $question->order }}. {{ $question->name }}</h4>
                                <table class="table table-votaciones table-results-elections table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="col-order text-center">Marcar</th>
                                        <th class="col-order">Lista N°</th>
                                        <th class="col-candidate">Partido</th>
                                        <th class="col-candidate">Candidato</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($question->candidatesOrdered() as $candidate)
                                        <tr class="candidate">
                                            <td class="list-{{ $candidate->list_number }} text-center">
                                                <input
                                                    type="radio"
                                                    class=""
                                                    name="question[{{ $question->id }}]"
                                                    value="{{ $candidate->id }}"
                                                    id="candidate_{{ $candidate->id }}"
                                                    placeholder="Agregue un candidato"
                                                    required
                                                    autocomplete="off"
                                                >
                                            </td>
                                            <td class="col-order list-{{ $candidate->list_number }}">
                                                <label class="form-check-label label-vote" for="candidate_{{ $candidate->id }}">
                                                    @if($candidate->list_number < 250)
                                                        {{ $candidate->list_number }}
                                                    @else
                                                        {{ $candidate->name }}
                                                    @endif
                                                </label>
                                            </td>
                                            <td class="col-candidate list-{{ $candidate->list_number }}">
                                                <label class="form-check-label label-vote" for="candidate_{{ $candidate->id }}">
                                                    @if($candidate->list_number < 250)
                                                        {{ $candidate->list_name }}
                                                    @else
                                                        {{ $candidate->name }}
                                                    @endif
                                                </label>

                                            </td>
                                            <td class="col-candidate">
                                                <label class="form-check-label label-vote" for="candidate_{{ $candidate->id }}">
                                                    @if($candidate->list_number < 250)
                                                        {{ $candidate->name }}
                                                    @else
                                                        {{ $candidate->name }}
                                                    @endif
                                                </label>


                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td>{{ __("Aún no hay candidatos")}}</td>
                                        </tr>
                                    @endforelse

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    @empty

                    @endforelse
                </div>
            </div>
            @endforeach

            @if($poll->auth_factors == \App\Poll::TWO_FACTOR)

            <div class="form-group text-center">
                <label for="token_cellphone">Código de votación enviado a su celular</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('token_cellphone') ? 'is-invalid': '' }} document-number-input-vote "
                    name="token_cellphone"
                    id="token_cellphone"
                    placeholder="Ingrese el código"
                    value="{{ old('token_cellphone') ?: '' }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('token_cellphone'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('token_cellphone') }}</strong>
                    </span>
                @endif
            </div>
            @endif


            <div class="row text-center d-block mt-5 mb-4">
                <button
                    class="btn btn-outline-info"
                    type="submit"
                >
                    VOTAR
                </button>
            </div>

        </form>


    </div>
@endsection

@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/device-uuid.js') }}"></script>
    <script>
        var expiration_date = {{ $secondsToCloseSession }};
        //var expiration_date = 5;

        printTimeLeft(expiration_date);

        function printTimeLeft(seconds_left) {
            let minutes = Math.floor(seconds_left / 60);
            let seconds = Math.floor(seconds_left % 60);

            let minutes_formatted = pad(minutes, 2);
            let seconds_formatted = pad(seconds, 2);
            $('.countdown').html(minutes_formatted + ':' + seconds_formatted);
            --seconds_left;

            if (seconds_left >= 0){
                setTimeout(function() { printTimeLeft(seconds_left); }, 1000);
            }else{
                $("form, .head-page").remove();
                $(".alert").html("Su sesión expiró, por favor ingrese de nuevo a la votación mediante el link del correo.");
            }

        }

        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }

        $(function() {
            var uuid = new DeviceUUID().get();
            console.log(uuid);
            $("#device_uuid").val(uuid);

        });
    </script>
@endpush
