@extends('layouts.app', ['page' => 'info'])

@section('content')
    <div class="container container-form">
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => \App\Poll::find(isset($poll_id_decrypted) ? $poll_id_decrypted:$poll_id)->name,
                    'icon' => "file-text-o"
                ])
            </div>
        </div>

        <div>
            <h4 class="text-center">AVISO: {{ $message }}</h4>
        </div>

    </div>
@endsection

@push('scripts')
    <script>


    </script>
@endpush
