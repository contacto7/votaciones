@extends('layouts.app', ['page' => 'info'])

@section('content')
    <div class="container container-form">
        <div class="head-vote">
            <img
                src="{{ asset('image/logo.svg') }}"
                alt="Logo de Thales"
                class="logo-vote logo-sistema">
            <div class="title-row">
                <div class="col-md-12">
                    <h1 class="votaciones-vote-title">
                        SISTEMA DE VOTACIONES
                    </h1>
                </div>
            </div>
        </div>

            <form
                method="POST"
                action="{{ route('votes.checkDni',['poll_id_elector_id_token_mail'=>$poll_id_elector_id_token_mail] )  }}"
                enctype="multipart/form-data"
            >
                @csrf



                <div class="form-group text-center">
                    <label for="document_number">Validación de identidad</label>
                    <div class="identity-validation-row">
                        <input
                            type="text"
                            class="form-control document-number-input-vote {{ $errors->has('document_number') ? 'is-invalid': '' }}"
                            name="document_number"
                            id="document_number"
                            placeholder="N° de documento"
                            value="{{ old('document_number') ?: '' }}"
                            required
                            autocomplete="off"
                        >
                        @if($auth_factor == \App\Poll::RENIEC_VALIDATION)
                            <span> - </span>
                            <input
                                type="text"
                                class="form-control verification-digit-input-vote {{ $errors->has('document_number') ? 'is-invalid': '' }}"
                                name="verification_digit"
                                id="verification_digit"
                                placeholder="Dígito verificador"
                                value="{{ old('verification_digit') ?: '' }}"
                                required
                                autocomplete="off"
                            >
                        @endif
                        @if($errors->has('document_number'))
                            <span class="invalid-feedback">
                        <strong>{{ $errors->first('document_number') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group d-none">
                    <label for="device_uuid">Elector identifier</label>
                    <input
                        type="text"
                        class="form-control document-number-input-vote {{ $errors->has('document_number') ? 'is-invalid': '' }}"
                        name="device_uuid"
                        id="device_uuid"
                        placeholder="Ingrese su documento"
                        value=""
                        required
                        autocomplete="off"
                    >
                    @if($errors->has('device_uuid'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('device_uuid') }}</strong>
                    </span>
                    @endif
                </div>


                <div class="row text-center d-block mt-5 mb-4">
                    @if($secondsToVoteAgain > 0)
                        <div class="message-error-validating">Podrá intentar de nuevo en <span class="countdown">{{ $secondsToVoteAgain }}</span></div>
                        <button
                            class="btn btn-outline-secondary btn-acces-voting"
                            type="submit"
                            disabled
                            style="cursor: not-allowed;"
                        >
                            Ingresar a votación
                        </button>

                    @else
                        <button
                            class="btn btn-outline-info btn-acces-voting"
                            type="submit"
                        >
                            Ingresar a votación
                        </button>

                    @endif
                </div>

            </form>


    </div>
@endsection

@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/device-uuid.js') }}"></script>
    <script>
        $(function() {
            var secondsToVoteAgain = {{ $secondsToVoteAgain }};
            //var expiration_date = 5;

            if(secondsToVoteAgain > 0){
                printTimeLeft(secondsToVoteAgain);
            }

            function printTimeLeft(seconds_left) {
                let minutes = Math.floor(seconds_left / 60);
                let seconds = Math.floor(seconds_left % 60);

                let minutes_formatted = pad(minutes, 2);
                let seconds_formatted = pad(seconds, 2);
                $('.countdown').html(minutes_formatted + ':' + seconds_formatted);
                --seconds_left;

                if (seconds_left >= 0){
                    setTimeout(function() { printTimeLeft(seconds_left); }, 1000);
                }else{
                    $(".message-error-validating").remove();
                    $(".btn-acces-voting")
                        .removeClass("btn-outline-secondary")
                        .addClass("btn-outline-info")
                        .css("cursor","pointer")
                        .prop("disabled", false);
                }

            }

            function pad(n, width, z) {
                z = z || '0';
                n = n + '';
                return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
            }
            var uuid = new DeviceUUID().get();
            console.log(uuid);
            $("#device_uuid").val(uuid);

        });

    </script>
@endpush
