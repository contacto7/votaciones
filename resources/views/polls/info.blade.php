@extends('layouts.app', ['page' => 'polls'])

@section('content')
    <div class="container container-form">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            <div class="head-page text-center">
                @include('partials.title', [
                    'title' => __(" $poll->name"),
                    'icon' => "file-text-o"
                ])
                <div class="btn-group" role="group" aria-label="Basic example">
                    @can('viewResults', [\App\Poll::class])
                    <a
                        class="btn btn-outline-info"
                        href="{{ route('polls.results', $poll->id) }}"
                    >
                        Ver resultados
                    </a>
                    <a
                        class="btn btn-outline-info"
                        href=" {{ action('PollController@resultsPDF', $poll->id)}}"
                    >
                        Descargar resultados
                    </a>
                    <a
                        class="btn btn-outline-info testing-button"
                        href=" {{ action('PollController@pollElectorsPDF', $poll->id)}}"
                    >
                        Descargar electores
                    </a>
                    @endcan
                </div>
            </div>
        </div>

        <div class="row mt-3 mb-3">
            <div class="p-3">
                Grupo de votaciones iniciadas a las <b>{{ \Carbon\Carbon::parse($poll->start_date )->format('H:i')}}h
                    del
                {{ \Carbon\Carbon::parse($poll->start_date )->format('d/ m/ Y')}}</b> hasta las
                <b>{{ \Carbon\Carbon::parse($poll->end_date )->format('H:i')}}h del
                {{ \Carbon\Carbon::parse($poll->end_date )->format('d/ m/ Y')}}</b>. Creadas por
                <b>{{ $poll->user->name." ".$poll->user->last_name }}</b> a las <b>{{ \Carbon\Carbon::parse($poll->created_at )->format('H:i')}}h
                    del
                {{ \Carbon\Carbon::parse($poll->created_at )->format('d/ m/ Y')}}.</b>

                <div>
                    @if($poll->auth_factors == \App\Poll::ONE_FACTOR)

                    @elseif($poll->auth_factors == \App\Poll::TWO_FACTOR)
                        Doble factor de autenticación activado.
                    @elseif($poll->auth_factors == \App\Poll::RENIEC_VALIDATION)
                        Validación con dígito verificador activada.
                    @endif
                </div>
                <div>
                    Link de resultados públicos:
                    <a
                        href="{{ route('polls.publicResults', ['slug'=>$poll->slug]) }}"
                    >
                        {{ route('polls.publicResults', ['slug'=>$poll->slug]) }}</a>
                </div>
            </div>
        </div>


        <div class="row mt-3 mb-3">
            <div class="col-12">

                <ul class="nav nav-tabs nav-justified" id="pills-tab" role="tablist">
                    @can('viewElections', [\App\Poll::class])
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#pills-elections">Lista de votaciones</a>
                    </li>
                    @endcan
                    @can('viewElectors', [\App\Poll::class])
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#pills-electors">Lista de electores</a>
                    </li>
                    @endcan
                    @can('viewVotes', [\App\Poll::class])
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#pills-votes">Votos procesados</a>
                    </li>
                    @endcan
                    @can('viewEmailsSent', [\App\Poll::class])
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#pills-emails">Detalle de correos</a>
                    </li>
                    @endcan
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    @can('viewElections', [\App\Poll::class])
                    <div class="tab-pane fade show active" id="pills-elections" role="tabpanel" aria-labelledby="pills-elections-tab">
                        <div class="list-model-wrapp" id="list-elections-wrapp"></div>
                    </div>
                    @endcan
                    @can('viewElectors', [\App\Poll::class])
                    <div class="tab-pane fade" id="pills-electors" role="tabpanel" aria-labelledby="pills-electors-tab">
                        <div class="list-model-wrapp" id="list-electors-wrapp"></div>
                    </div>
                    @endcan
                    @can('viewVotes', [\App\Poll::class])
                    <div class="tab-pane fade" id="pills-votes" role="tabpanel" aria-labelledby="pills-votes-tab">
                        <div class="list-model-wrapp" id="list-votes-wrapp"></div>
                    </div>
                    @endcan
                    @can('viewEmailsSent', [\App\Poll::class])
                    <div class="tab-pane fade" id="pills-emails" role="tabpanel" aria-labelledby="pills-emails-tab">

                        <div class="text-center">
                            <div class="btn-group pt-3 text-center" role="group" aria-label="Basic example">
                                @can('sendEmails', [\App\Poll::class])
                                    <div>
                                        <a
                                            class="btn btn-outline-info mail-button @if($poll->state == \App\Poll::MAILS_SENDED || $poll->state == \App\Poll::MAILS_RE_SENDED) disabled @endif"
                                            data-href="{{ route('polls.sendNotificationMailsAjax', $poll->id) }}"
                                            data-type="notification"
                                        >
                                            Enviar notificación
                                        </a>
                                        <div
                                            class="p-2 message-notification @if($poll->state == \App\Poll::MAILS_SENDED || $poll->state == \App\Poll::MAILS_RE_SENDED) d-block @endif"
                                        >
                                            <i> (Los correos ya han sido enviados)</i>
                                        </div>
                                    </div>
                                    <div>
                                        <a
                                            class="btn btn-outline-info mail-button"
                                            data-href="{{ route('polls.sendRecordatoryMailsAjax', $poll->id) }}"
                                            data-type="recordatory"
                                        >
                                            Enviar recordatorio
                                        </a><br>
                                        <div class="p-2 message-recordatory @if($poll->state == \App\Poll::MAILS_RE_SENDED) d-block @endif">
                                            <i> (Los recordatorios ya han sido enviados)</i>
                                        </div>
                                    </div>
                                @endcan
                            </div>
                        </div>
                        <div class="list-model-wrapp" id="list-emails-wrapp"></div>

                    </div>
                    @endcan
                </div>
            </div>

        </div>
    </div>

    <div class="modal" id="modalAjaxMensaje">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Mensaje</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body modal-ajax-content">

                    <div class="modal-body-msg-wrapp"></div>

                </div>

            </div>
        </div>
    </div>
@endsection
@push('scripts')

    <script>
        //MODELS AJAX ROUTES
        let elections_list_route = "{{ route('polls.electionsAjax', $poll->id) }}";
        let electors_list_route = "{{ route('polls.pollElectorsAjax', $poll->id) }}";
        let votes_list_route = "{{ route('polls.pollVotesAjax', $poll->id) }}";
        let emails_list_route = "{{ route('emailActivityLogs.listAjax', $poll->id) }}";
        //MODELS HTM WRAPPERS
        let elections_html_wrapper = "list-elections-wrapp";
        let electors_html_wrapper = "list-electors-wrapp";
        let votes_html_wrapper = "list-votes-wrapp";
        let emails_html_wrapper = "list-emails-wrapp";

        function getURLParameter(url, name) {
            return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
        }

        $(document).ready(function() {

            addModelsList(1, elections_list_route, elections_html_wrapper, "");
            addModelsList(1, electors_list_route, electors_html_wrapper, "");
            addModelsList(1, votes_list_route, votes_html_wrapper, "");
            addModelsList(1, emails_list_route, emails_html_wrapper, "");


            $(document).on('click', '.search-button', function(e){
                e.preventDefault();
                let element = $(this);
                search(element, e);
            });
            $(document).on('keyup', "input.search_input",function (e) {
                if (e.key === 'Enter' || e.keyCode === 13) {
                    e.preventDefault();
                    let element = $(this);
                    search(element, e);
                }
            });
            $(document).on('click', '.mail-button', function(e){
                e.preventDefault();
                let element = $(this);
                let button_type = $(this).attr("data-type");
                let route = element.attr('data-href');

                $("#modalAjaxMensaje .modal-body-msg-wrapp").html("Por favor espere mientras se procesan los correos...");
                $('#modalAjaxMensaje').modal('show');

                sendMail(route);

                //console.log("Button type: "+button_type);
                //console.log(".message-"+button_type);
                if(button_type === "notification"){
                    $(this).addClass("disabled");
                }
                $(".message-"+button_type).show();
            });

            $(document).on('click', '.model-pagination .pagination a', function(e){
                e.preventDefault();
                let model_html_wrapper = $(this).closest(".list-model-wrapp").attr("id");

                $("#"+model_html_wrapper).append('Cargando los datos...');
                var url = $(this).attr('href');
                var url_without_parameters = url.substring(0,url.indexOf("?"));
                var page = getURLParameter(url, 'page');
                var search_input = getURLParameter(url, 'search_input');

                console.log("Model html wrapper: "+model_html_wrapper);
                console.log("url: "+url);
                console.log("url without parameters: "+url_without_parameters);
                console.log("Page: "+page);
                console.log("Search input: "+search_input);

                addModelsList(page, url_without_parameters, model_html_wrapper, search_input);
            });

            function search(element, e) {
                console.log("evento: "+e.type);

                let model_html_wrapper = element.closest(".list-model-wrapp").attr("id");
                let search_input = element.closest(".list-model-wrapp").find(".search_input").val();
                let url = element.closest(".list-model-wrapp").find(".search_input").attr('data-href');

                $("#"+model_html_wrapper).append('Cargando los datos...');
                var url_without_parameters = url.split('?')[0];
                var page = getURLParameter(url, 'page');

                console.log("Model html wrapper: "+model_html_wrapper);
                console.log("url: "+url);
                console.log("search input: "+search_input);
                console.log("url without parameters: "+url_without_parameters);
                console.log("Page: "+page);

                addModelsList(page, url_without_parameters, model_html_wrapper, search_input);
            }

            function addModelsList(page, route, html_wrapper, search_input){
                //Enviamos una solicitud con el ruc de la empresa
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: route,
                    tryCount : 0,
                    retryLimit : 3,
                    data:{
                        page: page,
                        search_input: search_input,
                    },
                    success: function (data) {
                        $("#"+html_wrapper).html(data);

                    },error:function(data){
                        console.log("-----------------ERROR----------------");
                        this.tryCount++;
                        console.log("Try: "+this.tryCount+" out of "+this.retryLimit+".");
                        if (this.tryCount <= this.retryLimit) {
                            //try again
                            $.ajax(this);
                            return;
                        }
                        console.log(data);
                        console.log("-----------------ERROR----------------");
                    }
                });
            }

            function sendMail(route){

                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: route,
                    timeout: 0,
                    success: function (data) {

                        console.log(data);

                        let message_reload = "<br>Por favor no realice ninguna acción, esta página se cargará nuevamente en unos segundos.";
                        $("#modalAjaxMensaje .modal-body-msg-wrapp").html(data.message+message_reload);
                        $('#modalAjaxMensaje').modal('show');
                        setTimeout(function(){
                            window.location.reload(1);
                        }, 5000);

                    },error:function(data){
                        console.log("-----------------ERROR ENVIANDO MAILS----------------");
                        console.log(data);
                        let message_reload = "<br>Por favor no realice ninguna acción, esta página se cargará nuevamente en unos segundos.";
                        $("#modalAjaxMensaje .modal-body-msg-wrapp").html("Se presentó un error. <br>Error: "+data.statusText + message_reload);
                        $('#modalAjaxMensaje').modal('show');
                        console.log("-----------------ERROR ENVIANDO MAILS----------------");
                        setTimeout(function(){
                            window.location.reload(1);
                        }, 5000);
                    }
                });
            }


        });

    </script>

@endpush
