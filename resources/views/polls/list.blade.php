@extends('layouts.app', ['page' => 'polls'])

@section('content')
    <div class="container">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row d-none">
            @include('partials.polls.search')
        </div>
        <div class="row justify-content-center">
            <div class="w-100 text-center">
                <h5>
                    @can('create', [\App\Poll::class])
                        <a
                            class="form-control btn btn-agregar-sistema"
                            href="{{ route('polls.create') }}"
                        >Agregar</a>
                    @endcan
                    GRUPOS DE VOTACIONES
                </h5>
                <form action="{{ route('polls.search') }}" method="get" class="form-inline form-search">
                    <div class="input-group input-group-search">
                        <input
                            class="form-control"
                            name="name"
                            id="name"
                            type="text"
                            placeholder="{{ __("Buscar grupo de votaciones") }}"
                        >
                        <div class="input-group-append">
                            <input
                                class="btn btn-outline-secondary"
                                name="filter"
                                type="submit"
                                value="Buscar"
                            >
                        </div>
                    </div>
                </form>
            </div>
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col" class="text-center"># Votaciones</th>
                    <th scope="col" class="text-center">Comienza</th>
                    <th scope="col" class="text-center">Termina</th>
                </tr>
                </thead>
                <tbody>
                @forelse($polls as $poll)
                    <tr>
                        <td>

                            @if($poll->start_date > now())
                                <span class="badge badge-info text-white">Aún no comienza</span>
                            @elseif($poll->end_date < now())
                                <span class="badge badge-secondary">Terminado</span>
                            @else
                                <span class="badge badge-success">En proceso</span>
                            @endif<br>
                            <a href="{{ route('polls.info', $poll->id) }}">{{ $poll->name }}</a></td>
                        <td class="text-center">{{ $poll->elections->count() }}</td>
                        <td class="text-center">
                            {{ \Carbon\Carbon::parse($poll->start_date )->format('d/m/Y')}}<br>
                            {{ \Carbon\Carbon::parse($poll->start_date )->format('H:i')}}
                        </td>
                        <td class="text-center">
                            {{ \Carbon\Carbon::parse($poll->end_date )->format('d/m/Y')}}<br>
                            {{ \Carbon\Carbon::parse($poll->end_date )->format('H:i')}}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay votaciones disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $polls->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/jquery.session.js') }}"></script>
    <script>
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'YYYY-MM-DD',
                locale: 'es',
                sideBySide: true,
                buttons: {showClose: true},
                showClose: true,
                icons: {
                    close: 'fa fa-check'
                }
            });
        });

    </script>
@endpush
