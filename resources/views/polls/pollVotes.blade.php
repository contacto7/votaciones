@extends('layouts.app', ['page' => 'polls'])

@section('content')
    <div class="container container-form">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Votos del grupo de votación: $poll->name"),
                    'icon' => "file-text-o"
                ])
            </div>
        </div>
        <div class="row mt-3 mb-3">
            <div class="col-12 flex-column align-items-center d-flex mb-4">
                <table class="table table-hover table-light">
                    <thead>
                    <tr>
                        <th>ID voto</th>
                        <th>Pregunta</th>
                        <th>Candidato elegido</th>
                        <th>Registrado</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($votes as $vote)
                        <tr>
                            <td>{{ $vote->id }}</td>
                            <td>{{ $vote->candidate->question->name }}</td>
                            <td>{{ $vote->candidate->name }}</td>
                            <td>{{ $vote->created_at }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td>{{ __("Aún no hay votos")}}</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div>
        <div class="row justify-content-center mb-4">
            {{ $votes->appends(request()->except('page'))->links() }}
        </div>
        <div class="row text-center d-block mb-4">
            <a
                class="btn btn-outline-info"
                href="{{ route('polls.info', $poll->id) }}"
                data-toggle="tooltip"
                data-placement="top"
                title="Ver votación"
            >
                Regresar al grupo de votaciones
            </a>
        </div>
    </div>
@endsection
