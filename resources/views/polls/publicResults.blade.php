@extends('layouts.app', ['page' => 'results'])

@section('content')
    <div class="container container-results">


        <div class="row results-title-row">
            <h1 class="results-title">
                {{ $poll->name }}
            </h1>
            @if($poll->start_date > now())
                <span class="badge-poll-status badge badge-info text-white">Aún no comienza</span>
            @elseif($poll->end_date < now())
                <span class="badge-poll-status badge badge-secondary">Terminado</span>
            @else
                <span class="badge-poll-status badge badge-success">En proceso</span>
            @endif
            <div class="results-description">
                Inicio: {{ \Carbon\Carbon::parse($poll->start_date )->format('H:i')}}h del {{ \Carbon\Carbon::parse($poll->start_date )->format('d/ m/ Y')}}<br>
                Fin: {{ \Carbon\Carbon::parse($poll->end_date )->format('H:i')}}h del {{ \Carbon\Carbon::parse($poll->end_date )->format('d/ m/ Y')}}
            </div>
        </div>

        <div class="row electors-info-row">
            <h2 class="results-subtitle">
                Participación de electores
            </h2>
            <div class="electors-info-canvas-wrapp">
                <canvas id="electorsVotedChart" width="400" height="400"></canvas>
            </div>
            <div class="electors-info-description-wrapp">
                <div class="electors-info-description">
                    <div class="eid-section">
                        <div class="eid-section-title">
                            <b>Total electores</b>
                        </div>
                        <div class="eid-section-value">
                            <b>{{ $electorsPollCount }}</b>
                        </div>
                    </div>
                    <div class="eid-section">
                        <div class="eid-section-title">
                            <span class="color-legend color-legend-voted"></span> Votaron
                        </div>
                        <div class="eid-section-value">
                            <span class="total-voted-absolute">####</span>
                            (<span class="total-voted-relative">##</span> %)
                        </div>
                    </div>
                    <div class="eid-section">
                        <div class="eid-section-title">
                            <span class="color-legend color-legend-not-voted"></span> No votaron
                        </div>
                        <div class="eid-section-value">
                            <span class="total-not-voted-absolute">####</span>
                            (<span class="total-not-voted-relative">##</span> %)
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="elections-info-wrapp">
            <h2 class="results-elections-title">
                Resultados
            </h2>
            @foreach($poll->elections as $election)
                <div class="row elections-info-row">
                    <h2 class="results-subtitle">
                        {{ $election->name }}
                    </h2>
                    @foreach($election->questions as $question)
                        <h3 class="results-subtitle-question">
                            {{ $question->name }}
                        </h3>
                        <div class="electors-info-canvas-wrapp">
                            <canvas id="questionChart-{{ $question->id }}" width="600" height="400"></canvas>
                        </div>
                        <div class="electors-info-description-wrapp">
                            <div class="electors-info-description">

                            </div>
                        </div>
                    @endforeach

                </div>
            @endforeach
        </div>

    </div>

@endsection
@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@1"></script>

    <script>

        //INITIAL POLL VALUES
        let electorsPollCount = {{ $electorsPollCount }};
        let votesPollCount = {{ $votesPollCount }};
        let poll = @json($poll);

        // ELECTORS ABSOLUT VALUES
        let total_electors = parseInt(electorsPollCount);
        let electors_voted = parseInt(votesPollCount);
        let electors_not_voted = total_electors - electors_voted;
        // ELECTORS PERCENTAGE VALUES
        ////UP TO 3 DECIMALS PERCENTAGE
        let electors_voted_percentage = Math.round((electors_voted/total_electors*100*1000) )/1000;
        let electors_not_voted_percentage = 100 - electors_voted_percentage;
        electors_not_voted_percentage = Math.round((electors_not_voted_percentage*1000) )/1000;

        //CANVAS CONFIGURATION
        let primary_color = '#5ec5dd';
        let secondary_color = '#11c471';
        let third_color = '#3ab4de';
        let orange_color = '#7b68ee';
        let yellow_color = '#ffcc16';
        let lime_color = '#7aca04';
        let blackest_color = '#3c3c3b';
        let gray_color = '#f4f4f4';
        ////PIE CONFIGURATION - VOTE-DIDNT VOTE GRAPH SECTION
        var labels = ['Votaron', 'No votaron'];
        var canvas_element = '#electorsVotedChart';
        var backgroundColors = [orange_color,third_color];


        $(document).ready(function() {

            ///////VOTED - DIDNT VOTE PIE GRAPH SECTION /////////
            //PIE CHART
            createPieChart(labels, canvas_element, backgroundColors, electors_voted_percentage, electors_not_voted_percentage);
            //LEGEND SECTION
            updateElectorsInfo();
            ///////VOTED - DIDNT VOTE PIE GRAPH SECTION/////////

            //QUESTIONS CHARTS
            $.each(poll.elections, function(i, election) {
                console.log("-------ELECTIONS-------------");
                console.log(election.name);
                $.each(election.questions, function(i, question) {
                    console.log("-------QUESTIONS-------------");

                    //CANVAS CONFIGURATION
                    let labels_candidates = new Array();
                    let votes_candidates = new Array();
                    let backgroundColors = new Array();
                    let candidates_arr = new Array();
                    let canvas_element = "#questionChart-"+question.id;
                    let lighten_value = 0.3;
                    let lighten_index = 0;

                    //QUESTION CONFIGURATION
                    let total_votes = 0;

                    //EACH CANDIDATE
                    $.each(question.candidates, function(i, candidate) {
                        console.log("-------CANDIDATE-------------");
                        let votes_count = 0;
                        let candidate_obj = {};
                        $.each(candidate.votes, function(i, votes) {
                            console.log("-------VOTES-------------");
                            votes_count = votes.votes_count;
                            total_votes += votes_count;
                        });
                        candidate_obj.data = votes_count;

                        //IF CANDIDATE IS NOT BLANK VOTE OR FLAWED VOTE
                        candidate_obj.label = [candidate.list_name,candidate.name];
                        console.log(votes_count);
                        candidates_arr.push(candidate_obj);
                        backgroundColors.push(pSBC ( 0.1*lighten_index, orange_color ));
                        lighten_index++;
                    });

                    //SORT JSON BY DATA (VOTES)
                    //THEN
                    //GET LABELS AND VOTES AS SEPARATED ARRAYS
                    let label_data_obj = sortJsonThenGetLabelsAndData(candidates_arr, 'data', 'desc');
                    labels_candidates = label_data_obj.labels;
                    votes_candidates = label_data_obj.datas;

                    console.log("VOTOS: "+ votes_candidates);

                    //EACH CANDIDATE PERCENTAGE VOTES
                    let percentage_accumulated = 0;
                    let votes_arr_length = votes_candidates.length;
                    let votes_candidates_percentages = new Array();

                    $.each(votes_candidates, function(i, votes_candidate) {
                        let percentage = 0;

                        if(i < (votes_arr_length-1) ){
                            percentage = Math.round((votes_candidate/total_votes*100*1000) )/1000;
                            percentage_accumulated+=percentage;
                        }else{
                            percentage = Math.round(( (100 - percentage_accumulated)*1000) )/1000;;

                        }
                        votes_candidates_percentages.push(percentage);
                    });

                    createBarChart(labels_candidates, votes_candidates, canvas_element, backgroundColors, votes_candidates_percentages);
                });
            });
            //QUESTIONS CHARTS


//END DOCUMENT READY FUNCTION
        });


        function createBarChart(labels, votes_candidates, canvas_element, backgroundColors, votes_candidates_percentages) {
            let ctx = $(canvas_element);
            var myChart = new Chart(ctx, {
                type: 'horizontalBar',
                data: {
                    labels: labels,
                    datasets: [{
                        label: 'Votos',
                        data: votes_candidates_percentages,
                        backgroundColor: backgroundColors,
                        borderColor: backgroundColors,
                        borderWidth: 1,
                        barThickness: 40,
                    }],
                },
                plugins: [ChartDataLabels],
                options: {
                    layout: {
                        padding: {
                            left: 25,
                            right: 75,
                            top: 50,
                            bottom: 50
                        }
                    },
                    legend:{
                        display: false
                    },
                    title: {
                        display: false,
                    },
                    scales: {
                        yAxes: [{
                            display: true,
                            ticks: {
                                suggestedMin: 0,
                                beginAtZero: true,
                                fontSize: 12,
                                callback: function(value, index, values) {
                                    var len = 12;
                                    complete_arr = [];

                                    //DELETING DUPLICATED NAME, LIST NAME VALUES
                                    //MAINLY INTENDED FOR NULL O BLANK VOTES
                                    value = value.filter(function(item, pos) {
                                        return value.indexOf(item) == pos;
                                    });
                                    //DIVIDE INTO WORDS ARRAY OF MAX LEN = len
                                    $.each(value, function( i, val ) {
                                        complete_arr = stringSeparatedByMaxCharToArray(val, len, complete_arr);
                                    });

                                    return complete_arr;
                                }
                            },
                            gridLines: {
                                offsetGridLines: false,
                                display: true
                            }
                        }],
                        xAxes: [{
                            display: true,
                            ticks: {
                                suggestedMin: 0,
                                beginAtZero: true,
                                //ADD PERCENTAGE TO AXIS LABEL VALUE 10%
                                callback: function(value){return value+ "%"},
                            },
                        }],
                        scale: {
                            pointLabels: { fontSize: 38 }
                        },
                    },
                    tooltips: {
                        mode: 'label',
                        callbacks: {
                            title: function(tooltipItems, data) {

                                let title = tooltipItems[0].yLabel;
                                //DELETING DUPLICATED NAME, LIST NAME VALUES
                                //MAINLY INTENDED FOR NULL O BLANK VOTES
                                title = title.filter(function(item, pos) {
                                    return title.indexOf(item) == pos;
                                });
                                return title;
                            },
                            label: function(tooltipItem, data) {
                                //let percentage = Math.round((tooltipItem.xLabel/total_votes*100*1000) )/1000;
                                return data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.xLabel+"% ("+votes_candidates[tooltipItem.index]+")";
                            },
                        }
                    },
                    plugins:{
                        datalabels: {
                            align: 'end',
                            anchor: 'end',
                            backgroundColor: 'rgb(74, 74, 74, 0.8)',
                            color: 'white',
                            offset: 2,
                            padding: 4,
                            labels: {
                                title: {
                                    font: {
                                        size: 12
                                    }
                                }
                            },
                            formatter: function(value, context) {
                                return value + '%';
                            }

                        }

                    },

                }
            });

        }

        function createPieChart(labels, canvas_element, backgroundColors, electors_voted_percentage, electors_not_voted_percentage) {
            let ctx = $(canvas_element);
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: labels,
                    datasets: [{
                        label: '# of Votes',
                        data: [electors_voted_percentage, electors_not_voted_percentage],
                        backgroundColor: backgroundColors,
                        borderColor: backgroundColors,
                        borderWidth: 1
                    }],
                    datalabels: {
                        labels: {
                            title: {
                                color: 'green'
                            }
                        }
                    }
                },
                plugins: [ChartDataLabels],
                options: {
                    tooltips: {
                        enabled: false
                    },
                    hover: {mode: null},
                    plugins:{
                        datalabels: {
                            backgroundColor: 'rgb(74, 74, 74, 0.8)',
                            color: 'white',
                            font: {
                                weight: 'bold',
                            },
                            offset: 8,
                            padding: 6,
                            labels: {
                                title: {
                                    font: {
                                        weight: 'bold',
                                        size: 15
                                    }
                                }
                            },
                            formatter: function(value, context) {
                                return context.chart.data.labels[context.dataIndex] + ': ' + value + '%';
                            }

                        }

                    },
                    legend:{
                        display: false
                    }
                }
            });

        }


        function updateElectorsInfo() {
            $(".total-voted-absolute").html(electors_voted);
            $(".total-voted-relative").html(electors_voted_percentage);
            $(".total-not-voted-absolute").html(electors_not_voted);
            $(".total-not-voted-relative").html(electors_not_voted_percentage);
        }

        // Version 4.0
        const pSBC=(p,c0,c1,l)=>{
            let r,g,b,P,f,t,h,i=parseInt,m=Math.round,a=typeof(c1)=="string";
            if(typeof(p)!="number"||p<-1||p>1||typeof(c0)!="string"||(c0[0]!='r'&&c0[0]!='#')||(c1&&!a))return null;
            if(!this.pSBCr)this.pSBCr=(d)=>{
                let n=d.length,x={};
                if(n>9){
                    [r,g,b,a]=d=d.split(","),n=d.length;
                    if(n<3||n>4)return null;
                    x.r=i(r[3]=="a"?r.slice(5):r.slice(4)),x.g=i(g),x.b=i(b),x.a=a?parseFloat(a):-1
                }else{
                    if(n==8||n==6||n<4)return null;
                    if(n<6)d="#"+d[1]+d[1]+d[2]+d[2]+d[3]+d[3]+(n>4?d[4]+d[4]:"");
                    d=i(d.slice(1),16);
                    if(n==9||n==5)x.r=d>>24&255,x.g=d>>16&255,x.b=d>>8&255,x.a=m((d&255)/0.255)/1000;
                    else x.r=d>>16,x.g=d>>8&255,x.b=d&255,x.a=-1
                }return x};
            h=c0.length>9,h=a?c1.length>9?true:c1=="c"?!h:false:h,f=this.pSBCr(c0),P=p<0,t=c1&&c1!="c"?this.pSBCr(c1):P?{r:0,g:0,b:0,a:-1}:{r:255,g:255,b:255,a:-1},p=P?p*-1:p,P=1-p;
            if(!f||!t)return null;
            if(l)r=m(P*f.r+p*t.r),g=m(P*f.g+p*t.g),b=m(P*f.b+p*t.b);
            else r=m((P*f.r**2+p*t.r**2)**0.5),g=m((P*f.g**2+p*t.g**2)**0.5),b=m((P*f.b**2+p*t.b**2)**0.5);
            a=f.a,t=t.a,f=a>=0||t>=0,a=f?a<0?t:t<0?a:a*P+t*p:0;
            if(h)return"rgb"+(f?"a(":"(")+r+","+g+","+b+(f?","+m(a*1000)/1000:"")+")";
            else return"#"+(4294967296+r*16777216+g*65536+b*256+(f?m(a*255):0)).toString(16).slice(1,f?undefined:-2)
        }


        function sortJsonThenGetLabelsAndData(arr, key, way) {
            let candidates_arr = sortJSON(arr,'data', 'desc'); // asc or desc
            let label_data_obj = getLabelAndDataFromJson(candidates_arr);
            return label_data_obj;
        }
        function getLabelAndDataFromJson(json_arr) {
            let json_to_arr_separated = {};
            let labels_arr = new Array();
            let data_arr = new Array();

            $.each(json_arr, function(i, element) {
                labels_arr.push(element.label);
                data_arr.push(element.data);
            });

            json_to_arr_separated.labels = labels_arr;
            json_to_arr_separated.datas = data_arr;

            return json_to_arr_separated;
        }
        function sortJSON(arr, key, way) {
            return arr.sort(function(a, b) {
                var x = a[key]; var y = b[key];
                if (way === 'asc') { return ((x < y) ? -1 : ((x > y) ? 1 : 0)); }
                if (way === 'desc') { return ((x > y) ? -1 : ((x < y) ? 1 : 0)); }
            });
        }
        function stringSeparatedByMaxCharToArray(str, len, complete_arr) {
            var curr = len;
            var prev = 0;

            while (str[curr]) {

                let next = curr +1;
                let next_character = str[next];

                if (next_character == ' ') {
                    complete_arr.push(str.substring(prev,next));
                    prev = next;
                    curr += (len+1);
                }

                curr++;
            }
            complete_arr.push(str.substr(prev));
            console.log("+++++++STRING FUNCTION+++++++++++");

            return complete_arr;
        }
    </script>

@endpush
