@extends('layouts.app', ['page' => 'polls'])

@section('content')
<div class="container container-form">

    <div class="row">
        @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
    </div>
    <div class="row">
        <div class="head-page">
            @include('partials.title', [
            'title' => __("Agregar Grupo de votación"),
            'icon' => "user"
            ])
        </div>
    </div>
    <form
        method="POST"
        action="{{ ! $poll->id ? route('polls.store'): route('polls.update', ['id'=>$poll->id]) }}"
        novalidate
        enctype="multipart/form-data"
    >
        @if($poll->id)
        @method('PUT')
        @endif

        @csrf

        <div class="form-group">
            <label for="name">Nombre de grupo de votaciones</label>
            <input
                type="text"
                class="form-control {{ $errors->has('name') ? 'is-invalid': '' }}"
                name="name"
                id="name"
                placeholder=""
                value="{{ old('name') ?: $poll->name }}"
                required
                autocomplete="off"
            >
            @if($errors->has('name'))
            <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
            @endif
        </div>

        <div class="form-group">
            <label for="start_date">Fecha y hora de inicio</label>
            <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                <input
                    type="text"
                    name="start_date"
                    id="start_date"
                    class="form-control datetimepicker-input {{ $errors->has('start_date') ? 'is-invalid': '' }}"
                    data-target="#datetimepicker1"
                    data-toggle="datetimepicker"
                    value="{{ old('start_date') ?: $poll->start_date }}"
                    autocomplete="off"
                />
                <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
                @if($errors->has('start_date'))
                <span class="invalid-feedback">
                            <strong>{{ $errors->first('start_date') }}</strong>
                        </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label for="end_date">Fecha y hora de fin</label>
            <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                <input
                    type="text"
                    name="end_date"
                    id="end_date"
                    class="form-control datetimepicker-input {{ $errors->has('end_date') ? 'is-invalid': '' }}"
                    data-target="#datetimepicker2"
                    data-toggle="datetimepicker"
                    value="{{ old('end_date') ?: $poll->end_date }}"
                    autocomplete="off"
                />
                <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
                @if($errors->has('end_date'))
                <span class="invalid-feedback">
                        <strong>{{ $errors->first('end_date') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group testing-button">
            <label for="auth_factors">Validación de identidad</label>
            <select
                class="form-control {{ $errors->has('auth_factors') ? 'is-invalid': '' }}"
                name="auth_factors"
                id="auth_factors"
                required
            >
                <option
                    {{
                        (int) old('auth_factors') === \App\Poll::ONE_FACTOR
                        ||
                        (int) $poll->auth_factors === \App\Poll::ONE_FACTOR
                        ?
                        'selected' : ''
                    }}
                    value="{{ \App\Poll::ONE_FACTOR }}"
                >Simple (correo)</option>
                <option
                    {{
                        (int) old('auth_factors') === \App\Poll::TWO_FACTOR
                        ||
                        (int) $poll->auth_factors === \App\Poll::TWO_FACTOR
                        ?
                        'selected' : ''
                    }}
                    value="{{ \App\Poll::TWO_FACTOR }}"
                >Doble (correo + celular)</option>
                <option
                    {{
                        (int) old('auth_factors') === \App\Poll::RENIEC_VALIDATION
                        ||
                        (int) $poll->auth_factors === \App\Poll::RENIEC_VALIDATION
                        ?
                        'selected' : ''
                    }}
                    value="{{ \App\Poll::RENIEC_VALIDATION }}"
                >Validación con RENIEC</option>
            </select>
        </div>
        <div class="form-group testing-button">
            <label for="visibility">Votaciones públicas</label>
            <select
                class="form-control {{ $errors->has('visibility') ? 'is-invalid': '' }}"
                name="visibility"
                id="visibility"
                required
            >
                <option
                    {{
                        (int) old('visibility') === \App\Poll::PUBLIC
                        ||
                        (int) $poll->visibility === \App\Poll::PUBLIC
                        ?
                        'selected' : ''
                    }}
                    value="{{ \App\Poll::PUBLIC }}"
                >Si</option>
                <option
                    {{
                        (int) old('visibility') === \App\Poll::PRIVATE
                        ||
                        (int) $poll->visibility === \App\Poll::PRIVATE
                        ?
                        'selected' : ''
                    }}
                    value="{{ \App\Poll::PRIVATE }}"
                >No</option>
            </select>
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-danger">
                {{ __($btnText) }}
            </button>
        </div>

    </form>

</div>

@endsection

@push('styles')
<link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
<script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
<script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
<script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>

<script>
    $(function () {
        $('#datetimepicker1').datetimepicker({
            format: 'YYYY-MM-DD HH:mm',
            locale: 'es',
            minDate: new Date(),
            sideBySide: true,
            buttons: {showClose: true},
            showClose: true,
            icons: {
                close: 'fa fa-check'
            }
        });
        $('#datetimepicker2').datetimepicker({
            format: 'YYYY-MM-DD HH:mm',
            locale: 'es',
            minDate: new Date(),
            sideBySide: true,
            buttons: {showClose: true},
            showClose: true,
            icons: {
                close: 'fa fa-check'
            }
        });


        $("#datetimepicker1").on("change.datetimepicker", ({date, oldDate}) => {
            console.log("New date", date);
            console.log("Old date", oldDate);

            $("#datetimepicker2").datetimepicker('minDate', date);
        })
    });


</script>
@endpush
