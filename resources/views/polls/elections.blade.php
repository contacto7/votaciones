@extends('layouts.app', ['page' => 'polls'])

@section('content')
    <div class="container">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            @include('partials.polls.electionSearch')
        </div>
        <div class="row justify-content-center">
            <div class="w-100 text-center">
                <h5 class="text-uppercase">Lista de votaciones en {{ $poll->name }}</h5>
                @can('create', [\App\Election::class])
                    @can('updateAnymore', [\App\Poll::class, $poll])
                    <a
                        class="form-control btn btn-agregar-sistema"
                        href="{{ route('elections.create', ['id'=>$poll->id]) }}"
                    >Añadir</a>
                    @endcan
                @endcan
            </div>
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col" class="d-none">#</th>
                    <th scope="col">Votación</th>
                    <th scope="col">Grupo de electores</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @forelse($elections as $election)
                    <tr>
                        <td class="d-none">{{ $election->id }}</td>
                        <td>{{ $election->name }}</td>
                        <td>
                            {{ $election->group->name }}
                        </td>
                        <td>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('elections.info', $election->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Ver votación"
                            >
                                <i class="fa fa-info"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3">{{ __("Aún no se han agregado elecciones al grupo de votaciones")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $elections->appends(request()->except('page'))->links() }}
        </div>
        <div class="row justify-content-center">
            <a
                class="btn btn-outline-info"
                href="{{ route('polls.info', $poll->id) }}">
                Regresar al detalle del grupo de votaciones
            </a>

        </div>
    </div>
@endsection
@push('scripts')
@endpush
