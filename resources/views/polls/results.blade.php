@extends('layouts.app', ['page' => 'polls'])

@section('content')
    <div class="container container-form container-results">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>

        <div class="title-poll">
            <h1>
                {{ $poll->name }}
            </h1>
            @if($poll->start_date > now())
                <span class="badge badge-info text-white">Aún no comienza</span>
            @elseif($poll->end_date < now())
                <span class="badge badge-secondary">Terminado</span>
            @else
                <span class="badge badge-success">En proceso</span>
            @endif
        </div>

        <div class="text-center">
            <div class="mb-4">
                Inicio: {{ \Carbon\Carbon::parse($poll->start_date )->format('H:i')}}h del {{ \Carbon\Carbon::parse($poll->start_date )->format('d/ m/ Y')}}<br>
                Fin: {{ \Carbon\Carbon::parse($poll->end_date )->format('H:i')}}h del {{ \Carbon\Carbon::parse($poll->end_date )->format('d/ m/ Y')}}
            </div>
            <table class="table table-votaciones table-bordered table-results">
                <thead>
                <tr>
                    <th>Total Electores</th>
                    <th>Votaron</th>
                    <th>% Votaron</th>
                </tr>
                </thead>
                <tbody>
                    <tr class="candidate">
                        <td>{{ $total_electores }}</td>
                        <td>{{ $votesCount }}</td>
                        <td>{{ number_format($votesCount/$total_electores*100, 2) }}</td>
                    </tr>

                </tbody>
            </table>
        </div>
        @foreach($elections as $election)
            <h1 class="text-center title-poll-results">{{ $election->name }}</h1>
            @forelse($election->questions as $question)
                <div class="row mt-3 mb-3 question question-{{ $question->id }}">
                    <div class="col-12 flex-column align-items-center d-flex mb-4">
                        <h4 class="title-election-results">{{ $question->order }}. {{ $question->name }}</h4>
                        <table class="table table-votaciones table-results-elections table-hover table-bordered">
                            <thead>
                            <tr>
                                <th class="col-order">Lista N°</th>
                                <th class="col-candidate">Partido</th>
                                <th class="col-candidate">Candidato</th>
                                <th class="col-votes">Votos</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($question->candidatesOrdered() as $candidate)
                                <tr class="candidate">
                                    <td class="col-order list-{{ $candidate->list_number }}">{{ $candidate->list_number }}</td>
                                    <td class="col-candidate list-{{ $candidate->list_number }}">{{ $candidate->list_name }}</td>
                                    <td class="col-candidate">{{ $candidate->name }}</td>
                                    <td class="col-votes">{{ $candidate->votes->sum("weight") }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td>{{ __("Aún no hay candidatos")}}</td>
                                </tr>
                            @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            @empty
                <div class="row">
                    <h4 class="title-election-results">Aún no hay preguntas</h4>
                </div>
            @endforelse
        @endforeach



        <div class="row text-center d-block mb-4">
            <a
                class="btn btn-outline-info"
                href="{{ route('polls.info', $poll->id) }}"
                data-toggle="tooltip"
                data-placement="top"
                title="Ver votación"
            >
                Regresar a al grupo de votaciones
            </a>
            <a
                class="btn btn-outline-info"
                href="{{ action('PollController@resultsPDF', $poll->id)}}"
            >
                Descargar resultados
            </a>
        </div>
    </div>
@endsection
@push('scripts')
    <script>

        var next_question_order = 1;

        $(function() {
            $(".list-250, .list-251").html("-");
            $( ".question" ).each(function() {
                let number_candidates = $(this).find(".candidate").length;
                $( this ).find(".list-order-251").html(number_candidates);
                $( this ).find(".list-order-250").html(--number_candidates);
            });
        });


    </script>
@endpush
