<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<style>
    body{
        font-size: 14px;
        text-align: center;
        font-family: sans-serif !important;
    }
    td:first-child{
        font-weight: bold;
    }
    .title{
        text-align: center;
    }
    .subtitle{
        text-align: center;
        padding-bottom: 10px;
    }
    .firma-row{
        padding: 10px 0;
    }
    @page {
        margin: 100px 30px;
    }
    table{
        margin: 0 auto;
    }
    td{
        vertical-align: top;
        padding: 10px;
    }
    td.col-candidate{
        text-align: left;
    }
    .list-250 span,
    .list-251 span,
    .list-order-251 span,
    .list-order-250 span{
        display: none;
    }
    .list-250::after,
    .list-251::after,
    .list-order-251::after,
    .list-order-250::after{
        content: "-";
    }

    .letterhead-wrapp{
        position: fixed;
        left: -650px;
        top: -80px;
    }
    .letterhead{
        width: 70px;
    }

    .letterhead-wrapp-sec{
        position: fixed;
        right: -650px;
        top: -80px;
    }
    .letterhead-sec{
        width: 70px;
    }

    .letterhead-text-wrapp{
        font-size: 20px;
        position: fixed;
        top: -60px;
    }

    .footer-wrapp-text{
        font-size: 15px;
        font-style: italic;
        position: fixed;
        bottom: -40px;
        padding-left: 15px;
        padding-right: 15px;
    }
    .header, .footer{
        opacity: 0.5;

    }
    h1{
        font-size: 26px;
    }

</style>
<div class="letterhead-wrapp header" >
    <img class="letterhead" src="{{ asset('image/logo.png') }}">
</div>
<div class="letterhead-text-wrapp header" >
    <div>SISTEMA DE VOTACIONES</div>
</div>
<div class="letterhead-wrapp-sec header" >
    <img class="letterhead-sec" src="{{ asset('image/logo.png') }}">
</div>
<div class="footer-wrapp-text footer" >
    <div>Documento generado por el sistema de votaciones digitales</div>
</div>
<h1>Resultados: {{ $poll->name }}</h1>
@foreach($elections as $election)
    <h2>{{ $election->name }}</h2>
    <div>
        @forelse($election->questions as $question)
            <div class="row mt-3 mb-3 question question-{{ $question->id }}">
                <div class="col-12 flex-column align-items-center d-flex mb-4">
                    <h3 class="title-election-results">{{ $question->order }}. {{ $question->name }}</h3>
                    <table class="table table-votaciones table-hover table-light">
                        <thead>
                        <tr>
                            <th class="col-order" style="width: 25px">Opción</th>
                            <th class="col-order">Lista</th>
                            <th class="col-candidate">Partido</th>
                            <th class="col-candidate">Candidato</th>
                            <th class="col-votes">Votos</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $number_candidates = $question->candidates()->count() @endphp
                        {{ $number_candidates }}
                        @forelse($question->candidatesOrdered() as $candidate)
                            <tr class="candidate">
                                <td class="col-order">
                                    @if($candidate->list_number == 251)
                                        {{ $number_candidates }}
                                    @elseif($candidate->list_number == 250)
                                        {{ $number_candidates-1 }}
                                    @else
                                        {{ $candidate->order }}
                                    @endif
                                </td>
                                <td class="col-order list-{{ $candidate->list_number }}"><span>Lista {{ $candidate->list_number }}</span></td>
                                <td class="col-candidate list-{{ $candidate->list_number }}"><span>{{ $candidate->list_name }}</span></td>
                                <td class="col-candidate">{{ $candidate->name }}</td>
                                <td class="col-votes">{{ $candidate->votes->count() }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td>{{ __("Aún no hay candidatos")}}</td>
                            </tr>
                        @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        @empty
            <div class="row">
                <h4 class="title-election-results">Aún no hay preguntas</h4>
            </div>
        @endforelse
    </div>
@endforeach

</body>
</html>

<script>

    var next_question_order = 1;

    $(function() {
        $(".list-250, .list-251").html("-");
        $( ".question" ).each(function() {
            let number_candidates = $(this).find(".candidate").length;
            $( this ).find(".list-order-251").html(number_candidates);
            $( this ).find(".list-order-250").html(--number_candidates);
        });
    });


</script>
