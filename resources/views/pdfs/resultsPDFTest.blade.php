<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<?php
$primary_color= "#5ec5dd";
$secondary_color= "#11c471";
$secondary_alternate= "#10d895";
$third_color= "#3ab4de";
$blackest= "#3c3c3b";
$orange= "#7b68ee";
$white= "#fff";
$black= "#4a4a4a";
$yellow= "#ffcc16";
$lime_green= "#7aca04";

?>
<style>
    @font-face {
        font-family: 'Montserrat';
        font-style: normal;
        font-weight: normal;
        src: url({{ asset('fonts/Montserrat-Regular.ttf') }}) format('truetype');
    }
    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 600;
        src: url({{ asset('fonts/OpenSans-SemiBold.ttf') }}) format('truetype');
    }
    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: bold;
        src: url({{ asset('fonts/OpenSans-Bold.ttf') }}) format('truetype');
    }
    h1, h2, h3, h4, h5, h6, th{
        font-family: 'Open Sans', sans-serif;
        font-weight: 600;
    }
    body{
        font-size: 14px;
        text-align: center;
        font-family: 'Montserrat', sans-serif;
    }

    @page {
        margin: 100px 30px;
    }
    table{
        margin: 0 auto;
    }
    td{
        vertical-align: top;
        padding: 10px;
    }
    td.col-candidate{
        text-align: left;
    }
    .list-250 span,
    .list-251 span,
    .list-order-251 span,
    .list-order-250 span{
        display: none;
    }
    .list-250::after,
    .list-251::after,
    .list-order-251::after,
    .list-order-250::after{
        content: "-";
    }

    .letterhead-wrapp{
        position: fixed;
        left: -650px;
        top: -80px;
    }
    .letterhead{
        width: 70px;
    }

    .letterhead-wrapp-sec{
        position: fixed;
        right: -650px;
        top: -80px;
    }
    .letterhead-sec{
        width: 70px;
    }

    .letterhead-text-wrapp{
        font-size: 20px;
        position: fixed;
        top: -60px;
    }

    .footer-wrapp-text{
        font-size: 15px;
        font-style: italic;
        position: fixed;
        bottom: -40px;
        padding-left: 15px;
        padding-right: 15px;
    }
    .header, .footer{
        opacity: 0.5;

    }
    .poll-name{
        font-weight: bold;
        color: #212529;
        font-size: 19px;
        text-transform: uppercase;
        margin-bottom: 0;
    }
    .election-name{
        color: {{ $secondary_alternate }};
        font-size: 18px;
        text-transform: uppercase;
    }
    .question-title{
        font-size: 18px;
    }

    .table-results-elections{
        border: 1px solid #dee2e6;
        border-collapse: collapse;
    }
    .table-results-elections th, .table-results-elections td{
        border: 1px solid #dee2e6;
        padding: 0.75rem;
        vertical-align: top;
    }
    table.table-results-elections thead {
        background-color: #3c3c3b;
        color: #fff;
    }
    .text-center{
        text-align: center;
    }
    .text-left{
        text-align: left;
    }
    .poll-info{
        text-align: center;
        margin-bottom: 15px;
    }
    .poll-times{
        margin-bottom: 24px;
    }
    .d-none{
        display: none;
    }

</style>
<div class="letterhead-wrapp header" >
    <img class="letterhead" src="{{ asset('image/logo.png') }}">
</div>
<div class="letterhead-text-wrapp header" >
    <div>SISTEMA DE VOTACIONES</div>
</div>
<div class="letterhead-wrapp-sec header  d-none" >
    <img class="letterhead-sec" src="{{ asset('image/logo.png') }}">
</div>
<div class="footer-wrapp-text footer" >
    <div>Documento generado por el sistema de votaciones digitales</div>
</div>
<h1 class="poll-name">GRUPO DE SUFRAGIOS ADMINISTRATIVAS</h1>

<div class="poll-info">
    <div class="poll-times">
        Inicio: 15:15h del 11/02/2021<br>
        Fin: 07:12h del 13/02/2021
    </div>
    <table class="table table-results-elections">
        <thead>
        <tr>
            <th>Total Electores</th>
            <th>Votaron</th>
            <th>% Votaron</th>
        </tr>
        </thead>
        <tbody>
        <tr class="candidate">
            <td class="text-center">1525</td>
            <td class="text-center">1346</td>
            <td class="text-center">88</td>
        </tr>

        </tbody>
    </table>
</div>
<h2 class="election-name">ELECCIÓN DE COMITÉ NACIONAL E INTERNACIONAL</h2>
<div>
    <div class="row mt-3 mb-3 question question-1">
        <div class="col-12 flex-column align-items-center d-flex mb-4">
            <h3 class="question-title">1. Comité región I</h3>
            <table class="table table-results-elections">
                <thead>
                <tr>
                    <th class="col-order">Lista N°</th>
                    <th class="col-candidate text-left">Partido</th>
                    <th class="col-candidate text-left">Candidato</th>
                    <th class="col-votes">Votos</th>
                </tr>
                </thead>
                <tbody>
                    <tr class="candidate">
                        <td class="col-order list text-center"><span>2</span></td>
                        <td class="col-candidate list-"><span>DER</span></td>
                        <td class="col-candidate">Erick Runte Cassin</td>
                        <td class="col-votes text-center">768</td>
                    </tr>
                    <tr class="candidate">
                        <td class="col-order list text-center"><span>4</span></td>
                        <td class="col-candidate list-"><span>ACR</span></td>
                        <td class="col-candidate">Sunny Connelly Trantow</td>
                        <td class="col-votes text-center">491</td>
                    </tr>
                    <tr class="candidate">
                        <td class="col-order list text-center"><span>5</span></td>
                        <td class="col-candidate list-"><span>TPO</span></td>
                        <td class="col-candidate">Robert Kessler Runte</td>
                        <td class="col-votes text-center">206</td>
                    </tr>
                    <tr class="candidate">
                        <td class="col-order list text-center"><span>-</span></td>
                        <td class="col-candidate list-"><span>-</span></td>
                        <td class="col-candidate">Voto en blanco</td>
                        <td class="col-votes text-center">42</td>
                    </tr>
                    <tr class="candidate">
                        <td class="col-order list text-center"><span>-</span></td>
                        <td class="col-candidate list-"><span>-</span></td>
                        <td class="col-candidate">Voto viciado</td>
                        <td class="col-votes text-center">18</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>

<script>
    /*
        $(function() {
            //ADD CONTENT TO WHITE/NULLED VOTING
            $(".list-250, .list-251").html("-");

            $( ".question" ).each(function() {
                let number_candidates = $(this).find(".candidate").length;
                $( this ).find(".list-order-251").html(number_candidates);
                $( this ).find(".list-order-250").html(--number_candidates);
            });
        });
    */
</script>
