@extends('layouts.app', ['page' => 'groups-create'])

@section('content')
    <div class="container container-form">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Agregar grupo de electores"),
                    'icon' => "users"
                ])
            </div>
        </div>
        <form
            method="POST"
            action="{{ route('groups.store') }}"
            novalidate
            enctype="multipart/form-data"
        >
            @csrf

            <div class="form-group">
                <label for="name">Nombre de grupo de electores (debe ser único)</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('name') ? 'is-invalid': '' }}"
                    name="name"
                    id="name"
                    placeholder=""
                    value="{{ old('name') ?: "" }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group input-files-group">
                <label for="">Seleccione el archivo de electores</label><br>
                <div>El archivo debe estar en formato CSV con los siguientes campos obligatorios: nombres, apellido paterno, apellido materno, correo, número de documento de identidad</div>
                @error('electors_list.*')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror


                <div class="input-group-wrapp">
                    <div class="input-group">
                        <div class="custom-file">
                            <input
                                type="file"
                                class="custom-file-input form-control {{ $errors->has('electors_list') ? 'is-invalid': '' }}"
                                name="electors_list[]"
                                id="electors_list_1"
                                placeholder=""
                                multiple
                                value=""
                            >
                            <label  class="custom-file-label" for="electors_list_1">
                                Buscar archivo
                            </label>
                        </div>
                        <div class="input-group-append remove-btn-wrapp d-none">
                            <div class="btn btn-outline-danger remove-btn"><i class="fa fa-remove"></i></div>
                        </div>
                        @if($errors->has('electors_list'))
                            <span class="invalid-feedback">
                            <strong>{{ $errors->first('electors_list') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

            </div>


            <div class="form-group">
                <div class="btn btn-outline-info btn-add-file">Agregar archivo de electores</div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __("Añadir Electores") }}
                </button>
            </div>

        </form>

    </div>
    <div class="clone d-none">

        <div class="input-group-wrapp">
            <div class="input-group">
                <div class="custom-file">
                    <input
                        type="file"
                        class="custom-file-input form-control {{ $errors->has('electors_list') ? 'is-invalid': '' }}"
                        name="electors_list[]"
                        id="electors_list[]"
                        placeholder=""
                        multiple
                        value=""
                    >
                    <label  class="custom-file-label" for="electors_list[]">
                        Buscar archivo
                    </label>
                </div>
                <div class="input-group-append remove-btn-wrapp">
                    <div class="btn btn-outline-danger remove-btn"><i class="fa fa-remove"></i></div>
                </div>
                @if($errors->has('electors_list'))
                    <span class="invalid-feedback">
                            <strong>{{ $errors->first('electors_list') }}</strong>
                        </span>
                @endif
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>

        $(document).ready(function() {

            $(".btn-success").click(function(){
                var html = $(".clone").html();
                $(".increment").after(html);
            });

            $("body").on("click",".btn-danger",function(){
                $(this).parents(".control-group").remove();
            });

        });
        /*
        * Función para agregar el nombre del archivo
        * al input file
        */
        $(document).on('change', '.custom-file-input', function(){
            console.log("Cambió");
            var filename = $(this).val().split('\\').pop();
            console.log(filename);

            $(this).closest(".input-group-wrapp").addClass("input-group-wrapp-selected");

            $(this).siblings('.custom-file-label').html("<i>"+filename+"</i>");
        });

        $(document).on("click",".remove-btn",function(){
            console.log("click");
            $(this).closest(".input-group-wrapp").remove();
        });
        $(document).on("click",".btn-add-file",function(){
            console.log("click");
            var html = $(".clone").html();
            $(".input-files-group").append(html);
        });

    </script>
@endpush
