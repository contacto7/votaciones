@extends('layouts.app', ['page' => 'groups'])

@section('content')
    <div class="container">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row justify-content-center">
            <div class="w-100 text-center">
                <h5 class="text-uppercase">Lista de electores en {{ $group->name }} - {{ $electors_count }}</h5>
                <form action="{{ route('groups.electorsSearch') }}" method="get" class="form-inline form-search">
                    <div class="form-group row d-none">
                        <label
                            for="group_id_search"
                            class="col-sm-3 col-form-label"
                        >
                            Grupo de electores
                        </label>
                        <div class="col-sm-9">
                            <input
                                class="form-control"
                                name="group_id_search"
                                id="group_id_search"
                                type="number"
                                value="{{ $group->id }}"
                                placeholder="{{ __("Buscar por nombre") }}"
                            >
                        </div>
                    </div>
                    <div class="input-group input-group-search">
                        <input
                            class="form-control"
                            name="name_search"
                            id="name_search"
                            type="text"
                            placeholder="{{ __("Buscar electores") }}"
                        >
                        <div class="input-group-append">
                            <input
                                class="btn btn-outline-secondary"
                                name="filter"
                                type="submit"
                                value="Buscar"
                            >
                        </div>
                    </div>
                </form>
            </div>
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th class="col-candidate">Elector</th>
                    <th class="col-candidate">DNI</th>
                    <th class="col-candidate">Correo</th>
                    <th class="col-candidate">Peso</th>
                </tr>
                </thead>
                <tbody>
                @forelse($electors as $elector)
                    <tr>
                        <td class="col-candidate">
                            <a href="{{ route('electors.info', $elector->id) }}">
                                {{ $elector->name." ".$elector->fathers_last_name." ".$elector->mothers_last_name }}
                            </a>
                        </td>
                        <td class="col-candidate">{{ $elector->document_number }}</td>
                        <td class="col-candidate">{!! $elector->email."<br>"."+".$elector->country_code." ".$elector->cellphone !!}</td>
                        <td class="col-candidate">{{ $elector->pivot->weight }}</td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("Aún no hay electores seleccionados")}}</td>
                    </tr>
                @endforelse

                </tbody>
            </table>
        </div>
        <div class="row justify-content-center mb-4">
            {{ $electors->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
