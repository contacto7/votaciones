@extends('layouts.app', ['page' => 'groups'])

@section('content')
    <div class="container">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row d-none">
            @include('partials.groups.search')
        </div>
        <div class="row justify-content-center">
            <div class="w-100 text-center">
                <h5>
                    @can('create', [\App\Group::class])
                        <a
                            class="form-control btn btn-agregar-sistema"
                            href="{{ route('groups.create') }}"
                        >Añadir</a>
                    @endcan
                    GRUPOS DE ELECTORES
                </h5>
                <form action="{{ route('groups.search') }}" method="get" class="form-inline form-search">
                    <div class="input-group input-group-search">
                        <input
                            class="form-control"
                            name="name"
                            id="name"
                            type="text"
                            placeholder="{{ __("Buscar grupo de electores") }}"
                        >
                        <div class="input-group-append">
                            <input
                                class="btn btn-outline-secondary"
                                name="filter"
                                type="submit"
                                value="Buscar"
                            >
                        </div>
                    </div>
                </form>
            </div>
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col" class="d-none">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Electores</th>
                    <th scope="col">Creación</th>
                    <th scope="col">Creó</th>
                </tr>
                </thead>
                <tbody>
                @forelse($groups as $group)
                    <tr class="group-{{ $group->id }}">
                        <td class="d-none">{{ $group->id }}</td>
                        <td><a href="{{ route('groups.info', $group->id) }}">{{ $group->name }}</a></td>
                        <td class="group-count">
                            {{ $group->electors()->count() }}

                        </td>
                        <td>
                            A las {{ \Carbon\Carbon::parse($group->created_at )->format('H:i')}} del
                            {{ \Carbon\Carbon::parse($group->created_at )->format('d/m/Y')}}
                        </td>
                        <td>
                            {{ $group->user->name." ".$group->user->fathers_last_name }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay votaciones disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $groups->appends(request()->except('page'))->links() }}
        </div>
    </div>
    @if(session('groupMessage'))
        <div class="modal" id="groupMessage">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Importante</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content">

                        <div class="modal-body-msg-wrapp modal-group-msg-wrp">
                            Los electores se están creando, por favor no cierre esta ventana o la página hasta que terminen de cargar al sistema.
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(window).on('load',function(){
                $('#groupMessage').modal('show');
            });
        </script>
    @endif

@endsection


@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script>
        $(function () {
            let group = 0;
            let user_activity_log = 0;

            $('#datetimepicker1').datetimepicker({
                format: 'YYYY-MM-DD',
                locale: 'es',
                sideBySide: true,
                buttons: {showClose: true},
                showClose: true,
                icons: {
                    close: 'fa fa-check'
                }
            });



            @if(session('groupMessage'))
                group = {!! session('groupMessage')[0] !!};
                user_activity_log = {!! session('groupMessage')[1] !!};
                $(".group-"+group+" .group-count").html("cargando...");
                getUploadStatus();

            @endif



            function getUploadStatus(){
                //Enviamos una solicitud con el ruc de la empresa
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: "{{ route('groups.uploadStatusAjax') }}",
                    data:{
                        user_activity_log: user_activity_log,
                    },
                    dataType: "json",
                    success: function (data) {

                        console.log(data);
                        $(".modal-group-msg-wrp").html(data.message);

                        if(data.action == "continue"){
                            setTimeout(getUploadStatus, 500);
                        }else{

                            if(data.status == "normal"){
                                console.log("Normal break");
                                $(".modal-group-msg-wrp").append("<br>La página se actualizará en breve, por favor aún no cierre esta ventana.");
                                setTimeout(function(){
                                    window.location.reload(1);
                                }, 5000);
                            }else if(data.status == "error"){
                                console.log("Error break");
                                $(".modal-group-msg-wrp").append("<br>La página se actualizará en breve, por favor aún no cierre esta ventana.");
                                setTimeout(function(){
                                    window.location.reload(1);
                                }, 15000);
                            }
                            setTimeout(function(){
                                //window.location.reload(1);
                            }, 5000);
                        }

                    },error:function(data){
                        console.log("-----------------ERROR----------------");
                        console.log(data);

                        setTimeout(getUploadStatus, 500);
                        console.log("-----------------ERROR----------------");
                    }
                });
            }

        });

    </script>
@endpush
