@extends('layouts.app')

@section('content')
    <div class="container d-flex justify-content-center align-items-center">
        <div class="row justify-content-center align-items-center">
            <div class="welcome-msg-wrapp text-center">
                <div class="welcome-msg-inn">

                    <img
                        src="{{ asset('image/logo.svg') }}"
                        alt="Logo de Thales"
                        class="logo-welcome logo-sistema">
                    <div class="welcome-msg-txt">¡Bienvenido al sistema de Votaciones!</div>

                </div>
            </div>
        </div>
    </div>
@endsection
