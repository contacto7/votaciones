@component('mail::message')
Estimado(a) {{ $elector->name }}, se le ha registrado para votar en las elecciones "{{ $poll->name }}"

La votación estará abierta en el siguiente horario:<br>
Inicio: {{ \Carbon\Carbon::parse($poll->start_date )->format('d/m/Y')}} a las {{ \Carbon\Carbon::parse($poll->start_date )->format('H:i')}}<br>
Fin: {{ \Carbon\Carbon::parse($poll->end_date )->format('d/m/Y')}} a las {{ \Carbon\Carbon::parse($poll->end_date )->format('H:i')}}

Podrá hacer su votación, en el horario indicado, haciendo clic en el siguiente botón:

@php
$sep = \App\Vote::DATA_SEPARATOR;
$poll_id_elector_id_token_mail_auth_factors = $poll->id.$sep.$elector->id.$sep.$elector->token_mail.$sep.$poll->auth_factors;
$encrypted_data = Crypt::encryptString($poll_id_elector_id_token_mail_auth_factors);
@endphp

@component('mail::button', ['url' => route('votes.insertDni',['poll_id_elector_id_token_mail'=>$encrypted_data] ) ])
IR A VOTAR
@endcomponent

También puedes copiar y pegar este link en el navegador:<br>
<a style="word-break:break-all;"  href="{{ route('votes.insertDni',['poll_id_elector_id_token_mail'=>$encrypted_data] ) }}">{{ route('votes.insertDni',['poll_id_elector_id_token_mail'=>$encrypted_data] ) }}</a>

Gracias,<br>

{{ env("APP_NAME", "Sistema de votaciones") }}
@endcomponent
