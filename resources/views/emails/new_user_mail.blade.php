@component('mail::message')
Estimado(a) {{ $user->name }},

Se le ha registrado en el sistema de votaciones de la UNHEVAL, por favor confirme su correo, y cree una contraseña en el siguiente link

@component('mail::button', ['url' => route('users.confirmMail',['user_id'=>Crypt::encryptString($user->id), 'token_mail'=>Crypt::encryptString($user->token_mail)] ) ])
    Confirmar correo y crear contraseña
@endcomponent

Gracias,<br>

{{ env("APP_NAME", "Sistema de votaciones") }}
@endcomponent
