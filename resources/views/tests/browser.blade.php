@extends('layouts.app', ['page' => 'polls'])

@section('content')
    <div class="uuid"></div>
    <div>Mac: {{ $mac }}</div>
    <div>IP: {{ $ip }}</div>
@endsection
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/device-uuid.js') }}"></script>
    <script>
        $(function() {
            var uuid = new DeviceUUID().get();
            console.log(uuid);
            $(".uuid").html(uuid);

        });

    </script>

@endpush
