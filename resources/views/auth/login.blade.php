@extends('layouts.app', ['page' => 'login'])

@section('content')
    <div class="container-fluid d-flex h-100">
        <div class="row d-flex flex-wrap flex-grow-1">
            <div class="col-sm-7 d-flex justify-content-center align-items-center col-welcome-brand">
                <img
                    class="lazy logo-footer lazy-loaded logo-sistema"
                    src="{{ asset('image/logo.svg') }}"
                    data-lazy-type="image"
                    data-lazy-src="/wp-content/uploads/2019/05/logo.svg"
                    alt="logo">
            </div>
            <div class="col-sm-5 d-flex justify-content-center align-items-center col-welcome-login">

                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="form-group row">
                        <div class="col-md-12">
                            <input
                                id="email"
                                type="email"
                                class="form-control @error('email') is-invalid @enderror @error('active') is-invalid @enderror"
                                name="email" value="{{ old('email') }}"
                                required
                                autocomplete="email"
                                placeholder="Ingrese su correo"
                                autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror

                            @error('active')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <input
                                id="password"
                                type="password"
                                class="form-control @error('password') is-invalid @enderror"
                                name="password"
                                required
                                placeholder="Ingrese  su contraseña"
                                autocomplete="current-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-12 justify-content-center">
                            <button type="submit" class="btn btn-thales-secondary">
                                {{ __('Ingresar') }}
                            </button>
                            @if (Route::has('password.request'))
                                <a class="btn btn-link link-white" href="{{ route('password.request') }}">
                                    {{ __('Olvidé mi contraseña') }}
                                </a>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>    <div class="modal" id="modalMensaje">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">IMPORTANTE</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body modal-ajax-content">

                    <div class="modal-body-msg-wrapp">
                        <div>Este acceso es SÓLO para los administradores del sistema de votación digital.</div><br>
                        <h4>Entonces, ¿Cómo y donde voto?</h4>
                        <div>Los pasos para votar son los siguientes:</div>
                        <ol>
                            <li>Ir a <a href="http://hotmail.com/" target="_blank">http://hotmail.com/</a></li>
                            <li>Ingresar con su CORREO INSTITUCIONAL.</li>
                            <li>Entrar al mensaje enviado por el Sistema de Votaciones.</li>
                            <li>Hacer click en el botón "Ir a Votar" dentro del mensaje.</li>
                            <li>Ingresar su DNI (los alumnos de postgrado deben ingresar su código de alumno).</li>
                            <li>Votar.</li>
                        </ol>
                        <br>
                        <h4>¿No puedes acceder a tu correo?</h4>
                        <div>Envía un correo a informatica@unheval.pe</div>

                        <br>
                        <h4>¿Tienes problemas al efectuar tu voto virtual?</h4>
                        <div>Envía un correo a sistema.votaciones@unheval.edu.pe</div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(window).on('load',function(){
            //$('#modalMensaje').modal('show');
        });
    </script>
@endsection
