@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.emailActivityLogs.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Elector</th>
                    <th scope="col">Acción</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Estado</th>
                </tr>
                </thead>
                <tbody>
                @forelse($emailActivityLogs as $emailActivityLog)
                    <tr>
                        <td>{{ $emailActivityLog->id }}</td>
                        <td>
                            {{ $emailActivityLog->electorReceived->name." ".$emailActivityLog->electorReceived->fathers_last_name }}<br>
                            {{ $emailActivityLog->email }}
                        </td>
                        <td class="first-letter-uppercase">{{ $emailActivityLog->action->name }}</td>
                        <td class="first-letter-uppercase">{{ $emailActivityLog->description }}</td>
                        <td>
                            @if($emailActivityLog->state == \App\EmailActivityLog::RECEIVED)
                                Procesado a las {{ \Carbon\Carbon::parse($emailActivityLog->sent_at )->format('H:i')}} del
                                {{ \Carbon\Carbon::parse($emailActivityLog->sent_at )->format('d/m/Y')}}
                            @elseif($emailActivityLog->state == \App\EmailActivityLog::ERROR)
                                <b class="alert-danger p-1">Error al procesar<b>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("Aún no se han enviado correos")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $emailActivityLogs->appends(request()->except('page'))->links() }}
        </div>
        <div class="row justify-content-center">
            <a
                class="btn btn-outline-info"
                href="{{ route('polls.info', $poll->id) }}"
                data-toggle="tooltip"
                data-placement="top"
                title=""
                data-original-title="Ver votación">
                Regresar al grupo de votaciones
            </a>

        </div>
    </div>
@endsection


