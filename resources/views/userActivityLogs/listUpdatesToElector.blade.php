@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => 'Registro de modificaciones a elector',
                    'icon' => "user"
                ])
            </div>
        </div>
        <div class="row justify-content-center">
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Usuario que modificó</th>
                    <th scope="col">ID</th>
                    <th scope="col">Realizado</th>
                </tr>
                </thead>
                <tbody>
                @forelse($userActivityLogs as $userActivityLog)
                    <tr>
                        <td>{{ $userActivityLog->id }}</td>
                        <td>{{ $userActivityLog->description }}</td>
                        <td>{{ $userActivityLog->getModel()->name." ".$userActivityLog->getModel()->fathers_last_name }}</td>
                        <td>{{ $userActivityLog->model_id }}</td>
                        <td>{{ $userActivityLog->created_at }}</td>

                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No se registran acciones")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $userActivityLogs->appends(request()->except('page'))->links() }}
        </div>
        <div class="row justify-content-center">
            <a
                class="btn btn-outline-info"
                href="{{ route('electors.list') }}"
                data-toggle="tooltip"
                data-placement="top"
                title=""
                data-original-title="Ver votación">
                Ver lista de usuarios
            </a>

        </div>
    </div>
@endsection


