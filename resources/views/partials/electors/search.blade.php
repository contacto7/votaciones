<div class="head-page">
    @include('partials.title', [
        'title' => 'Búsqueda de Electores',
        'icon' => "user"
    ])
    <form action="{{ route('electors.search') }}" method="get" class="col-sm-12 form-search-villamares">
        <div class="form-group row">
            <label
                for="name_search"
                class="col-sm-3 col-form-label"
            >
                Nombres
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="name_search"
                    id="name_search"
                    type="text"
                    placeholder="{{ __("Buscar por nombre") }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <label
                for="last_name_search"
                class="col-sm-3 col-form-label"
            >
                Apellidos
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="last_name_search"
                    id="last_name_search"
                    type="text"
                    placeholder="{{ __("Buscar por apellido") }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <label
                for="document_number_search"
                class="col-sm-3 col-form-label"
            >
                Documento de identidad
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="document_number_search"
                    id="document_number_search"
                    type="text"
                    placeholder="{{ __("Buscar por documento de identidad") }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3 offset-sm-3">
                <input
                    class="form-control btn btn-buscar"
                    name="filter"
                    type="submit"
                    value="Buscar"
                >
            </div>
        </div>
    </form>
</div>



