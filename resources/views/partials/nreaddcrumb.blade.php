
<nav class="breadcrumb-nav">
    <ol class="breadcrumb">
        @foreach($bread_arr as $bread)
            @if($bread->type == "link")
                <li class="breadcrumb-item"><a href="{{ $bread->route }}">{{ $bread->text }}</a></li>
            @elseif($bread->type == "active")
                <li class="breadcrumb-item active">{{ $bread->text }}</li>
            @endif
        @endforeach
    </ol>
</nav>
