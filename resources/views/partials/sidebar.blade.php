<!--
Sidebar
Example of: https://bootstrapious.com/p/bootstrap-sidebar
-->
<nav id="sidebar">
    <a class="sidebar-header" href="{{ url('/') }}">
        <img
            src="{{ asset('image/logo.svg') }}"
            alt="Logo de Thales"
            class="logo-thales logo-complete logo-sistema">
        <img
            src="{{ asset('image/logo.svg') }}"
            alt="Logo de Thales"
            class="logo-thales logo-partial logo-sistema">
    </a>

    <ul class="list-unstyled components">
        @include('partials.sidebar.' . \App\User::navigation())
    </ul>

</nav>
