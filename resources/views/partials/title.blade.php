<div class="title-row">
    <div class="col-md-12">
        <div class="pt-4">
            <h1 class="pt-1 mb-4 font-bold text-center">
                <i class="fa fa-{{ $icon }} fa-title d-none"></i>
                {{ $title }}
            </h1>

        </div>
    </div>
</div>
