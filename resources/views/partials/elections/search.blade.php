<div class="head-page">
    @include('partials.title', [
        'title' => $poll->name,
        'icon' => "user"
    ])
    <form action="{{ route('elections.search') }}" method="get" class="col-sm-12 form-search-villamares">
        <div class="form-group row">
            <label
                for="name"
                class="col-sm-3 col-form-label"
            >
                Nombres
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="name"
                    id="name"
                    type="text"
                    placeholder="{{ __("Ingrese el nombre") }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3 offset-sm-3">
                <input
                    class="form-control btn btn-buscar"
                    name="filter"
                    type="submit"
                    value="Buscar"
                >
            </div>
        </div>
    </form>
</div>



