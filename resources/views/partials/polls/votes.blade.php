<table class="table table-hover table-light">
    <thead>
    <tr>
        <th class="d-none">ID voto</th>
        <th>Pregunta</th>
        <th>Candidato elegido</th>
        <th>Registrado</th>
    </tr>
    </thead>
    <tbody>
    @forelse($votes as $vote)
        <tr>
            <td class="d-none">{{ $vote->id }}</td>
            <td>{{ $vote->candidate->question->name }}</td>
            <td>
                @if($vote->candidate->order == 251 || $vote->candidate->order == 250)
                    {{ $vote->candidate->list_name }}
                @else
                    {{ $vote->candidate->list_name." / ".$vote->candidate->name }}
                @endif
            </td>
            <td>{{ $vote->created_at }}</td>
        </tr>
    @empty
        <tr>
            <td>{{ __("Aún no hay votos")}}</td>
        </tr>
    @endforelse

    </tbody>
</table>
<div class="row justify-content-center mb-4 model-pagination">
    {{ $votes->appends(request()->except('page'))->links() }}
</div>
