<table class="table table-hover table-light">
    <thead>
    <tr>
        <th scope="col" class="d-none">#</th>
        <th scope="col">Votación
            @can('create', [\App\Election::class])
                @can('updateAnymore', [\App\Poll::class, $poll])
                    <a
                        class="form-control btn btn-agregar-sistema float-none"
                        href="{{ route('elections.create', ['id'=>$poll->id]) }}"
                    >Añadir</a>
                @endcan
            @endcan</th>
        <th scope="col">Grupo de electores</th>
        <th scope="col">Votaron / Total electores<br>{{ $votesPollCount }} / {{ $electorsPollCount }} </th>
    </tr>
    </thead>
    <tbody>
    @forelse($elections as $election)
        @php
            $election_id = $election->id;
            $group = $election->group;
            $group_id = $group->id;
            $votesCount = \App\Elector::with(['groups'])
                ->whereHas("polls", function ($q) use ($election_id) {
                    $q
                        ->whereHas("elections", function ($q) use ($election_id) {
                            $q->where("id", $election_id);
                        })
                        ->where("elector_poll.state", \App\Poll::VOTED);
                })
                ->whereHas("groups", function ($q) use ($group_id) {
                    $q->where("id", $group_id);
                })
                ->count();

        @endphp
        <tr>
            <td class="d-none">{{ $election->id }}</td>
            <td><a href="{{ route('elections.info', $election->id) }}">{{ $election->name }}</a></td>
            <td>
                <a href="{{ route('groups.info', $election->group->id) }}">{{ $election->group->name }}</a>
            </td>
            <td>{{ $votesCount }} / {{ $election->group->electors()->count() }}</td>
        </tr>
    @empty
        <tr>
            <td colspan="3">{{ __("Aún no se han agregado elecciones al grupo de votaciones")}}</td>
        </tr>
    @endforelse
    </tbody>
</table>
<div class="row justify-content-center model-pagination">
    {{ $elections->appends(request()->except('page'))->links() }}
</div>
