<div class="input-group col-md-12 pt-2">
    <input
        class="form-control py-2 border-right-0 border search_input"
        type="search"
        placeholder="Buscar Elector por nombres, apellidos, DNI o correo"
        id="example-search-input"
        data-href="{{ route('polls.pollElectorsAjax', $poll->id) }}"
    >
    <span class="input-group-append">
        <button class="btn btn-outline-secondary border-left-0 border search-button" type="button">
            <i class="fa fa-search"></i>
        </button>
    </span>
</div>
<table class="table table-hover table-light">
    <thead>
    <tr>
        <th class="col-candidate">Elector</th>
        <th class="col-candidate">DNI</th>
        <th class="col-candidate">Contacto</th>
        <th class="col-candidate">Votó</th>
        <th class="col-candidate">Correo</th>
    </tr>
    </thead>
    <tbody>
    @forelse($electors as $elector)
        <tr>
            <td class="col-candidate">
                <a href="{{ route('electors.info', $elector->id) }}">{{ $elector->name." ".$elector->fathers_last_name." ".$elector->mothers_last_name }}</a>
            </td>
            <td class="col-candidate">{{ $elector->document_number }}</td>
            <td class="col-candidate">
                {!! $elector->email !!}
                @if($elector->cellphone)
                    <br>
                    + {!! $elector->country_code." ".$elector->cellphone !!}
                @endif
            </td>
            <td class="col-candidate">
                @if($elector->pivot->state == \App\Poll::VOTED)

                    <span class="text-success">Si ({{ \Carbon\Carbon::parse($elector->pivot->voted_at )->format('H')}}h del
                    {{ \Carbon\Carbon::parse($elector->pivot->voted_at )->format('d/m/Y')}})</span>
                @else()

                    No
                @endif

            </td>
            <td class="col-candidate">
                @if($elector->pivot->state == \App\Poll::VOTED)
                @else()

                    @can('sendEmails', [\App\Poll::class])
                        <a
                            class="btn btn-outline-info"
                            href="{{ route('polls.sendRecordatoryMail',['poll_id'=>$poll->id,'elector_id'=>$elector->id ] ) }}"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="Ver detalle de votos"
                        >
                            Enviar
                        </a>
                    @else
                            -
                    @endcan
                @endif
            </td>
        </tr>
    @empty
        <tr>
            <td colspan="4">{{ __("Aún no hay electores para este grupo, seleccione el grupo de electores para cada votación.")}}</td>
        </tr>
    @endforelse

    </tbody>
</table>

<div class="row justify-content-center mb-4 model-pagination">
    {{ $electors->appends(request()->except('page'))->links() }}
</div>
