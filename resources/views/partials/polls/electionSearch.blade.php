<div class="head-page">
    @include('partials.title', [
        'title' => "Búsqueda de Votaciones en ".$poll->name,
        'icon' => "user"
    ])
    <form action="{{ route('polls.electionSearch') }}" method="get" class="col-sm-12 form-search-villamares">
        <div class="form-group row d-none">
            <label
                for="poll_id"
                class="col-sm-3 col-form-label"
            >
                Grupo de elecciones
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="poll_id"
                    id="poll_id"
                    type="number"
                    value="{{ $poll->id }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <label
                for="name"
                class="col-sm-3 col-form-label"
            >
                Votación
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="name"
                    id="name"
                    type="text"
                    placeholder="{{ __("Buscar por nombre de votación") }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3 offset-sm-3">
                <input
                    class="form-control btn btn-buscar"
                    name="filter"
                    type="submit"
                    value="Buscar"
                >
            </div>
        </div>
    </form>
</div>



