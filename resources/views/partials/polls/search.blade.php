<div class="head-page">
    @include('partials.title', [
        'title' => __("Búsqueda de Grupos de votaciones"),
        'icon' => "user"
    ])
    <form action="{{ route('polls.search') }}" method="get" class="col-sm-12 form-search-villamares">
        <div class="form-group row">
            <label
                for="name"
                class="col-sm-3 col-form-label"
            >
                Nombre
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="name"
                    id="name"
                    type="text"
                    placeholder="{{ __("Buscar grupo de votaciones por nombre") }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <label
                for="document_date_filter"
                class="col-sm-3 col-form-label"
            >
                Fecha de creación
            </label>
            <div class="col-sm-9">
                <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                    <input
                        type="text"
                        name="document_date_filter"
                        id="document_date_filter"
                        class="form-control datetimepicker-input"
                        data-target="#datetimepicker1"
                        data-toggle="datetimepicker"
                        value=""
                        autocomplete="off"
                        placeholder="{{ __("Buscar grupo de votaciones por fecha") }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>

            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3 offset-sm-3">
                <input
                    class="form-control btn btn-buscar"
                    name="filter"
                    type="submit"
                    value="Buscar"
                >
            </div>
        </div>
    </form>
</div>



