<div class="input-group col-md-12 pt-2">
    <input
        class="form-control py-2 border-right-0 border search_input"
        type="search"
        placeholder="Buscar correo por nombres, apellidos, DNI o correo de elector"
        data-href="{{ route('emailActivityLogs.listAjax', $poll->id) }}"
    >
    <span class="input-group-append">
        <button class="btn btn-outline-secondary border-left-0 border search-button" type="button">
            <i class="fa fa-search"></i>
        </button>
    </span>
</div>
<table class="table table-hover table-light">
    <thead>
    <tr>
        <th scope="col" class="d-none">#</th>
        <th scope="col">Elector</th>
        <th scope="col">Acción</th>
        <th scope="col">Descripción</th>
        <th scope="col">Estado</th>
    </tr>
    </thead>
    <tbody>
    @forelse($emailActivityLogs as $emailActivityLog)
        <tr>
            <td class="d-none">{{ $emailActivityLog->id }}</td>
            <td>

                <a href="{{ route('electors.info', $emailActivityLog->electorReceived->id) }}">
                    {{ $emailActivityLog->electorReceived->name." ".$emailActivityLog->electorReceived->fathers_last_name." ".$emailActivityLog->electorReceived->mothers_last_name }}</a><br>
                {{ $emailActivityLog->email }}
            </td>
            <td class="first-letter-uppercase">{{ $emailActivityLog->action->name }}</td>
            <td class="first-letter-uppercase">{{ $emailActivityLog->description }}</td>
            <td>
                @if($emailActivityLog->state == \App\EmailActivityLog::RECEIVED)
                    Procesado a las {{ \Carbon\Carbon::parse($emailActivityLog->sent_at )->format('H:i')}} del
                    {{ \Carbon\Carbon::parse($emailActivityLog->sent_at )->format('d/m/Y')}}
                @elseif($emailActivityLog->state == \App\EmailActivityLog::ERROR)
                    <b class="alert-danger p-1">Error al procesar<b>
                @endif
            </td>
        </tr>
    @empty
        <tr>
            <td>{{ __("Aún no se han enviado correos")}}</td>
        </tr>
    @endforelse
    </tbody>
</table>

<div class="row justify-content-center model-pagination">
    {{ $emailActivityLogs->appends(request()->except('page'))->links() }}
</div>
