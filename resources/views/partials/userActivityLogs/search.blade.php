<div class="head-page">
    @include('partials.title', [
        'title' => 'Registro de actividades de usuario',
        'icon' => "user"
    ])
    <form action="{{ route('userActivityLogs.search') }}" method="get" class="col-sm-12 form-search-villamares">
        <div class="form-group row d-none">
            <label
                for="name_search"
                class="col-sm-3 col-form-label"
            >
                Grupo de votaciones
            </label>
            <div class="col-sm-9">
                <input
                    class="form-control"
                    name="user_id"
                    id="user_id"
                    type="number"
                    value="{{ $user->id }}"
                    placeholder="{{ __("Ingrese el usuario") }}"
                >
            </div>
        </div>
        <div class="form-group row">
            <label
                for="action_id_search"
                class="col-sm-3 col-form-label"
            >
                Acción
            </label>
            <div class="col-sm-9">

                <select

                    class="form-control {{ $errors->has('action_id_search') ? 'is-invalid': '' }}"
                    name="action_id_search"
                    id="action_id_search"
                >
                    @foreach(\App\Action::orderBy("name",'ASC')->pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('action_id_search') === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3 offset-sm-3">
                <input
                    class="form-control btn btn-buscar"
                    name="filter"
                    type="submit"
                    value="Buscar"
                >
            </div>
        </div>
    </form>
</div>



