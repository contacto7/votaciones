<li class="nav-polls">
    <a href="{{ route('polls.list') }}"><span class="icon-navbar icon-votaciones"></span> Grupos de votaciones</a>
</li>
<li class="">
    <a href="#groupsSubmenuu" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">
        <span class="icon-navbar icon-electores"></span>
        <div class="large-word-dropdown">Grupos de electores</div>
    </a>
    <ul class="list-unstyled" id="groupsSubmenu">
        <li class="nav-groups">
            <a href="{{ route('groups.list') }}">Ver grupos</a>
        </li>
        <li class="nav-groups-create">
            <a href="{{ route('groups.create') }}">Añadir grupo de electores</a>
        </li>
        <li class="nav-electors">
            <a href="{{ route('electors.list') }}">Ver electores</a>
        </li>
    </ul>
</li>
<li class="nav-users">
    <a href="{{ route('users.list') }}"><span class="icon-navbar icon-usuarios"></span> Usuarios</a>
</li>
@include('partials.sidebar.logged')
