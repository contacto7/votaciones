<li class="nav-polls">
    <a href="{{ route('polls.list') }}"><i class="fa fa-files-o"></i> Grupos de votaciones</a>
</li>
<li class="">
    <a href="#groupsSubmenuu" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">
        <i class="fa fa-files-o"></i>
        <div class="large-word-dropdown">Grupos de electores</div>
    </a>
    <ul class="list-unstyled" id="groupsSubmenu">
        <li class="nav-groups">
            <a href="{{ route('groups.list') }}">Ver grupos</a>
        </li>
        <li class="nav-electors">
            <a href="{{ route('electors.list') }}">Ver electores</a>
        </li>
    </ul>
</li>
<li class="nav-users">
    <a href="{{ route('users.list') }}"><i class="fa fa-users"></i> Usuarios</a>
</li>
@include('partials.sidebar.logged')
