<li class="">
    <a href="#userSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <span class="icon-navbar icon-usuario"></span>
        {{ explode(' ',trim(auth()->user()->name))[0] }}
    </a>
    <ul class="collapse list-unstyled" id="userSubmenu">
        <li>
            <a
                href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();"

            >Cerrar sesión</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</li>
