@extends('layouts.app', ['page' => 'users'])

@section('content')
    <div class="container container-form">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Usuario"),
                    'icon' => "user"
                ])
            </div>
        </div>
        <form
            method="POST"
            action="{{ ! $user->id ? route('users.store'): route('users.update', ['id'=>$user->id]) }}"
            novalidate
            enctype="multipart/form-data"
        >
            @if($user->id)
                @method('PUT')
            @endif

            @csrf

            <div class="form-group">
                <label for="role_id">Rol</label>
                <select
                    style="text-transform: capitalize"
                    class="form-control {{ $errors->has('role_id') ? 'is-invalid': '' }}"
                    name="role_id"
                    id="role_id"
                >
                    @foreach(\App\Role::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('role_id') === $id || $user->role_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="name">Nombres</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('name') ? 'is-invalid': '' }}"
                    name="name"
                    id="name"
                    placeholder=""
                    value="{{ old('name') ?: $user->name }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="fathers_last_name">Apellido paterno</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('fathers_last_name') ? 'is-invalid': '' }}"
                    name="fathers_last_name"
                    id="name"
                    placeholder=""
                    value="{{ old('fathers_last_name') ?: $user->fathers_last_name }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('fathers_last_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('fathers_last_name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="mothers_last_name">Apellido materno</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('mothers_last_name') ? 'is-invalid': '' }}"
                    name="mothers_last_name"
                    id="mothers_last_name"
                    placeholder=""
                    value="{{ old('mothers_last_name') ?: $user->mothers_last_name }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('mothers_last_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('mothers_last_name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="document_number">Número de documento de identidad</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('document_number') ? 'is-invalid': '' }}"
                    name="document_number"
                    id="document_number"
                    placeholder=""
                    value="{{ old('document_number') ?: $user->document_number }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('document_number'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('document_number') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="cellphone">Número de celular</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('cellphone') ? 'is-invalid': '' }}"
                    name="cellphone"
                    id="cellphone"
                    placeholder=""
                    value="{{ old('cellphone') ?: $user->cellphone }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('cellphone'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('cellphone') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="email">Correo</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('email') ? 'is-invalid': '' }}"
                    name="email"
                    id="email"
                    placeholder=""
                    value="{{ old('email') ?: $user->email }}"
                    required
                    autocomplete="off"
                    @if($user->id) disabled @endif
                >
                @if($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            @if($user->id)
            <div class="form-group">
                <label for="state">Estado</label>
                <select
                    class="form-control {{ $errors->has('state') ? 'is-invalid': '' }}"
                    name="state"
                    id="state"
                    required
                    {{ (int) $user->state === \App\User::FOR_ACTIVATION ? 'disabled' : '' }}
                >
                    <option
                        {{
                            (int) old('state') === \App\User::ACTIVE
                            ||
                            (int) $user->state === \App\User::ACTIVE
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\User::ACTIVE }}"
                    >Activo</option>
                    <option
                        {{
                            (int) old('state') === \App\User::INACTIVE
                            ||
                            (int) $user->state === \App\User::INACTIVE
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\User::INACTIVE }}"
                    >Inactivo</option>
                    <option disabled
                        {{
                            (int) old('state') === \App\User::FOR_ACTIVATION
                            ||
                            (int) $user->state === \App\User::FOR_ACTIVATION
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\User::FOR_ACTIVATION }}"
                    >Por activar</option>
                </select>
            </div>
            @endif


            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>

@endsection
