@extends('layouts.app', ['page' => 'users'])

@section('content')
    <div class="container container-form">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Información de usuario"),
                    'icon' => "user"
                ])
            </div>
        </div>
        <div class="row mt-3 mb-3">
            <table class="table table-hover table-light">
                <tbody>
                <tr>
                    <th>Nombre</th>
                    <td>{{ $user->name." ".$user->fathers_last_name." ".$user->mothers_last_name }}</td>
                </tr>
                <tr>
                    <th>Rol</th>
                    <td>{{ $user->role->name }}</td>
                </tr>
                <tr>
                    <th>Documento</th>
                    <td>{{ $user->document_number }}</td>
                </tr>
                <tr>
                    <th>Celular</th>
                    <td>{{ $user->cellphone }}</td>
                </tr>
                <tr>
                    <th>Correo</th>
                    <td>{{ $user->email }}</td>
                </tr>
                <tr>
                    <th>Estado</th>
                    <td>

                        @if($user->state == \App\User::ACTIVE)
                            Activo
                        @elseif($user->state == \App\User::INACTIVE)
                            Inactivo
                        @elseif($user->state == \App\User::FOR_ACTIVATION)
                            Por activar
                        @endif

                    </td>
                </tr>
                <tr>
                    <th>Fecha de creación</th>
                    <td>{{ \Carbon\Carbon::parse($user->created_at )->format('d/m/Y H:i')}}</td>
                </tr>
                <tr>
                    <th>Fecha de activación</th>
                    <td>
                        @if($user->email_verified_at)
                        {{ \Carbon\Carbon::parse($user->email_verified_at )->format('d/m/Y H:i')}}
                        @else
                        Aún no ha confirmado su correo
                        @endif
                    </td>
                </tr>
                @can('audit', [\App\User::class])
                <tr>
                    <th>Registro de actividades</th>
                    <td>
                        <a
                            class="btn btn-outline-info"
                            href="{{ route('userActivityLogs.list', $user->id) }}"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="Ver registro de actividades realizadas por el usuario"
                        >
                            Ver
                        </a>
                    </td>
                </tr>
                @endcan
                <tr>
                    <th>Registro de modificaciones a usuario</th>
                    <td>
                        <a
                            class="btn btn-outline-info"
                            href="{{ route('userActivityLogs.listUpdatesToUser', $user->id) }}"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="Ver registro de modificaciones realizadas al usuario"
                        >
                            Ver
                        </a>
                    </td>
                </tr>


                </tbody>
            </table>
        </div>
    </div>
@endsection
