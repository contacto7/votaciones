@extends('layouts.app', ['page' => 'info'])

@section('content')
    <div class="container container-form">
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Establecer contraseña inicial"),
                    'icon' => "user"
                ])
            </div>
        </div>
        <form
            method="POST"
            action="{{ route('users.storeInitialPassword', ['user_id'=>$user_id,'token_mail'=>$token_mail]) }}"
            novalidate
            enctype="multipart/form-data"
        >
            @method('PUT')

            @csrf
            <div class="form-group">
                <label for="name">Contraseña</label>
                <input
                    type="password"
                    class="form-control {{ $errors->has('password') ? 'is-invalid': '' }}"
                    name="password"
                    id="password"
                    placeholder=""
                    value="{{ old('password') ?: ""}}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="name">Confirme contraseña</label>
                <input
                    type="password"
                    class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid': '' }}"
                    name="password_confirmation"
                    id="password_confirmation"
                    placeholder=""
                    value="{{ old('password_confirmation') ?: ""}}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('password_confirmation'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    Guardar
                </button>
            </div>

        </form>

    </div>

@endsection
