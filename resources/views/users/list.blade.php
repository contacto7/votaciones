@extends('layouts.app', ['page' => 'users'])

@section('content')
    <div class="container">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row d-none">
            @include('partials.users.search')
        </div>
        <div class="row justify-content-center">
            <div class="w-100 text-center">
                <h5>
                    @can('create', [\App\User::class])
                        <a
                            class="form-control btn btn-agregar-sistema"
                            href="{{ route('users.create') }}"
                        >Añadir</a>
                    @endcan
                    LISTA DE USUARIOS
                </h5>
                <form action="{{ route('users.search') }}" method="get" class="form-inline form-search">
                    <div class="input-group input-group-search">
                        <input
                            class="form-control"
                            name="name_search"
                            id="name_search"
                            type="text"
                            placeholder="{{ __("Buscar usuarios") }}"
                        >
                        <div class="input-group-append">
                            <input
                                class="btn btn-outline-secondary"
                                name="filter"
                                type="submit"
                                value="Buscar"
                            >
                        </div>
                    </div>
                </form>
            </div>
            </div>
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col" class="d-none">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Celular</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Estado</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @forelse($users as $user)
                    <tr>
                        <td class="d-none">{{ $user->id }}</td>
                        <td>{{ $user->name." ".$user->fathers_last_name." ".$user->mothers_last_name }}</td>
                        <td>{{ $user->cellphone }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            @if($user->state == \App\User::ACTIVE)
                                Activo
                            @elseif($user->state == \App\User::INACTIVE)
                                Inactivo
                            @elseif($user->state == \App\User::FOR_ACTIVATION)
                                Por activar
                            @endif

                        </td>
                        <td>
                            <a
                                class="btn btn-yellow"
                                href="{{ route('users.info', $user->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Ver usuario"
                            >
                                <i class="fa fa-info"></i>
                            </a>
                            @can('update', [\App\User::class, $user])
                            <a
                                class="btn btn-limegreen"
                                href="{{ route('users.edit', $user->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Editar usuario"
                            >
                                <i class="fa fa-pencil"></i>
                            </a>
                            @endcan
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay usuarios disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $users->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('scripts')
@endpush
