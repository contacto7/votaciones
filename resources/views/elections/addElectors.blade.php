@extends('layouts.app')

@section('content')
    <div class="container container-form">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Votaciones"),
                    'icon' => "user"
                ])
            </div>
        </div>
        <form
            method="POST"
            action="{{ route('electors.store',['id'=>$election->id]) }}"
            novalidate
            enctype="multipart/form-data"
        >

            @csrf

            <div class="form-group">
                <label for="">Lista de electores</label>
                <div class="col-md-12">
                    <input
                        type="file"
                        class="custom-file-input form-control {{ $errors->has('electors_list') ? 'is-invalid': '' }}"
                        name="electors_list"
                        id="electors_list"
                        placeholder=""
                        value=""
                    >
                    <label  class="custom-file-label" for="electors_list">
                        Buscar archivo
                    </label>
                    @if($errors->has('electors_list'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('electors_list') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __("Añadir Electores") }}
                </button>
            </div>

        </form>

    </div>

@endsection
@push('scripts')
    <script>

        /*
        * Función para agregar el nombre del archivo
        * al input file
        */
        $(document).on('change', '.custom-file-input', function(){
            console.log("Cambió");
            var filename = $(this).val().split('\\').pop();
            console.log(filename);
            $(this).siblings('.custom-file-label').html("<i>"+filename+"</i>");
        });


    </script>
@endpush
