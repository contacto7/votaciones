@extends('layouts.app', ['page' => 'polls'])

@section('content')
    <div class="container container-form">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => "Votación: ".$election->name,
                    'icon' => "file-text-o"
                ])
            </div>
        </div>
        <div class="row mt-3 mb-3">
            <table class="table table-hover table-light">
                <tbody>
                <tr>
                    <th>Título</th>
                    <td>{{ $election->name }}</td>
                </tr>
                <tr>
                    <th>Grupo de electores</th>
                    <td>{{ $election->group->name }}</td>
                </tr>
                <tr>
                    <th>Comienza</th>
                    <td>A las {{ \Carbon\Carbon::parse($election->poll->start_date )->format('H:i')}} horas del
                        {{ \Carbon\Carbon::parse($election->poll->start_date )->format('d/ m/ Y')}}</td>
                </tr>
                <tr>
                    <th>Finaliza:</th>
                    <td>A las {{ \Carbon\Carbon::parse($election->poll->end_date )->format('H:i')}} horas del
                        {{ \Carbon\Carbon::parse($election->poll->end_date )->format('d/ m/ Y')}}</td>
                </tr>
                <tr>
                    <th>Creó</th>
                    <td>{{ $election->user->name." ".$election->user->last_name }}</td>
                </tr>
                <tr>
                    <th>Fecha de creación</th>
                    <td>A las {{ \Carbon\Carbon::parse($election->created_at )->format('H:i')}} horas del
                        {{ \Carbon\Carbon::parse($election->created_at )->format('d/ m/ Y')}}</td>
                </tr>
                <tr>
                    <th>Electores totales</th>
                    <td>{{ $electorsCount }}</td>
                </tr>
                <tr>
                    <th>Electores que votaron</th>
                    <td>{{ $votesCount }}</td>
                </tr>
                <tr>
                    <th>Electores que no votaron</th>
                    <td>{{ $electorsCount - (int)$votesCount }}</td>
                </tr>
                @can('viewResults', [\App\Poll::class])
                <tr>
                    <th>Resultados de votación</th>
                    <td>
                        <a
                            class="btn btn-outline-info"
                            href="{{ route('elections.results', $election->id) }}"
                        >
                            Ver resultados
                        </a>
                        <a
                            class="btn btn-outline-info"
                            href="{{ action('ElectionController@resultsPDF', $election->id)}}"
                        >
                            Descargar resultados
                        </a>
                    </td>
                </tr>
                @endcan
                @can('viewElectors', [\App\Poll::class])
                <tr>
                    <th>Detalle de electores</th>
                    <td>
                        <a
                            class="btn btn-outline-info"
                            href="{{ route('elections.electionElectors', $election->id) }}"
                        >
                            Ver electores
                        </a>
                    </td>
                </tr>
                @endcan
                @can('viewVotes', [\App\Poll::class])
                <tr>
                    <th>Detalle de votación</th>
                    <td>
                        <a
                            class="btn btn-outline-info"
                            href="{{ route('elections.electionVotes', $election->id) }}"
                        >
                            Auditar
                        </a>
                    </td>
                </tr>
                @endcan
                <tr>
                    <th>Grupo de votaciones</th>
                    <td>
                        <a
                            class="btn btn-outline-info"
                            href="{{ route('polls.info', $election->poll->id) }}"
                        >
                            Ver
                        </a>
                    </td>
                </tr>
                @can('sendEmails', [\App\Poll::class])
                <tr class="d-none">
                    <th>Enviar correo a electores</th>
                    <td>
                        <a
                            class="btn btn-outline-info @if($election->poll->state == \App\Poll::MAILS_SENDED || $election->poll->state == \App\Poll::MAILS_RE_SENDED) disabled @endif"
                            href="{{ route('elections.sendNotificationMails', $election->id) }}"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="Ver detalle de votos"
                            disabled

                        >
                            Enviar
                        </a>
                        @if($election->poll->state == \App\Poll::MAILS_SENDED || $election->poll->state == \App\Poll::MAILS_RE_SENDED) <i> (Los correos ya han sido enviados)</i> @endif
                    </td>
                </tr>
                <tr>
                    <th>Enviar correo recordatorio</th>
                    <td>
                        <a
                            class="btn btn-outline-info"
                            href="{{ route('elections.sendRecordatoryMails', $election->id) }}"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="Ver detalle de votos"
                        >
                            Enviar
                        </a>
                        @if($election->poll->state == \App\Poll::MAILS_RE_SENDED) <i> (Los recordatorios ya han sido enviados)</i> @endif
                    </td>
                </tr>
                @endcan

                </tbody>
            </table>
        </div>
    </div>
@endsection
