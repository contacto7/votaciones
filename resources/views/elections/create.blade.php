@extends('layouts.app', ['page' => 'polls'])

@section('content')
    <div class="container container-form">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => "Crear eleccion en ".$poll->name,
                    'icon' => "user"
                ])
            </div>
        </div>
        <form
            method="POST"
            action="{{ ! $election->id ? route('elections.store'): route('elections.update', ['id'=>$election->id]) }}"
            novalidate
            enctype="multipart/form-data"
        >
            @if($election->id)
                @method('PUT')
            @endif

            @csrf

            <div class="form-group">
                <label for="name">Nombre de elección</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('name') ? 'is-invalid': '' }}"
                    name="name"
                    id="name"
                    placeholder=""
                    value="{{ old('name') ?: $election->name }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="group_name">Grupo de electores (Ingrese el nombre y selecciónelo de la lista)</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('group_name') ? 'is-invalid': '' }}"
                    name="group_name"
                    id="group_name"
                    placeholder=""
                    value="{{ old('group_name') ?: $election->group_name }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('group_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('group_name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group d-none">
                <label for="group_id">Id de Grupo</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('group_id') ? 'is-invalid': '' }}"
                    name="group_id"
                    id="group_id"
                    placeholder=""
                    value="{{ old('group_id') ?: $election->group_id }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('group_id'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('group_id') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group d-none">
                <label for="poll_id">Id de grupo de votaciones</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('poll_id') ? 'is-invalid': '' }}"
                    name="poll_id"
                    id="poll_id"
                    placeholder=""
                    value="{{ old('poll_id') ?: $poll->id }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('poll_id'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('poll_id') }}</strong>
                </span>
                @endif
            </div>


            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>

@endsection

@push('styles')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <link rel="stylesheet"  href="{{ asset('css/easy-autocomplete.min.css') }}">
    <link rel="stylesheet"  href="{{ asset('css/easy-autocomplete.themes.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/jquery.easy-autocomplete.min.js') }}"></script>

    <script>
        $(function () {

            //DEFINIMOS LOS ARREGLOS DISTRITOS Y TIPOS DE INMUEBLES COMO JSON
            var groups = @json($groups);


            let group_options = [];
            $.each(groups, function (key, element) {
                let date_db = element.created_at;
                date_db = moment(date_db);
                let date_formated = moment(date_db).format("DD/MM/YYYY");

                let group_obj = {id:element.id, name: element.name+" - "+date_formated};
                group_options.push(group_obj)
                //console.log(element.id);
            });

            console.log(group_options);

            var options = {
                data: group_options,

                getValue: "name",

                list: {

                    onSelectItemEvent: function() {
                        var value = $("#group_name").getSelectedItemData().id;

                        $("#group_id").val(value).trigger("change");
                    },
                    match: {
                        enabled: true
                    },
                    maxNumberOfElements: 5,
                }
            };


            $("#group_name").easyAutocomplete(options);
        });


    </script>
@endpush
