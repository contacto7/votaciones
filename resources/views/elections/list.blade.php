@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            @include('partials.elections.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Grupo de votación</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @forelse($elections as $election)
                    <tr>
                        <td>{{ $election->id }}</td>
                        <td>{{ $election->name }}</td>
                        <td>
                            {{ $election->group->name }}
                        </td>
                        <td>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('elections.info', $election->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Ver votación"
                            >
                                <i class="fa fa-info"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay votaciones disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $elections->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('scripts')
@endpush
