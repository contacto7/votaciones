@extends('layouts.app', ['page' => 'polls'])

@section('content')
    <div class="container container-form">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => "Añadir preguntas en ". $election->name,
                    'icon' => "file-text-o"
                ])
            </div>
        </div>
        <div class="questions-wrap">
            <div class="questions-inn">



                @forelse($election->questions as $question)
                <div class="question question-exists question-{{ $question->id }}" data-id="{{ $question->id }}">
                    <div class="question-name-wrap">
                        <div class="question-number d-inline">{{ $question->order }}.</div>
                        <div class="question-name d-inline"> {{ $question->name }}</div>
                    </div>
                    <div class="candidates-wrap">
                        <div class="candidates-inn">
                            @php $temp_candidate = 0; @endphp
                            @forelse($question->candidates->where('list_number','<',250) as $candidate)
                                <div class="candidate candidate-exists candidate-{{ $candidate->id }}" data-id="{{ $candidate->id }}">
                                    <div class="candidate-number d-inline">{{ $temp_candidate =$candidate->order }}.</div>
                                    <div class="candidate-number d-inline">Lista {{ $candidate->list_number }} - {{ $candidate->list_name }} - </div>
                                    <div class="candidate-name d-inline"> {{ $candidate->name }}</div>
                                </div>

                            @empty

                            @endforelse

                            <div class="candidate candidate-add">
                                <div class="candidate-number d-inline">{{ ++$temp_candidate }}.</div>
                                <div class="candidate-name">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <input
                                                type="number"
                                                class="form-control list-number-to-add list-number-input"
                                                name="candidate_list_number"
                                                placeholder="# Lista"
                                                required
                                                autocomplete="off"
                                                min="1"
                                            >
                                        </div>
                                        <input
                                            type="text"
                                            class="form-control list-name-to-add"
                                            name="candidate_list_name"
                                            placeholder="Partido"
                                            required
                                            autocomplete="off"
                                        >
                                        <input
                                            type="text"
                                            class="form-control candidate-to-add"
                                            name="candidate_name"
                                            placeholder="Nombre del candidato"
                                            required
                                            autocomplete="off"
                                        >
                                        <div class="input-group-prepend">
                                            <a class="btn btn-success add-candidate-btn">Agregar</a>
                                        </div>
                                        <span class="invalid-feedback">
                                            <strong>El campo es obligatorio</strong>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="candidate candidate-white-nulled">
                                <div class="candidate-number-white-nulled d-inline">{{ ++$temp_candidate }}.</div>
                                <div class="candidate-number d-inline"> </div>
                                <div class="candidate-name d-inline"> Voto en blanco</div>
                            </div>
                            <div class="candidate candidate-white-nulled">
                                <div class="candidate-number-white-nulled d-inline">{{ ++$temp_candidate }}.</div>
                                <div class="candidate-number d-inline"> </div>
                                <div class="candidate-name d-inline"> Voto viciado</div>
                            </div>

                        </div>
                    </div>
                </div>
                @empty

                @endforelse


                    <div class="question question-add mt-3">
                        <div class="question-name-wrap">
                            <div class="question-number d-inline">1.</div>
                            <div class="question-name">
                                <div class="input-group">
                                    <input
                                        type="text"
                                        class="form-control question-to-add"
                                        name="question_name"
                                        placeholder="Agregue una pregunta"
                                        required
                                        autocomplete="off"
                                    >
                                    <div class="input-group-prepend">
                                        <a class="btn btn-success add-question-btn">Agregar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="candidates-wrap">
                            <div class="candidates-inn">

                                <div class="candidate candidate-add">
                                    <div class="candidate-number d-inline">1.</div>
                                    <div class="candidate-name">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <input
                                                    type="text"
                                                    class="form-control list-number-input"
                                                    name="candidate_list_number"
                                                    placeholder="# Lista"
                                                    required
                                                    autocomplete="off"
                                                    readonly
                                                >
                                            </div>
                                            <input
                                                type="text"
                                                class="form-control"
                                                name="candidate_list_name"
                                                placeholder="Partido"
                                                required
                                                autocomplete="off"
                                                readonly
                                            >
                                            <input
                                                type="text"
                                                class="form-control"
                                                name="candidate_name"
                                                placeholder="Agregue un candidato"
                                                required
                                                autocomplete="off"
                                                readonly
                                            >
                                            <div class="input-group-prepend">
                                                <a class="btn btn-success">Agregar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>





            </div>

        </div>

        <div class="row text-center d-block mt-5 mb-4">
            <a
                class="btn btn-outline-info"
                href="{{ route('polls.info', $election->poll->id) }}"
                data-toggle="tooltip"
                data-placement="top"
                title="Siguiente"
            >
                Siguiente
            </a>
        </div>
    </div>


    @if(session('groupMessage'))
        <div class="modal" id="groupMessage">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Importante</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content">

                        <div class="modal-body-msg-wrapp modal-group-msg-wrp">
                            Los electores se están agtegando, por favor no cierre esta ventana o la página hasta que terminen de cargar al sistema.
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(window).on('load',function(){
                $('#groupMessage').modal('show');
            });
        </script>
    @endif
@endsection

@push('scripts')
    <script>
        let election = 0;
        let user_activity_log = 0;

        var next_question_order = 1;

        $(function() {
            let number_of_questions = $('.question-exists').length;
            next_question_order += number_of_questions;

            $(".question-add .question-number").html(next_question_order+".");

            console.log("Next question order: "+next_question_order);


            @if(session('groupMessage'))
                election = {!! session('groupMessage')[0] !!};
                user_activity_log = {!! session('groupMessage')[1] !!};
                getUploadStatus();

            @endif


            function getUploadStatus(){
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: "{{ route('elections.electorsSyncStatusAjax') }}",
                    data:{
                        user_activity_log: user_activity_log,
                    },
                    dataType: "json",
                    success: function (data) {

                        console.log(data);
                        $(".modal-group-msg-wrp").html(data.message);

                        if(data.action == "continue"){
                            setTimeout(getUploadStatus, 500);
                        }else{
                            setTimeout(function(){
                                window.location.reload(1);
                            }, 5000);
                        }

                    },error:function(data){
                        console.log("-----------------ERROR----------------");
                        console.log(data);

                        setTimeout(getUploadStatus, 500);
                        console.log("-----------------ERROR----------------");
                    }
                });
            }
        });

        @php
        //token_e is the id of the election, encripted
        @endphp
        //let tocken_e = {{ \Crypt::encrypt($election->id) }};
        let election_id = {{ $election->id }};

        /*
        * Función para cambiar el estado de un documento
         * cuando se cambio el selector del documento
        */
        $(document).on('click', '.add-question-btn', function(){
            let question_name = $(this).closest('.question-name').find(".question-to-add").val();

            console.log(question_name);
            console.log(election_id);

            addQuestion(question_name);
        });
        $(document).on('click', '.add-candidate-btn', function(){

            $(".question input").removeClass("is-invalid");

            let candidate_name = $(this).closest('.candidate-name').find(".candidate-to-add").val();
            let list_number = $(this).closest('.candidate-name').find(".list-number-to-add").val();
            let list_name = $(this).closest('.candidate-name').find(".list-name-to-add").val();
            let question = $(this).closest('.question').attr("data-id");
            let candidates_on_question = $(this).closest('.question').find(".candidate-exists").length;
            let next_candidate_order = candidates_on_question + 1;

            console.log("Nombre de candidato: "+candidate_name);
            console.log("Pregunta: "+question);
            console.log("Orden: "+next_candidate_order);
            console.log("Lista: "+list_number);
            console.log("Partido: "+list_name);

            addCandidate(candidate_name, question, next_candidate_order, list_number, list_name);
        });

        function addQuestion(question_name) {
            //Enviamos una solicitud con el id del documento
            $.ajax({
                type: 'POST', //THIS NEEDS TO BE GET
                url: '{{ route('questions.store') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "name": question_name,
                    "order": next_question_order,
                    "election_id": election_id
                },
                success: function (data) {
                    console.log(data);
                    console.log("Code: "+ data.code);
                    console.log("Message: "+ data.message);

                    if (data.code == 200){

                        console.log("response ok:"+data.action);
                        location.reload();
                    }else{
                        let message = "Ocurrió algún problema. Por favor, inténtelo de nuevo. ";
                        message += "Code: "+data.code;
                        message += ". Message: "+ data.message;
                        alert( message );

                    }
                },error:function(data){
                    console.log("--- ERROR ---");
                    let message = "Se tienen los siguientes errores en su solicitud: ";
                    let errors = data.responseJSON;
                    $.each( errors.errors, function( key, value ) {
                        message += value+" "; //showing only the first error.
                    });
                    alert(message);
                    console.log(errors);
                    //$(".question-"+question+" input").addClass("is-invalid");
                }
            });
        }

        function addCandidate(candidate_name, question, order, list_number, list_name) {
            //Enviamos una solicitud con el id del documento
            $.ajax({
                type: 'POST', //THIS NEEDS TO BE GET
                url: '{{ route('candidates.store') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "name": candidate_name,
                    "order": order,
                    "list_number": list_number,
                    "list_name": list_name,
                    "question_id": question
                },
                success: function (data) {
                    console.log(data);
                    console.log("Code: "+ data.code);
                    console.log("Message: "+ data.message);

                    if (data.code == 200){

                        console.log("response ok:"+data.action);
                        location.reload();
                    }else{
                        let message = "";
                        console.log("Code: "+data.code);
                        message += data.message;
                        alert( message );

                    }
                },error:function(data){
                    console.log("--- ERROR ---");
                    let message = "Se tienen los siguientes errores en su solicitud: ";
                    let errors = data.responseJSON;
                    $.each( errors.errors, function( key, value ) {
                        message += value+" "; //showing only the first error.
                        $(".question-"+question+" input[name='candidate_"+key+"']").addClass("is-invalid");
                    });
                    //alert(message);
                    console.log(errors);
                    $(".question-"+question+" .invalid-feedback strong").html(message);
                    //$(".question-"+question+" input").addClass("is-invalid");
                }
            });
        }


    </script>
@endpush
