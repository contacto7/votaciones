@extends('layouts.app', ['page' => 'polls'])

@section('content')
    <div class="container container-form">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Electores de elección: $election->name"),
                    'icon' => "file-text-o"
                ])
            </div>
        </div>
        <div class="row mt-3 mb-3">
            <div class="col-12 flex-column align-items-center d-flex mb-4">
                <table class="table table-hover table-light">
                    <thead>
                    <tr>
                        <th class="col-candidate">Elector</th>
                        <th class="col-candidate">DNI</th>
                        <th class="col-candidate">Correo</th>
                        <th class="col-candidate">Votó</th>
                        <th class="col-candidate">Correo</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($electors as $elector)
                        <tr>
                            <td class="col-candidate">{{ $elector->name." ".$elector->fathers_last_name." ".$elector->mothers_last_name }}</td>
                            <td class="col-candidate">{{ $elector->document_number }}</td>
                            <td class="col-candidate">{{ $elector->email }}</td>
                            <td class="col-candidate">
                                @if($elector->pivot->state == \App\Poll::VOTED)
                                    Si, a las {{ \Carbon\Carbon::parse($elector->pivot->voted_at )->format('H')}} del
                                    {{ \Carbon\Carbon::parse($elector->pivot->voted_at )->format('d/m/Y')}}
                                @else()
                                    No
                                @endif

                            </td>
                            <td class="col-candidate">
                                @if($elector->pivot->state == \App\Poll::VOTED)
                                @else()
                                    <a
                                        class="btn btn-outline-info"
                                        href="{{ route('polls.sendRecordatoryMail',['poll_id'=>$poll->id,'elector_id'=>$elector->id ] ) }}"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="Ver detalle de votos"
                                    >
                                        Enviar
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>{{ __("Aún no hay electores seleccionados")}}</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div>
        <div class="row justify-content-center mb-4">
            {{ $electors->appends(request()->except('page'))->links() }}
        </div>
        <div class="row text-center d-block mb-4">
            <a
                class="btn btn-outline-info"
                href="{{ route('elections.info', $election->id) }}"
                data-toggle="tooltip"
                data-placement="top"
                title="Ver votación"
            >
                Regresar a la votación
            </a>
        </div>
    </div>
@endsection
