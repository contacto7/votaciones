@extends('layouts.app', ['page' => 'electors'])

@section('content')
    <div class="container">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row d-none">
            @include('partials.electors.search')
        </div>
        <div class="row justify-content-center">
            <div class="w-100 text-center">
                <h5>ELECTORES - TOTAL {{ $electors_total_count }}</h5>
                <form action="{{ route('electors.search') }}" method="get" class="form-inline form-search">
                    <div class="input-group input-group-search">
                        <input
                            class="form-control"
                            name="name_search"
                            id="name_search"
                            type="text"
                            placeholder="{{ __("Buscar electores") }}"
                        >
                        <div class="input-group-append">
                            <input
                                class="btn btn-outline-secondary"
                                name="filter"
                                type="submit"
                                value="Buscar"
                            >
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12 flex-column align-items-center d-flex mb-4">
                <table class="table table-hover table-light">
                    <thead>
                    <tr>
                        <th class="col-candidate">Elector</th>
                        <th class="col-candidate">DNI</th>
                        <th class="col-candidate">Correo</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($electors as $elector)
                        <tr>
                            <td class="col-candidate">
                                <a href="{{ route('electors.info', $elector->id) }}">{{ $elector->name." ".$elector->fathers_last_name." ".$elector->mothers_last_name }}</a>
                            </td>
                            <td class="col-candidate">{{ $elector->document_number }}</td>
                            <td class="col-candidate">{{ $elector->email }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td>{{ __("Aún no hay electores seleccionados")}}</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div>
        <div class="row justify-content-center mb-4">
            {{ $electors->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
