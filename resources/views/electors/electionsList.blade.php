@extends('layouts.app', ['page' => 'electors'])

@section('content')
    <div class="container container-form">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => "Elector: ".$elector->name." ".$elector->fathers_last_name." ".$elector->mothers_last_name,
                    'icon' => "file-text-o"
                ])
            </div>
        </div>
        <div class="row mt-3 mb-3">
            <div class="col-12 flex-column align-items-center d-flex mb-4">
                <table class="table table-hover table-light">
                    <thead>
                    <tr>
                        <th class="col-candidate d-none">Id</th>
                        <th class="col-candidate">Votación</th>
                        <th class="col-candidate">Grupo de votaciones</th>
                        <th class="col-candidate">Grupo de electores</th>
                        <th class="col-candidate">Fecha de elección</th>
                        <th class="col-candidate">Votó</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($elections as $election)

                        @php $pivot = $election->poll->electors[0]->pivot @endphp
                        <tr>
                            <td class="col-candidate d-none">{{ $election->id }}</td>
                            <td class="col-candidate"><a href="{{ route('elections.info',$election->id ) }}">{{ $election->name }}</a></td>
                            <td class="col-candidate"><a href="{{ route('polls.info',$election->poll->id ) }}">{{ $election->poll->name }}</a></td>
                            <td class="col-candidate"><a href="{{ route('groups.info',$election->group->id ) }}">{{ $election->group->name }}</a></td>
                            <td class="col-candidate">
                                {{ \Carbon\Carbon::parse($election->poll->start_date )->format('d/m/Y')}}<br>
                                {{ \Carbon\Carbon::parse($election->poll->start_date )->format('H:i')}}</td>
                            <td class="col-$election">
                                @if($pivot->state == \App\Poll::VOTED)
                                    Si, a las {{ \Carbon\Carbon::parse($pivot->voted_at )->format('H')}} horas del
                                    {{ \Carbon\Carbon::parse($pivot->voted_at )->format('d/m/Y')}}
                                @else()
                                    No
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>{{ __("Aún no hay elecciones para este elector")}}</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div>
        <div class="row justify-content-center mb-4">
            {{ $elections->appends(request()->except('page'))->links() }}
        </div>
        <div class="row text-center d-block mb-4">
            <a
                class="btn btn-outline-info"
                href="{{ route('electors.info', $elector->id) }}"
            >
                Regresar al elector
            </a>
        </div>
    </div>
@endsection
