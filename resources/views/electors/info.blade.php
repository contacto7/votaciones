@extends('layouts.app', ['page' => 'electors'])

@section('content')
    <div class="container container-form">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Información de elector"),
                    'icon' => "user"
                ])
            </div>
        </div>
        <div class="row mt-3 mb-3">
            <table class="table table-hover table-light">
                <tbody>
                <tr>
                    <th>Nombre</th>
                    <td>{{ $elector->name." ".$elector->fathers_last_name." ".$elector->mothers_last_name }}</td>
                </tr>
                <tr>
                    <th>Documento</th>
                    <td>{{ $elector->document_number }}</td>
                </tr>
                <tr>
                    <th>Correo</th>
                    <td>{{ $elector->email }}</td>
                </tr>
                <tr>
                    <th>Celular</th>
                    <td>{{ "+".$elector->country_code." ".$elector->cellphone }}</td>
                </tr>
                <tr>
                    <th>Fecha de creación</th>
                    <td>{{ \Carbon\Carbon::parse($elector->created_at )->format('d/m/Y H:i')}}</td>
                </tr>

                <tr>
                    <th>Registro de votaciones</th>
                    <td>
                        <a
                            class="btn btn-outline-info"
                            href="{{ route('electors.electionList', $elector->id) }}"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="Ver registro de votaciones realizadas por el usuario"
                        >
                            Ver
                        </a>
                    </td>
                </tr>
                <tr class="d-none">
                    <th>Registro de modificaciones a elector</th>
                    <td>
                        <a
                            class="btn btn-outline-info"
                            href="{{ route('userActivityLogs.listUpdatesToElector', $elector->id) }}"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="Ver registro de modificaciones realizadas al usuario"
                        >
                            Ver
                        </a>
                    </td>
                </tr>


                </tbody>
            </table>
        </div>
    </div>
@endsection
