@extends('layouts.app')

@section('content')
    <div class="container container-form">

        <div class="row">
            @include('partials.nreaddcrumb', ['bread_arr'=>$bread_arr])
        </div>
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Electores"),
                    'icon' => "user"
                ])
            </div>
        </div>
        <form
            method="POST"
            action="{{ ! $elector->id ? route('electors.store'): route('electors.update', ['id'=>$elector->id]) }}"
            novalidate
            enctype="multipart/form-data"
        >
            @if($elector->id)
                @method('PUT')
            @endif

            @csrf

            <div class="form-group">
                <label for="name">Nombres</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('name') ? 'is-invalid': '' }}"
                    name="name"
                    id="name"
                    placeholder=""
                    value="{{ old('name') ?: $elector->name }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="fathers_last_name">Apellido paterno</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('fathers_last_name') ? 'is-invalid': '' }}"
                    name="fathers_last_name"
                    id="name"
                    placeholder=""
                    value="{{ old('fathers_last_name') ?: $elector->fathers_last_name }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('fathers_last_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('fathers_last_name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="mothers_last_name">Apellido materno</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('mothers_last_name') ? 'is-invalid': '' }}"
                    name="mothers_last_name"
                    id="mothers_last_name"
                    placeholder=""
                    value="{{ old('mothers_last_name') ?: $elector->mothers_last_name }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('mothers_last_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('mothers_last_name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="document_number">Número de documento de identidad</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('document_number') ? 'is-invalid': '' }}"
                    name="document_number"
                    id="document_number"
                    placeholder=""
                    value="{{ old('document_number') ?: $elector->document_number }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('document_number'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('document_number') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="email">Correo</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('email') ? 'is-invalid': '' }}"
                    name="email"
                    id="email"
                    placeholder=""
                    value="{{ old('email') ?: $elector->email }}"
                    required
                    autocomplete="off"
                >
                @if($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>

@endsection
