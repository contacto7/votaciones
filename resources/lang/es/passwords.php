<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben coincidir y contener al menos 6 caracteres',
    'reset'    => '¡Tu contraseña ha sido restablecida!',
    'sent'     => '¡Te hemos enviado por correo el enlace para restablecer tu contraseña!',
    'token'    => 'El token de recuperación de contraseña es inválido.',
    'user'     => 'No podemos encontrar ningún usuario con ese correo electrónico.',
    "Hello!"   => 'Hola!',
    "You are receiving this email because we received a password reset request for your account."   => 'Estás recibiendo este correo porque recibimos una solicitud de restablecer contraseña.',
    "This password reset link will expire in :count minutes."   => 'Este link expirará en :count minutos.',
    "If you did not request a password reset, no further action is required."   => 'Si no solicitaste restablecer tu contraseña, no debes hacer nada más.',
    "Regards"   => 'Saludos',
    "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\ninto your web browser: [:actionURL](:actionURL)"   => 'Si estás teniendo problemas haciendo click al botón ":actionText", copia y pega la URL debajo\nen tu navegador: [:actionURL](:actionURL)',
];
