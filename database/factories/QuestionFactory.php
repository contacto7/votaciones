<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    $questions_arr = ['Secretario', 'Rector', 'Director', 'Sub-director', 'Sub-secretario', 'Vicerector', 'Tesorero', 'Secretario de planeación', 'Secretario de relaciones públicas', 'Secretario general'];
    $adding_arr = ['principal', 'accesitario', 'suplente', 'adjunto', 'auxiliar', 'provisional', 'temporal'];

    $question_name = $faker->randomElement($questions_arr)." ".$faker->randomElement($adding_arr);

    return [
        'name' => $question_name,
        'order' => $faker->numberBetween(1, 5),
        'election_id'=>\App\Election::all()->random()->id,
    ];
});
