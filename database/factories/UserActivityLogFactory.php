<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UserActivityLog;
use Faker\Generator as Faker;

$factory->define(UserActivityLog::class, function (Faker $faker) {
    return [
        'user_id'=>\App\User::all()->random()->id,
        'action_id'=>\App\Action::all()->random()->id,
        'description'=> $faker->sentence,
    ];
});
