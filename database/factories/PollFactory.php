<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Poll;
use Faker\Generator as Faker;

$factory->define(Poll::class, function (Faker $faker) {
    $start_date= \Carbon\Carbon::createFromTimestamp($faker->dateTimeBetween($startDate = '+1 days', $endDate = '+2 days')->getTimeStamp());
    $end_date= \Carbon\Carbon::createFromTimestamp($faker->dateTimeBetween($startDate = '+3 days', $endDate = '+4day')->getTimeStamp());

    $poll_name_arr = ['Elecciones', 'Grupo de votaciones', 'Grupo de elecciones', 'Grupo de sufragios', 'Comicios', 'Plebiscito'];
    $adding_arr = ['generales', 'provisionales', 'académicas', 'institucionales', 'administrativas'];

    $name = $faker->randomElement($poll_name_arr)." ".$faker->randomElement($adding_arr);

    $slug = Str::slug($name." ".Str::random(6));

    return [
        'name' => $name,
        'start_date' => $start_date,
        'end_date' => $end_date,
        'auth_factors' => $faker->numberBetween(\App\Poll::ONE_FACTOR, \App\Poll::TWO_FACTOR),
        'state' => $faker->numberBetween(\App\Poll::FOR_OPEN, \App\Poll::CLOSED),
        'user_id'=>\App\User::where('role_id',\App\Role::ELECTION_CREATOR)->get()->random()->id,
        'slug'=>$slug,
        'visibility'=>random_int(Poll::PUBLIC,Poll::PRIVATE),
    ];
});
