<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Election;
use Faker\Generator as Faker;

$factory->define(Election::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'group_id'=>\App\Group::all()->random()->id,
        'user_id'=>\App\User::where('role_id',\App\Role::ELECTION_CREATOR)->get()->random()->id,
        'poll_id'=>\App\Poll::all()->random()->id,
    ];
});
