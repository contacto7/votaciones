<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'role_id'=>\App\Role::all()->random()->id,
        'name' => $faker->name,
        'fathers_last_name' => $faker->lastName,
        'mothers_last_name' => $faker->lastName,
        'document_number'=> $faker->randomLetter.random_int(1000000,9999999).$faker->randomLetter,
        'cellphone'=> $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'token_mail' => '$2y$12$qm2AurpYwopPOTnJ.KKLvelEHQUxnwVMCEDcAIQ3Zp.Zbb.7NsJqW',//123456
        'state'=> User::ACTIVE,
        'remember_token' => Str::random(10),
    ];
});
