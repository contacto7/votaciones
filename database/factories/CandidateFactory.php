<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Candidate;
use Faker\Generator as Faker;

$factory->define(Candidate::class, function (Faker $faker) {
    $candidate_name = $faker->name." ".$faker->lastName;
    return [
        'name' => $candidate_name,
        'order' => $faker->numberBetween(1, 5),
        'list_number' => $faker->numberBetween(1, 5),
        'list_name' => $faker->company,
        'question_id'=>\App\Question::all()->random()->id,
    ];
});
