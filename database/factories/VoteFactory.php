<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Vote;
use Faker\Generator as Faker;

$factory->define(Vote::class, function (Faker $faker) {
    return [
        'candidate_id'=>\App\Candidate::all()->random()->id,
        'weight'=>rand(100,1000)/1000,
    ];
});
