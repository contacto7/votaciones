<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Elector;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Elector::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'fathers_last_name' => $faker->lastName,
        'mothers_last_name' => $faker->lastName,
        'document_number'=> random_int(10000000,99999999),
        'email' => $faker->unique()->safeEmail,
        'country_code' => '51',//PERU
        'cellphone' => '961518524',//123456
        'token_mail' => '123457',//123456
        'token_cellphone' => '123456',//123456
    ];
});
