<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Group;
use Faker\Generator as Faker;

$factory->define(Group::class, function (Faker $faker) {
    $group_arr = ['Tercio estudiantil', 'Alumnos', 'Profesores', 'Administrativos', 'Consejo directivo', 'Asamblea estudiantil', 'Asociación estudiantil'];
    $added_arr = ['activo', 'del presente año', 'del próximo año', 'del presente semestre', 'del semestre pasado', 'del semestre próximo'];

    $group_name = $faker->randomElement($group_arr)." ".$faker->randomElement($added_arr);

    return [
        'name' => $group_name,
        'user_id'=>\App\User::all()->random()->id,
    ];
});
