<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\EmailActivityLog;
use Faker\Generator as Faker;

$factory->define(EmailActivityLog::class, function (Faker $faker) {
    return [
        'email'=> $faker->email,
        'action_id'=>\App\Action::all()->random()->id,
        'description'=> $faker->sentence,
        'state'=> $faker->sentence,
        'model_received'=> EmailActivityLog::MODEL_USER,
        'model_id'=> $faker->numberBetween(1, 2),
        'sent_at'=> \Carbon\Carbon::createFromTimestamp($faker->dateTimeBetween($startDate = '-20 days', $endDate = '+4day')->getTimeStamp()),
    ];
});
