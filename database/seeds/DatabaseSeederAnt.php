<?php

use Illuminate\Database\Seeder;

class DatabaseSeederAnt extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {

        factory(\App\User::class, 10)->create([
            'state' => \App\User::ACTIVE,
        ]);
        factory(\App\User::class, 2)->create([
            'state' => \App\User::FOR_ACTIVATION,
        ]);
        factory(\App\User::class, 2)->create([
            'state' => \App\User::INACTIVE,
        ]);

        //CREAMOS GRUPOS DE ELECTORES
        factory(\App\Group::class, 4)->create([]);

        //CREAMOS GRUPOS DE VOTACIÓN
        factory(\App\Poll::class, 2)->create([]);

        //ELECTIONES - PREGUNTAS - CANDIDATOS
        $elecciones_arr = ['Elección de consejo directivo', 'Elección de mesa directiva', 'Elección de consejo de facultad', 'Elección de consejo universitario'];
        $grupo_elecciones_arr = [1,1,2,2];
        for ($e = 0; $e<4; $e++){
            factory(\App\Election::class, 1)->create([
                'name' => $elecciones_arr[$e],
                'poll_id' => $grupo_elecciones_arr[$e],
                'group_id' => ($e + 1),
            ])->each(function (\App\Election $el) {
                for ($i = 1; $i<6; $i++){
                    factory(\App\Question::class, 1)->create([
                        'order'=>$i,
                        'election_id'=>$el->id,
                    ])->each(function (\App\Question $qu) {
                        for ($j = 1; $j<6; $j++){
                            factory(\App\Candidate::class, 1)->create([
                                'order'=>$j,
                                'list_number'=>$j,
                                'question_id'=>$qu->id,
                            ]);
                        }
                    });
                }
            });
        }


        //ELECTORES QUE VOTAN
        factory(\App\Elector::class, 1)->create([
            'name' => 'Heser',
            'email' => 'contacto@inoloop.com',
            'document_number' => '123456',
            'token_mail' => '123456',
        ]);
        factory(\App\Elector::class, 1)->create([
            'name' => 'Harold',
            'email' => 'heser.leon@pucp.edu.pe',
            'document_number' => '70681083',
            'token_mail' => '111111',
        ]);
        factory(\App\Elector::class, 1)->create([
            'name' => 'Hael',
            'email' => 'heserlr@hotmail.com',
            'document_number' => '11111111',
            'token_mail' => '222222',
        ]);
        factory(\App\Elector::class, 1)->create([
            'name' => 'Giancarla',
            'email' => 'a20112270@pucp.pe',
            'document_number' => '22222222',
            'token_mail' => '333333',
        ]);

        factory(\App\Elector::class, 1)->create([
            'name' => 'Esthefani',
            'email' => 'inoloopsac@gmail.com',
            'document_number' => '33333333',
            'token_mail' => '444444',
        ]);
        factory(\App\Elector::class, 20)->create([]);


        //////////ASIGNAMOS LOS ELECTORES A LOS 4 GRUPOS
        /// DE VOTACION CREADOS
        $grupos_totales = [1,2,3,4];
        foreach(\App\Elector::get() as $elector){
            $elector->groups()->sync($grupos_totales);
        }

        //////////ASIGNAMOS LOS ELECTORES A LAS VOTACIONES
        ///SON 3 ELECCCIONES SEGÚN LO DEFINIDO EN LAS LÍNEAS
        /// 78-79 DE
        //ELECTIONES - PREGUNTAS - CANDIDATOS
        $polls_totales = [1,2];
        foreach(\App\Elector::get() as $elector){
            $elector->polls()->sync($polls_totales);
        }

        //ELECTORES QUE NO VOTAN
        factory(\App\Elector::class, 15)->create([]);


        //VOTOS
        factory(\App\Vote::class, 500)->create([]);


    }



}
