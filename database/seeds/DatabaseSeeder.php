<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        if (!File::exists('group_docs')) {
            Storage::makeDirectory('group_docs');
        }
        $this->call(CountriesTableSeeder::class);


        $faker = \Faker\Factory::create();

        //////////ROLES
        factory(\App\Role::class, 1)->create([
            'name' => 'Desarrollador',
            'slug' => 'desarrollador',
            'description' => 'Encargado de desarrollar la aplicación.',
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'Administrador',
            'slug' => 'administrador',
            'description' => 'Encargado de administrar el sistema de votaciones.',
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'Creador de elecciones',
            'slug' => 'creador-de-elecciones',
            'description' => 'Encargado de crear votaciones.',
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'Auditor',
            'slug' => 'auditor',
            'description' => 'Encargado de auditar las votaciones.',
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'Staff',
            'slug' => 'staff',
            'description' => 'Personero.',
        ]);

        /////////ACTIONS
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::EMAIL_NOTIFICATION_BULK,
            'name' => 'envío de notificación masivo',
            'slug' => 'envio-notificacion-masivo',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::EMAIL_RECORDATORY_BULK,
            'name' => 'envío de recordatorio masivo',
            'slug' => 'envio-recordatorio-masivo',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::EMAIL_NOTIFICATION,
            'name' => 'envío de notificación',
            'slug' => 'envio-notificacion',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::EMAIL_RECORDATORY,
            'name' => 'envío de recordatorio',
            'slug' => 'envio-recordatorio',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::USER_ADDED,
            'name' => 'usuario añadido',
            'slug' => 'usuario-anadido',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::CANDIDATE_ADDED,
            'name' => 'candidato añadido',
            'slug' => 'candidato-anadido',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::POLL_ADDED,
            'name' => 'grupo de votaciones añadido',
            'slug' => 'grupo-votaciones-anadido',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::ELECTION_ADDED,
            'name' => 'elección añadida',
            'slug' => 'eleccion-anadida',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::QUESTION_ADDED,
            'name' => 'pregunta añadida',
            'slug' => 'pregunta-anadida',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::GROUP_ADDED,
            'name' => 'grupo añadido',
            'slug' => 'grupo-anadido',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::ELECTOR_ADDED,
            'name' => 'elector añadido',
            'slug' => 'elector-anadido',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::LOGIN,
            'name' => 'ingreso a sistema',
            'slug' => 'ingreso-sistema',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::LOGOUT,
            'name' => 'cierre de sesión',
            'slug' => 'cierre-sesion',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::PASSWORD_SET,
            'name' => 'contraseña inicial establecida',
            'slug' => 'contrasena-inicial',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::PASSWORD_CHANGE,
            'name' => 'contraseña cambiada',
            'slug' => 'contrasena-cambiada',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::RESULTS_ELECTION_DOWNLOADED,
            'name' => 'Resultados de votación descargado',
            'slug' => 'resultados-votacion-descargados',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::RESULTS_POLL_DOWNLOADED,
            'name' => 'resultados de grupo de votaciones descargados',
            'slug' => 'resultados-grupo-votaciones-descargados',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::USER_MODIFIED,
            'name' => 'usuario modificado',
            'slug' => 'usuario-modificado',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::ADDING_ELECTORS_TO_GROUP,
            'name' => 'añadiendo electores al grupo de electores',
            'slug' => 'anadiendo-electores-grupo-electores',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::ELECTORS_ADDED_TO_GROUP,
            'name' => 'electores añadidos al grupo de electores',
            'slug' => 'electores-anadidos-grupo-electores',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::MAILS_PROCESSED,
            'name' => 'correos procesados',
            'slug' => 'correos-procesados',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::MAILS_SENT,
            'name' => 'correos enviados',
            'slug' => 'correos-enviados',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::ADDING_ELECTORS_TO_POLL,
            'name' => 'añadiendo electores al grupo de votaciones',
            'slug' => 'anadiendo-electores-grupo-votaciones',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::ELECTORS_ADDED_TO_POLL,
            'name' => 'electores añadidos al grupo de votaciones',
            'slug' => 'electores-anadidos-grupo-votaciones',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::ERROR_ADDING_ELECTORS_TO_GROUP,
            'name' => 'error añadiendo electores al grupo de electores',
            'slug' => 'error-anadiendo-electores-grupo-electores',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::ERROR_ADDING_ELECTORS_TO_POLL,
            'name' => 'error añadiendo electores al grupo de votaciones',
            'slug' => 'error-anadiendo-electores-grupo-votaciones',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::SYNCING_ELECTORS_TO_GROUP,
            'name' => 'sincronizando electores en el grupo de electores',
            'slug' => 'sincronizando-electores-grupo-electores',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::SYNCING_ELECTORS_TO_POLL,
            'name' => 'sincronizando electores en el grupo de votaciones',
            'slug' => 'sincronizando-electores-grupo-votaciones',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::ELECTORS_PDF_DOWNLOADED,
            'name' => 'lista de electores en pdf descargada',
            'slug' => 'lista-de-electores-en-pdf-descargada',
        ]);
        factory(\App\Action::class, 1)->create([
            'id' => \App\Action::ELECTORS_CSV_DOWNLOADED,
            'name' => 'lista de electores en csv descargada',
            'slug' => 'lista-de-electores-en-csv-descargada',
        ]);


        factory(\App\User::class, 1)->create([
            'name' => 'Inoloop',
            'fathers_last_name' => 'Votaciones',
            'mothers_last_name' => 'Virtuales',
            'email' => 'contacto@inoloop.com',
            'password' => 'secret',
            'role_id' => \App\Role::DEVELOPER,
            'state' => \App\User::ACTIVE,
        ]);

        //////////USERS
        /*
        factory(\App\User::class, 1)->create([
            'name' => 'Administrador',
            'fathers_last_name' => 'Votaciones',
            'mothers_last_name' => 'Virtuales',
            'email' => 'administrador@votaciones-virtuales.com',
            'password' => 'secret',
            'role_id' => \App\Role::ELECTION_CREATOR,
            'state' => \App\User::ACTIVE,
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Auditor 1',
            'fathers_last_name' => 'Votaciones',
            'mothers_last_name' => 'Virtuales',
            'email' => 'auditor@votaciones-virtuales.com',
            'password' => 'secret',
            'role_id' => \App\Role::AUDITOR,
            'state' => \App\User::ACTIVE,
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Auditor 2',
            'fathers_last_name' => 'Votaciones',
            'mothers_last_name' => 'Virtuales',
            'email' => 'auditor2@votaciones-virtuales.com',
            'password' => 'secret',
            'role_id' => \App\Role::AUDITOR,
            'state' => \App\User::ACTIVE,
        ]);
        */

        //$this->call(DatabaseSeederAnt::class);
    }
}
