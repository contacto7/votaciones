<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('electors', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('fathers_last_name');
            $table->string('mothers_last_name');
            $table->string('document_number', 10)->unique();
            $table->string('email');
            $table->string('country_code')->nullable();
            $table->string('cellphone')->nullable();
            $table->string('token_mail')->nullable();
            $table->string('token_cellphone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('electors');
    }
}
