<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_activity_logs', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->unsignedBigInteger('action_id');
            $table->foreign('action_id')->references('id')->on('actions');
            $table->string('description');
            $table->unsignedTinyInteger('state')->default(\App\EmailActivityLog::RECEIVED);
            $table->string('model_received');
            $table->unsignedBigInteger('model_id');
            $table->dateTime('sent_at')->nullable();
            $table->unsignedBigInteger('poll_id')->nullable();
            $table->foreign('poll_id')->references('id')->on('polls');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_activity_logs');
    }
}
