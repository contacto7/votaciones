<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElectionElectorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('election_elector', function (Blueprint $table) {
            $table->unsignedBigInteger('election_id');
            $table->foreign('election_id')->references('id')->on('elections');
            $table->unsignedBigInteger('elector_id');
            $table->foreign('elector_id')->references('id')->on('electors');
            $table->dateTime('voted_at')->nullable();
            $table->unsignedTinyInteger('state')->default(\App\Poll::NOT_VOTED);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('election_elector');
    }
}
