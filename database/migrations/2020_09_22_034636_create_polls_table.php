<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->unsignedTinyInteger('state')->default(\App\Poll::CREATING);
            $table->unsignedTinyInteger('auth_factors')->default(\App\Poll::ONE_FACTOR);

            $table->unsignedBigInteger('user_id')->comment('user that created');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('slug')->nullable();
            $table->tinyInteger('visibility')->default(\App\Poll::PUBLIC);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polls');
    }
}
