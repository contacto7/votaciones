<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElectorPollTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elector_poll', function (Blueprint $table) {
            $table->unsignedBigInteger('poll_id');
            $table->foreign('poll_id')->references('id')->on('polls');
            $table->unsignedBigInteger('elector_id');
            $table->foreign('elector_id')->references('id')->on('electors');
            $table->dateTime('voted_at')->nullable();
            $table->unsignedTinyInteger('state')->default(\App\Poll::NOT_VOTED);
            $table->string('device_uuid')->nullable();
            $table->string('ip_registered')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elector_poll');
    }
}
