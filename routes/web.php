<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
/*
Route::get('/aa', function () {
    $url = "https://inoloop.com/sistema-votaciones/";
    return redirect($url);
});
*/
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/dashboard', function () {
    return redirect('/home/dashboard');
});
/*
//CUSTOM INDEX PAGES
Route::get('/', 'Controller@indexPage')->name('index');
Route::get('/home', 'Controller@indexPage')->name('index');
*/

Route::group(['middleware' => ['auth']], function (){
    Route::group(['prefix' => 'elections'], function (){
        Route::get('/list','ElectionController@list')
            ->name('elections.list');
        Route::get('/info/{id}','ElectionController@info')
            ->name('elections.info');
        Route::get('/results/{id}','ElectionController@results')
            ->name('elections.results');
        Route::get('/resultsPDF/{id}','ElectionController@resultsPDF')
            ->name('elections.resultsPDF');
        Route::get('/testPDF/{id}','ElectionController@testPDF')
            ->name('elections.testPDF');
        Route::get('/election-electors/{id}','ElectionController@electionElectors')
            ->name('elections.electionElectors');
        Route::get('/election-votes/{id}','ElectionController@electionVotes')
            ->name('elections.electionVotes');
        Route::get('/search','ElectionController@filter')
            ->name('elections.search');
        Route::get('/create/{id}','ElectionController@create')
            ->name('elections.create');
        Route::get('/addQuestions/{id}','ElectionController@addQuestions')
            ->name('elections.addQuestions');
        Route::get('/addElectors/{id}','ElectionController@addElectors')
            ->name('elections.addElectors');
        Route::get('/sendNotificationMails/{id}','ElectionController@sendNotificationMails')
            ->name('elections.sendNotificationMails');
        Route::get('/electorsSyncStatusAjax','ElectionController@electorsSyncStatusAjax')
            ->name('elections.electorsSyncStatusAjax');
        Route::get('/sendRecordatoryMails/{id}','ElectionController@sendRecordatoryMails')
            ->name('elections.sendRecordatoryMails');
        Route::get('/sendRecordatoryMail/{election_id}/{elector_id}','ElectionController@sendRecordatoryMail')
            ->name('elections.sendRecordatoryMail');
        Route::post('/store','ElectionController@store')
            ->name('elections.store');
        Route::post('/storeElectors','ElectionController@storeElectors')
            ->name('elections.storeElectors');
        Route::put('/update/{id}','ElectionController@update')
            ->name('elections.update');
    });
    Route::group(['prefix' => 'polls'], function (){
        Route::get('/list','PollController@list')
            ->name('polls.list');
        Route::get('/elections/{id}','PollController@elections')
            ->name('polls.elections');
        Route::get('/electionsAjax/{poll_id}','PollController@electionsAjax')
            ->name('polls.electionsAjax');
        Route::get('/info/{id}','PollController@info')
            ->name('polls.info');
        Route::get('/search','PollController@filter')
            ->name('polls.search');
        Route::get('/electionSearch','PollController@electionFilter')
            ->name('polls.electionSearch');
        Route::get('/create','PollController@create')
            ->name('polls.create');
        Route::get('/sendNotificationMails/{id}','PollController@sendNotificationMails')
            ->name('polls.sendNotificationMails');
        Route::get('/sendNotificationSMS/{phone}/{code}','PollController@sendNotificationSMS')
            ->name('polls.sendNotificationSMS');
        Route::get('/sendNotificationMailsAjax/{id}','PollController@sendNotificationMailsAjaX')
            ->name('polls.sendNotificationMailsAjax');
        Route::get('/sendRecordatoryMailsAjax/{id}','PollController@sendRecordatoryMailsAjax')
            ->name('polls.sendRecordatoryMailsAjax');
        Route::get('/sendRecordatoryMails/{id}','PollController@sendRecordatoryMails')
            ->name('polls.sendRecordatoryMails');
        Route::get('/sendRecordatoryMail/{poll_id}/{elector_id}','PollController@sendRecordatoryMail')
            ->name('polls.sendRecordatoryMail');
        Route::post('/store','PollController@store')
            ->name('polls.store');
        Route::put('/update/{id}','PollController@update')
            ->name('polls.update');
        Route::get('/poll-electors/{id}','PollController@pollElectors')
            ->name('polls.pollElectors');
        Route::get('/poll-electors-ajax/{poll_id}','PollController@pollElectorsAjax')
            ->name('polls.pollElectorsAjax');
        Route::get('/poll-votes/{id}','PollController@pollVotes')
            ->name('polls.pollVotes');
        Route::get('/poll-votes-ajax/{id}','PollController@pollVotesAjax')
            ->name('polls.pollVotesAjax');
        Route::get('/resultsPDF/{id}','PollController@resultsPDF')
            ->name('polls.resultsPDF');
        Route::get('/results/{id}','PollController@results')
            ->name('polls.results');
        Route::get('/electorsPDF/{id}','PollController@pollElectorsPDF')
            ->name('polls.pollElectorsPDF');
    });
    Route::group(['prefix' => 'groups'], function (){
        Route::get('/list','GroupController@list')
            ->name('groups.list');
        Route::get('/info/{id}','GroupController@info')
            ->name('groups.info');
        Route::get('/search','GroupController@filter')
            ->name('groups.search');
        Route::get('/electorsSearch','GroupController@electorsFilter')
            ->name('groups.electorsSearch');
        Route::get('/create','GroupController@create')
            ->name('groups.create');
        Route::post('/store','GroupController@store')
            ->name('groups.store');
        Route::get('/processFile','GroupController@processFile')
            ->name('groups.processFile');
        Route::get('/addElectors','GroupController@addElectors')
            ->name('groups.addElectors');
        Route::put('/update/{id}','GroupController@update')
            ->name('groups.update');
        Route::get('/uploadStatusAjax','GroupController@uploadStatusAjax')
            ->name('groups.uploadStatusAjax');
    });
    Route::group(['prefix' => 'questions'], function (){
        Route::get('/create','QuestionController@create')
            ->name('questions.create');
        Route::post('/store','QuestionController@store')
            ->name('questions.store');
        Route::put('/update/{id}','QuestionController@update')
            ->name('questions.update');
    });
    Route::group(['prefix' => 'candidates'], function (){
        Route::get('/create','CandidateController@create')
            ->name('candidates.create');
        Route::post('/store','CandidateController@store')
            ->name('candidates.store');
        Route::put('/update/{id}','CandidateController@update')
            ->name('candidates.update');
    });
    Route::group(['prefix' => 'electors'], function (){
        Route::get('/list','ElectorController@list')
            ->name('electors.list');
        Route::get('/info/{id}','ElectorController@info')
            ->name('electors.info');
        Route::get('/electionList/{id}','ElectorController@electionList')
            ->name('electors.electionList');
        Route::get('/search','ElectorController@filter')
            ->name('electors.search');
        Route::get('/create','ElectorController@create')
            ->name('electors.create');
        Route::post('/store','ElectorController@store')
            ->name('electors.store');
        Route::put('/update/{id}','ElectorController@update')
            ->name('electors.update');
    });
    Route::group(['prefix' => 'users'], function (){
        Route::get('/list','UserController@list')
            ->name('users.list');
        Route::get('/info/{id}','UserController@info')
            ->name('users.info');
        Route::get('/userActivityLog/{id}','UserController@userActivityLog')
            ->name('users.userActivityLog');
        Route::get('/search','UserController@filter')
            ->name('users.search');
        Route::get('/create','UserController@create')
            ->name('users.create');
        Route::post('/store','UserController@store')
            ->name('users.store');
        Route::get('/edit/{id}','UserController@edit')
            ->name('users.edit');
        Route::put('/update/{id}','UserController@update')
            ->name('users.update');
    });
    Route::group(['prefix' => 'email-activity-log'], function (){
        Route::get('/list/poll/{id}','EmailActivityLogController@list')
            ->name('emailActivityLogs.list');
        Route::get('/listAjax/poll/{id}','EmailActivityLogController@listAjax')
            ->name('emailActivityLogs.listAjax');
        Route::get('/search','EmailActivityLogController@filter')
            ->name('emailActivityLogs.search');
    });
    Route::group(['prefix' => 'user-activity-log'], function (){
        Route::get('/list/user/{id}','UserActivityLogController@list')
            ->name('userActivityLogs.list');
        Route::get('/list-updates-to-user/user/{id}','UserActivityLogController@listUpdatesToUser')
            ->name('userActivityLogs.listUpdatesToUser');
        Route::get('/list-updates-to-elector/elector/{id}','UserActivityLogController@listUpdatesToElector')
            ->name('userActivityLogs.listUpdatesToElector');
        Route::get('/search','UserActivityLogController@filter')
            ->name('userActivityLogs.search');
    });
});
Route::group(['prefix' => 'votes'], function (){
    /*
    Route::get('/insert-dni/{poll_id}/{elector_id}/{token_mail}','VoteController@insertDni')
        ->name('votes.insertDni');
    */
    Route::get('/insert-dni/{poll_id_elector_id_token_mail}','VoteController@insertDni')
        ->name('votes.insertDni');
    //    ->middleware('accepted.browsers');
    Route::post('/check-dni/{poll_id_elector_id_token_mail}','VoteController@checkDni')
        ->name('votes.checkDni')
        ->middleware('penalization.time.validation')
        ->middleware('device.validation');
    Route::get('/vote','VoteController@vote')
        ->name('votes.vote')->middleware(\App\Http\Middleware\CheckVoteSession::class);
    Route::post('/store/{poll_id_elector_id_token_mail}','VoteController@store')
        ->name('votes.store')->middleware(\App\Http\Middleware\CheckVoteSession::class);
    Route::get('/voteNotOpen/{poll_id}/{elector_id}/{token_mail}','VoteController@voteNotOpen')
        ->name('votes.voteNotOpen');
    Route::get('/error/{error_code}','VoteController@error')
        ->name('votes.error');
});

Route::group(['prefix' => 'users'], function (){
    Route::get('/confirmMail/{user_id}/{token_mail}','UserController@confirmMail')
        ->name('users.confirmMail');
    Route::put('/storeInitialPassword/{user_id}/{token_mail}','UserController@storeInitialPassword')
        ->name('users.storeInitialPassword');
});

Route::group(['prefix' => 'errors'], function (){
    Route::get('/invalid-browser','Controller@invalidBrowser')
        ->name('errors.invalidBrowser');
    Route::get('/forbidden-device','Controller@forbiddenDevice')
        ->name('errors.forbiddenDevice');
});

Route::group(['prefix' => 'tests'], function (){
    Route::get('/browser','TestController@browser')
        ->name('tests.browser');
    //    ->middleware('accepted.browsers');
    Route::get('/reniec/{dni?}','TestController@reniec')
        ->name('tests.reniec');
    Route::get('/fetchTinyUrl','TestController@fetchTinyUrl')
        ->name('tests.fetchTinyUrl');
    Route::get('/poll-electors-pdf/{id}','TestController@pollElectorsPDF')
        ->name('tests.pollElectorsPDF');
    Route::get('/results','TestController@results')
        ->name('tests.results');
});

Route::get('public-results/{slug?}','PollController@publicResults')
    ->name('polls.publicResults');




Route::get('images/{path}/{attachment}', function ($path, $attachment){
    $file = sprintf('storage/%s/%s', $path, $attachment);

    if(File::exists($file)){
        return \Intervention\Image\Facades\Image::make($file)->response();
    }else{
        return \Intervention\Image\Facades\Image::make('image/not-found.png')->response();
    }
});
