<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Poll
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $elections
 * @property-read int|null $elections_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Poll newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Poll newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Poll query()
 * @mixin \Eloquent
 */
class Poll extends Model
{
    const ONE_FACTOR = 1;
    const TWO_FACTOR = 2;
    const RENIEC_VALIDATION = 3;

    const CREATING = 1;
    const ELECTION_CREATED = 2;
    const QUESTIONS_ADDED = 3;
    const ELECTORS_ADDED = 4;
    const CREATED = 5;
    const FOR_OPEN = 6;
    const OPEN = 7;
    const CLOSED = 8;
    const DELETED = 9;
    const TERMINATED = 10;
    const ARCHIVED = 11;
    const MAILS_SENDED = 12;
    const MAILS_RE_SENDED = 13;

    const NOT_VOTED = 1;
    const VOTED = 2;

    const PUBLIC = 1;
    const PRIVATE = 2;

    public $timestamps = true;

    protected $fillable = [
        'name','start_date','end_date','state','auth_factors','user_id','slug','visibility',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function elections(){
        return $this->hasMany(Election::class);
    }

    public function electors(){
        return $this
            ->belongsToMany(Elector::class)
            ->withPivot('voted_at', 'state', 'device_uuid', 'ip_registered')
            ->withTimestamps()
            ->using(ElectorPoll::class);
    }
}
