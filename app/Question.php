<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Question
 *
 * @property int $id
 * @property string $name
 * @property int $order
 * @property int $election_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Candidate[] $candidates
 * @property-read int|null $candidates_count
 * @property-read \App\Election $election
 * @method static \Illuminate\Database\Eloquent\Builder|Question newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Question newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Question query()
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereElectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Question extends Model
{
    protected $fillable = [
        'name','order','election_id',
    ];

    public function election(){
        return $this->belongsTo(Election::class);
    }
    public function candidates(){
        return $this->hasMany(Candidate::class)->orderBy('order','ASC');
    }
    public function candidatesOrdered(){
        /*
        $question_id = $this->id;
        $candidates = Candidate::
            where('question_id', $question_id)
            ->get()
            ->sortBy(function($candidate)
            {
                return $candidate->votes->count();
            });
        return $candidates;
        */
        $question_id = $this->id;
        $candidates = Candidate::
        where('question_id', $question_id)
            ->withCount('votes')
            ->orderBy('votes_count', 'DESC')
            ->orderBy('order', 'ASC')
            ->get();
        return $candidates;
    }
}
