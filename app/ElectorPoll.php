<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ElectorPoll extends Pivot
{
    public function setUpdatedAtAttribute($value) {
        $dt = new \DateTime();
        $updated_at = $dt->format('y-m-d H:00:00');
        $this->attributes['updated_at'] = $updated_at;
    }
}
