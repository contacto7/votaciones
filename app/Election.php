<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Election
 *
 * @property int $id
 * @property string $name
 * @property string $start_date
 * @property string $end_date
 * @property int $state
 * @property int $user_id user that created
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Elector[] $electors
 * @property-read int|null $electors_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Question[] $questions
 * @property-read int|null $questions_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Election newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Election newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Election query()
 * @method static \Illuminate\Database\Eloquent\Builder|Election whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Election whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Election whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Election whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Election whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Election whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Election whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Election whereUserId($value)
 * @mixin \Eloquent
 */
class Election extends Model
{

    protected $fillable = [
        'name','group_id','user_id','poll_id',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function group(){
        return $this->belongsTo(Group::class);
    }

    public function poll(){
        return $this->belongsTo(Poll::class);
    }
    public function questions(){
        return $this->hasMany(Question::class)->orderBy('order','ASC');
    }

    /*
    public function electors(){
        return $this
            ->poll()
            ->electors()
            ->belongsToMany(Elector::class)
            ->withPivot('voted_at', 'state')
            ->withTimestamps();
    }
    public function votesCount(){
        //$electors = $this->electors()->where('voted_at','!=', null)->count();
        $electors = $this->electors()->where('voted_at','!=', null);
        return $electors;
    }
*/

}
