<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Action
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EmailActivityLog[] $emailActivityLogs
 * @property-read int|null $email_activity_logs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserActivityLog[] $userActivityLogs
 * @property-read int|null $user_activity_logs_count
 * @method static \Illuminate\Database\Eloquent\Builder|Action newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Action newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Action query()
 * @mixin \Eloquent
 */
class Action extends Model
{
    const EMAIL_NOTIFICATION_BULK = 1;
    const EMAIL_RECORDATORY_BULK = 2;
    const EMAIL_NOTIFICATION = 3;
    const EMAIL_RECORDATORY = 4;
    const USER_ADDED = 5;
    const CANDIDATE_ADDED = 6;
    const POLL_ADDED = 7;
    const ELECTION_ADDED = 8;
    const QUESTION_ADDED = 9;
    const GROUP_ADDED = 10;
    const ELECTOR_ADDED = 11;
    const LOGIN = 12;
    const LOGOUT = 13;
    const PASSWORD_SET = 14;
    const PASSWORD_CHANGE = 15;
    const RESULTS_ELECTION_DOWNLOADED = 16;
    const RESULTS_POLL_DOWNLOADED = 17;
    const USER_MODIFIED = 18;
    const ADDING_ELECTORS_TO_GROUP = 19;
    const ELECTORS_ADDED_TO_GROUP = 20;
    const MAILS_PROCESSED = 21;
    const MAILS_SENT = 22;
    const ADDING_ELECTORS_TO_POLL = 23;
    const ELECTORS_ADDED_TO_POLL = 24;
    const ERROR_ADDING_ELECTORS_TO_GROUP = 25;
    const ERROR_ADDING_ELECTORS_TO_POLL = 26;
    const SYNCING_ELECTORS_TO_GROUP = 27;
    const SYNCING_ELECTORS_TO_POLL = 28;
    const ELECTORS_PDF_DOWNLOADED = 29;
    const ELECTORS_CSV_DOWNLOADED = 30;


    protected $fillable = [
        'name', 'slug'
    ];
    public function emailActivityLogs(){
        return $this->hasMany(EmailActivityLog::class);
    }
    public function userActivityLogs(){
        return $this->hasMany(UserActivityLog::class);
    }
}
