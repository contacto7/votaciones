<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Candidate
 *
 * @property int $id
 * @property string $name
 * @property int $order
 * @property int $question_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Question $question
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Vote[] $votes
 * @property-read int|null $votes_count
 * @method static \Illuminate\Database\Eloquent\Builder|Candidate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Candidate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Candidate query()
 * @method static \Illuminate\Database\Eloquent\Builder|Candidate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidate whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidate whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidate whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidate whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $list_number
 * @property string $list_name
 * @method static \Illuminate\Database\Eloquent\Builder|Candidate whereListName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Candidate whereListNumber($value)
 */
class Candidate extends Model
{
    protected $fillable = [
        'name','order','list_number','list_name','question_id',
    ];

    public function question(){
        return $this->belongsTo(Question::class);
    }
    public function votes(){
        return $this->hasMany(Vote::class);
    }

    public function votesForThisCandidate(){
        $votes = Vote::where('candidate_id',$this->id)
            ->get()
            ->count();
        return $votes;
    }


}
