<?php

namespace App\Jobs;

use App\Action;
use App\Elector;
use App\EmailActivityLog;
use App\Mail\ElectionNotificationMail;
use App\Poll;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendNotificationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $elector;
    public $poll;
    public $attempt;
    public $action;
    public $mail_local_log;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 0;

    /**
     * The number of seconds to wait before retrying the job.
     *
     * @var int
     */
    public $retryAfter = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Elector $elector, Poll $poll, $action, $attempt)
    {
        $this->elector = $elector;
        $this->poll = $poll;
        $this->action = $action;
        $this->attempt = $attempt;

        $mail_local_log = EmailActivityLog::create([
            'email' => $elector->email,
            'action_id' => $action,
            'description' => 'correo enviado a '.$elector->name." para notificar la elección.",
            'state' => EmailActivityLog::RECEIVED,
            'model_received' => EmailActivityLog::MODEL_ELECTOR,
            'model_id' => $elector->id,
            'sent_at' => now(),
            'poll_id' => $poll->id,
        ]);
        $this->mail_local_log = $mail_local_log;
    }

    /**
     * Execute the job.
     *
     * @return void
     */


    public function handle()
    {

        try {
            \Mail::to($this->elector)->send(new ElectionNotificationMail($this->elector,$this->poll));
        } catch (\Exception $exception) {

            $new_attempt = ($this->attempt) + 1;
            if($new_attempt <= 3){
                $this->reQueueJob($exception, $new_attempt);
            }else{
                $this->failed($exception);
            }
        }
    }

    public function failed(\Exception $exception)
    {
        $this->mail_local_log->fill([
            'state' => EmailActivityLog::ERROR,
            'sent_at' => NULL,
        ])->save();
        $this->fail();
    }

    public function reQueueJob(\Exception $exception, $new_attempt)
    {
        $this->delete();

        $delay_seconds = $new_attempt;
        if ($this->job->isDeleted()) {
            dispatch(new self($this->elector,$this->poll, $this->action, $new_attempt))
                ->delay(Carbon::now()->addSeconds($delay_seconds));
        }

    }
}
