<?php

namespace App\Jobs;

use App\Group;
use App\Http\Controllers\GroupController;
use App\UserActivityLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessGroupDoc implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $group;
    public $jobActivityLog;
    public $files_data;
    public $timeout = 0;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Group $group, UserActivityLog $jobActivityLog, $files_data)
    {
        $this->group = $group;
        $this->jobActivityLog = $jobActivityLog;
        $this->files_data = $files_data;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $group_controller = New GroupController();
        $group = $this->group;
        $jobActivityLog = $this->jobActivityLog;
        $files_data = $this->files_data;

        $group_controller->processFile($group, $jobActivityLog, $files_data);
    }
}
