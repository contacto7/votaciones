<?php

namespace App\Jobs;

use App\Group;
use App\Http\Controllers\ElectionController;
use App\Election;
use App\UserActivityLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddElectorsToPoll implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $group;
    public $jobActivityLog;
    public $election;
    public $timeout = 0;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Group $group, Election $election, UserActivityLog $jobActivityLog)
    {
        $this->group = $group;
        $this->jobActivityLog = $jobActivityLog;
        $this->election = $election;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $electionController = New ElectionController();
        $group = $this->group;
        $jobActivityLog = $this->jobActivityLog;
        $election = $this->election;

        $electionController->syncElectorsToPoll($group, $election, $jobActivityLog);
    }
}
