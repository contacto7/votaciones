<?php

namespace App\Jobs;

use App\Group;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncElectors implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $group;
    public $electors_id_array_chunked;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Group $group, $electors_id_array_chunked)
    {
        $this->group = $group;
        $this->electors_id_array_chunked = $electors_id_array_chunked;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $group = $this->group;
        $electors_id_array_chunked = $this->electors_id_array_chunked;
        $group->electors()->sync($electors_id_array_chunked);
    }
}
