<?php

namespace App\Mail;

use App\Poll;
use App\Elector;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ElectionNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Elector $elector, Poll $poll)
    {
        $this->elector = $elector;
        $this->poll = $poll;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject("Notificación de elecciones: ".$this->poll->name)
            ->markdown("emails.election_notification_mail")
            ->with('elector', $this->elector)
            ->with('poll', $this->poll);
    }
}
