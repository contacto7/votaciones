<?php

namespace App\Mail;

use App\Election;
use App\Elector;
use App\Poll;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ElectionRecordatoryMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $elector;

    protected $poll;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Elector $elector, Poll $poll)
    {
        $this->elector = $elector;
        $this->poll = $poll;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject("Recordatorio de elección: ".$this->poll->name)
            ->markdown("emails.election_recordatory_mail")
            ->with('elector', $this->elector)
            ->with('poll', $this->poll);
    }
}
