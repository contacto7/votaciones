<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CountryCode implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //CODES STARTING WITH NO-ZERO NUMBERS, LENGTH 1-4
        return preg_match('/^([1-9][0-9]{0,3})$/', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El código de pais no es válido. El código debe tener sólo números, no debe tener paréntesis, ni cero o símbolo de más por delante.';
    }
}
