<?php

namespace App\Policies;

use App\Election;
use App\Poll;
use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ElectionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->role_id === Role::DEVELOPER || $user->role_id === Role::ADMIN || $user->role_id === Role::AUDITOR || $user->role_id === Role::ELECTION_CREATOR;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Election  $election
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->role_id === Role::DEVELOPER || $user->role_id === Role::ADMIN || $user->role_id === Role::AUDITOR || $user->role_id === Role::ELECTION_CREATOR;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role_id === Role::DEVELOPER || $user->role_id === Role::ADMIN || $user->role_id === Role::ELECTION_CREATOR;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Election  $election
     * @return mixed
     */
    public function update(User $user, Election $election)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Election  $election
     * @return mixed
     */
    public function delete(User $user, Election $election)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Election  $election
     * @return mixed
     */
    public function restore(User $user, Election $election)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Election  $election
     * @return mixed
     */
    public function forceDelete(User $user, Election $election)
    {
        //
    }

    /**
     * Determine whether the user can view the model electors.
     *
     * @param  \App\User  $user
     * @param  \App\Election  $election
     * @return mixed
     */
    public function viewElectors(User $user)
    {
        return $user->role_id === Role::DEVELOPER || $user->role_id === Role::ADMIN || $user->role_id === Role::AUDITOR || $user->role_id === Role::ELECTION_CREATOR;
    }

    /**
     * Determine whether the user can view the model votes.
     *
     * @param  \App\User  $user
     * @param  \App\Election  $election
     * @return mixed
     */
    public function viewVotes(User $user)
    {
        return $user->role_id === Role::DEVELOPER || $user->role_id === Role::ADMIN || $user->role_id === Role::AUDITOR || $user->role_id === Role::ELECTION_CREATOR;
    }

    /**
     * Determine whether the user can send emails to electors.
     *
     * @param  \App\User  $user
     * @param  \App\Election  $election
     * @return mixed
     */
    public function sendEmails(User $user)
    {
        return $user->role_id === Role::DEVELOPER || $user->role_id === Role::ADMIN || $user->role_id === Role::ELECTION_CREATOR;
    }


    /**
     * Determine whether the user can view the results.
     *
     * @param  \App\User  $user
     * @param  \App\Election  $election
     * @return mixed
     */
    public function viewResults(User $user)
    {
        return $user->role_id === Role::DEVELOPER || $user->role_id === Role::ADMIN || $user->role_id === Role::AUDITOR || $user->role_id === Role::ELECTION_CREATOR;
    }

    /**
     * Determine whether the user can add questions.
     *
     * @param  \App\User  $user
     * @param  \App\Election  $election
     * @return mixed
     */
    public function addQuestions(User $user)
    {
        return $user->role_id === Role::DEVELOPER || $user->role_id === Role::ADMIN || $user->role_id === Role::ELECTION_CREATOR;
    }

    /**
     * Determine whether the user can add questions.
     *
     * @param  \App\User  $user
     * @param  \App\Election  $election
     * @return mixed
     */
    public function addQuestionsAnymore(User $user, Poll $poll)
    {
        $start_date = $poll->start_date;
        return $start_date > now();
    }

}
