<?php

namespace App\Policies;

use App\Elector;
use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ElectorPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->role_id === Role::DEVELOPER || $user->role_id === Role::ADMIN || $user->role_id === Role::AUDITOR || $user->role_id === Role::ELECTION_CREATOR;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Elector  $elector
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->role_id === Role::DEVELOPER || $user->role_id === Role::ADMIN || $user->role_id === Role::AUDITOR || $user->role_id === Role::ELECTION_CREATOR;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role_id === Role::DEVELOPER || $user->role_id === Role::ADMIN || $user->role_id === Role::ELECTION_CREATOR;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Elector  $elector
     * @return mixed
     */
    public function update(User $user, Elector $elector)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Elector  $elector
     * @return mixed
     */
    public function delete(User $user, Elector $elector)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Elector  $elector
     * @return mixed
     */
    public function restore(User $user, Elector $elector)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Elector  $elector
     * @return mixed
     */
    public function forceDelete(User $user, Elector $elector)
    {
        //
    }
}
