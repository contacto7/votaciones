<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasswordReset extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $count = config('auth.passwords.'.config('auth.defaults.passwords').'.expire');
        $expiration = 'Este link expirará en '.$count.' minutos.';
        return (new MailMessage)
            ->subject('Restablecer contraseña')
            ->line('Estás recibiendo este correo porque recibimos una solicitud de restablecer contraseña.') // Here are the lines you can safely override
            ->action('Restablecer Contraseña', url('password/reset', $this->token))
            ->line($expiration)
            ->line('Si no solicitaste restablecer tu contraseña, no debes hacer nada más.');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
