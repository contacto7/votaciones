<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Elector
 *
 * @property int $id
 * @property string $name
 * @property string $last_name
 * @property string $document_number
 * @property string $email
 * @property string $token_mail
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Election[] $elections
 * @property-read int|null $elections_count
 * @method static \Illuminate\Database\Eloquent\Builder|Elector newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Elector newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Elector query()
 * @method static \Illuminate\Database\Eloquent\Builder|Elector whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Elector whereDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Elector whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Elector whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Elector whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Elector whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Elector whereTokenMail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Elector whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $fathers_last_name
 * @property string $mothers_last_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @property-read int|null $groups_count
 * @method static \Illuminate\Database\Eloquent\Builder|Elector whereFathersLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Elector whereMothersLastName($value)
 */
class Elector extends Model
{
    public $timestamps = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
        'token_mail', 'token_cellphone',
    ];

    protected $fillable = [
        'name', 'fathers_last_name', 'mothers_last_name', 'document_number',
        'email', 'country_code', 'cellphone', 'token_mail','token_cellphone', 'created_at', 'updated_at'
    ];

    public function groups(){
        return $this
            ->belongsToMany(Group::class)
            ->withPivot('weight')
            ->withTimestamps();
    }

    public function polls(){
        return $this
            ->belongsToMany(Poll::class)
            ->withPivot('voted_at', 'state', 'device_uuid', 'ip_registered')
            ->withTimestamps()
            ->using(ElectorPoll::class);
    }
}
