<?php

namespace App\Http\Controllers;

use App\Action;
use App\Election;
use App\Elector;
use App\Http\Requests\ElectorRequest;
use App\Http\Requests\ElectorsFromElectionRequest;
use App\Mail\ElectionNotificationMail;
use App\UserActivityLog;
use Carbon\Carbon;
use Dotenv\Store\File\Reader;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Http\Request;
use Kfirba\QueryGenerator;
use Illuminate\Support\Str;

class ElectorController extends Controller
{


    public function list(){

        //POLICIES
        $canListElectors = auth()->user()->can('viewAny', [Elector::class] );
        //END POLICIES

        if(! $canListElectors ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver la lista de electores.")]);
        }

        $electors = Elector::with([])
            ->orderBy('id', 'desc')
            ->paginate(25);

        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Electores";
        $bread_1->route = route('electors.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Lista";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;

        $electors_total_count = Elector::get()->count();
        //dd($electors);
        return view('electors.list', compact('electors', 'bread_arr', 'electors_total_count'));
    }

    public function filter(Request $request, Elector $electors){

        //$company = $company->newQuery();
        $electors = $electors::with([]);

        $search_input = $request->has('name_search') ? $request->input('name_search'): null;

        //POLICIES
        $canListElectors = auth()->user()->can('viewAny', [Elector::class] );
        //END POLICIES

        if(! ($canListElectors) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite listar los electores.")]);
        }

        $search_input_expanded = str_replace(" ", "%", $search_input);
        //dd($search_input_expanded);

        // Search for a user based on their name.
        if ($search_input) {
            $electors
                ->where(function($query) use ($search_input, $search_input_expanded){
                    $query
                        ->where('fathers_last_name','LIKE', '%'.$search_input.'%')
                        ->orWhere('mothers_last_name','LIKE', '%'.$search_input.'%')
                        ->orWhereRaw("concat(name, ' ', fathers_last_name, ' ', mothers_last_name) like '%$search_input_expanded%' ")
                        ->orWhere('name','LIKE', '%'.$search_input.'%')
                        ->orWhere('document_number', $search_input)
                        ->orWhere('email', $search_input);
                });
        }

        $electors =  $electors->orderBy('id', 'desc')->paginate(25);


        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Electores";
        $bread_1->route = route('electors.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Filtro de búsqueda";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;
        //dd($movements);
        $electors_total_count = Elector::get()->count();

        return view('electors.list', compact('electors', 'bread_arr', 'electors_total_count'));

    }

    public function info($id){

        //POLICIES
        $canListThisElector = auth()->user()->can('view', [Elector::class] );
        //END POLICIES

        if(!$canListThisElector ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver este elector.")]);
        }

        $elector = Elector::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();


        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Electores";
        $bread_1->route = route('electors.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Detalle de elector";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;
        //dd($companies);
        return view('electors.info', compact('elector', 'bread_arr'));
    }

    public function electionList($id){

        //POLICIES
        $canListThisElector = auth()->user()->can('view', [Elector::class] );
        //END POLICIES

        if(! $canListThisElector ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver este elector.")]);
        }

        $elector = Elector::with(['groups'])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $elections = Election::with([
            'group',
            'poll.electors'=> function($query) use ($id){
                $query->where("id", $id)->get();   //your popular comment or other logic
            },
        ])
            ->whereHas("group", function ($q) use ($id) {
                $q->whereHas("electors", function ($q) use ($id) {
                    $q->where("id", $id);
                });
            })
            ->orderBy('id', 'desc')
            ->paginate(25);


        //dd($elections[0]->poll->electors[0]->pivot);
        //dd($elections->poll->electors->where('id', $id)->first->pivot);

        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Electores";
        $bread_1->route = route('electors.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $elector->name." ".$elector->fathers_last_name." ".$elector->mothers_last_name;
        $bread_2->route = route('electors.info',$elector->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Lista de elecciones en que participó";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;

        return view('electors.electionsList', compact('elections','elector', 'bread_arr'));
    }

    public function create(){
        $elector = new Elector();

        //POLICIES
        $canCreateElectors = auth()->user()->can('create', [Elector::class] );
        //END POLICIES

        if(! $canCreateElectors){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite crear electores.")]);
        }

        $btnText = __("Crear Elector");


        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Electores";
        $bread_1->route = route('electors.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Agregar elector";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;
        return view('electors.create', compact('elector','btnText', 'bread_arr'));
    }

    public function store(ElectorRequest $electorRequest){

        //POLICIES
        $canCreateElectors = auth()->user()->can('create', [Elector::class] );
        //END POLICIES

        if(! $canCreateElectors){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite crear electores.")]);
        }

        $token_mail = Str::random(22);

        $electorRequest->merge(['token_mail' => $token_mail ]);

        try {
            $elector = Elector::create($electorRequest->input());


            UserActivityLog::create([
                'user_id' => auth()->user()->id,
                'action_id' => Action::ELECTOR_ADDED,
                'description' => 'El usuario '.auth()->user()->name.' añadió el elector.',
                'model_modified' => 'Elector',
                'model_id' => $elector->id,
            ]);

            return back()->with('modalMessage',['Aviso',
                __("Se agregó correctamente el elector.")]);
        } catch(\Illuminate\Database\QueryException $e){
            return back()->with('modalMessage',['Aviso',
                __("Hubo un error agregando el elector,
                por favor verifique que está colocando los datos requeridos.")]);

        }
    }

}
