<?php

namespace App\Http\Controllers;

use App\EmailActivityLog;
use App\Poll;
use http\Client\Curl\User;
use Illuminate\Http\Request;

class EmailActivityLogController extends Controller
{
    public function list($id){

        //POLICIES
        $canListEmailActivityLog = true;
        //END POLICIES

        if(! $canListEmailActivityLog ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver los registros de correos.")]);
        }

        $emailActivityLogs = EmailActivityLog::with([
            'electorReceived',
            'action'
        ])
            ->where('poll_id',$id)
            ->orderBy('id', 'desc')
            ->paginate(25);

        $poll = Poll::where('id',$id)->first();

        //$poll = $poll->elections()->electors()->paginate(25);
        //dd($emailActivityLogs);
        return view('emailActivityLogs.list', compact('emailActivityLogs', 'poll'));
    }
    public function listAjax(Request $request, $id){

        //POLICIES
        $canListEmailActivityLog = true;
        //END POLICIES

        if(! $canListEmailActivityLog ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver los registros de correos.")]);
        }

        $search_input = $request->has('search_input') ? $request->input('search_input'): null;


        $emailActivityLogs = EmailActivityLog::with([
            'electorReceived',
            'action'
        ])
            ->where('poll_id',$id);

        if($search_input){
            $electors = new \App\Elector();
            $electors = $electors
                ->where(function($query) use ($search_input){
                    $query
                        ->where('fathers_last_name','LIKE', '%'.$search_input.'%')
                        ->orWhere('mothers_last_name','LIKE', '%'.$search_input.'%')
                        ->orWhere('name','LIKE', '%'.$search_input.'%')
                        ->orWhereRaw("concat(name, ' ', fathers_last_name, ' ', mothers_last_name) like '%$search_input%' ")
                        ->orWhere('document_number', $search_input)
                        ->orWhere('email', $search_input);
                });
            $electors = $electors->whereHas("polls", function ($q) use ($id) {
                $q->where("id", $id);
            })->pluck('id')->toArray();
            $emailActivityLogs = $emailActivityLogs->whereIn('model_id',$electors);
        }

        $emailActivityLogs = $emailActivityLogs
            ->orderBy('id', 'desc')
            ->paginate(25);

        $poll = Poll::where('id',$id)->first();

        //$poll = $poll->elections()->electors()->paginate(25);
        //dd($emailActivityLogs);
        return view('partials.polls.emails', compact('emailActivityLogs', 'poll'));
    }

    public function filter(Request $request, EmailActivityLog $emailActivityLogs){

        //POLICIES
        $canListEmailActivityLog = true;
        //END POLICIES

        if(! $canListEmailActivityLog ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver los registros de correos.")]);
        }

        $emailActivityLogs = $emailActivityLogs::with([]);
        $electors = new \App\Elector();

        $poll_id = $request->has('poll_id') ? $request->input('poll_id'): null;
        $name_search = $request->has('name_search') ? $request->input('name_search'): null;
        $last_name_search = $request->has('last_name_search') ? $request->input('last_name_search'): null;
        $document_number_search = $request->has('document_number_search') ? $request->input('document_number_search'): null;

        // Search for a user based on their name.
        if (!$poll_id) {
            return back()->with('modalMessage',['Aviso', "Ocurrió un error. Por favor ingrese de nuevo al registro de correos."]);
        }else{
            $poll = Poll::where('id',$poll_id)->first();
        }

        // Search for a user based on their name.
        if ($name_search) {
            $electors = $electors->where('name','LIKE', '%'.$name_search.'%');
        }

        // Search for a user based on their name.
        if ($last_name_search) {
            $electors =
                $electors
                    ->where('fathers_last_name','LIKE', '%'.$last_name_search.'%')
                    ->orWhere('mothers_last_name','LIKE', '%'.$last_name_search.'%');
        }

        // Search for a user based on their name.
        if ($document_number_search) {
            $electors = $electors->where('document_number', $document_number_search);
        }

        $electors = $electors->whereHas("polls", function ($q) use ($poll_id) {
                    $q->where("id", $poll_id);
                })->pluck('id')->toArray();


        $emailActivityLogs =
            $emailActivityLogs
                ->with([
                    'electorReceived',
                    'action'
                ])
                ->where('poll_id',$poll_id)
                ->whereIn('model_id',$electors)
                ->orderBy('id', 'desc')
                ->paginate(25);

        //dd($movements);
        return view('emailActivityLogs.list', compact('emailActivityLogs', 'poll'));

    }
}
