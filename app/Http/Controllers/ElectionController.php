<?php

namespace App\Http\Controllers;

use App\Action;
use App\Election;
use App\Elector;
use App\Group;
use App\Http\Requests\ElectionRequest;
use App\Jobs\AddElectorsToPoll;
use App\Jobs\SendRecordatoryEmail;
use App\Mail\ElectionNotificationMail;
use App\Mail\ElectionRecordatoryMail;
use App\Poll;
use App\Question;
use App\User;
use App\UserActivityLog;
use App\Vote;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

class ElectionController extends Controller
{
    public function list(){

        //POLICIES
        $canListElections = auth()->user()->can('view', [Election::class] );
        $canListAllElections = auth()->user()->can('viewAny', [Election::class] );
        //END POLICIES

        if(! ($canListElections || $canListAllElections) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite listar las elecciones.")]);
        }

        $elections = Election::
            orderBy('end_date','desc')
            ->paginate(25);


        //dd($elections);
        return view('elections.list', compact('elections') );
    }

    public function info($id){

        //POLICIES
        $canListThisElection = auth()->user()->can('view', [Election::class] );
        //END POLICIES

        if(! $canListThisElection ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver esta elección.")]);
        }

        $election = Election::with([
            'poll',
            'group'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $group = $election->group;
        $group_id = $group->id;


        $electorsCount = Elector::with(['groups'])
            ->whereHas("polls", function ($q) use ($id) {
                $q->whereHas("elections", function ($q) use ($id) {
                    $q->where("id", $id);
                });
            })
            ->whereHas("groups", function ($q) use ($group_id) {
                $q->where("id", $group_id);
            })
            ->orderBy('id', 'desc')
            ->count();

        $votesCount = Elector::with(['groups'])
            ->whereHas("polls", function ($q) use ($id) {
                $q
                    ->whereHas("elections", function ($q) use ($id) {
                        $q->where("id", $id);
                    })
                    ->where("elector_poll.state", Poll::VOTED);
            })
            ->whereHas("groups", function ($q) use ($group_id) {
                $q->where("id", $group_id);
            })
            ->orderBy('id', 'desc')
            ->count();

        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $election->poll->name;
        $bread_2->route = route('polls.info',$election->poll->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Detalle de votación";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;

        return view('elections.info', compact('election', 'votesCount', 'electorsCount', 'bread_arr'));
    }

    public function results($id){

        //POLICIES
        $canListThisElectionResults = auth()->user()->can('viewResults', [Election::class] );
        //END POLICIES

        if(! $canListThisElectionResults ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver estos resultados.")]);
        }

        $election = Election::with([
            'questions.candidates.votes'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $election->poll->name;
        $bread_2->route = route('polls.info',$election->poll->id);
        $bread_arr[]=$bread_2;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = "Lista de votaciones";
        $bread_2->route = route('polls.elections',$election->poll->id);
        $bread_arr[]=$bread_2;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = "Detalle de votación";
        $bread_2->route = route('elections.info',$election->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Resultados de Votación";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;
        //dd($companies);

        $election_id = $id;
        $group = $election->group;
        $poll = $election->poll;
        $group_id = $group->id;

        $total_electors = $group->electors()->count();
        $votesCount = \App\Elector::with(['groups'])
            ->whereHas("polls", function ($q) use ($election_id) {
                $q
                    ->whereHas("elections", function ($q) use ($election_id) {
                        $q->where("id", $election_id);
                    })
                    ->where("elector_poll.state", \App\Poll::VOTED);
            })
            ->whereHas("groups", function ($q) use ($group_id) {
                $q->where("id", $group_id);
            })
            ->count();

        return view('elections.results', compact('election', 'poll', 'total_electors', 'votesCount', 'bread_arr'));
    }

    public function testPDF($id){

        //POLICIES
        $canListThisElectionResults = auth()->user()->can('viewResults', [Election::class] );
        //END POLICIES

        if(! $canListThisElectionResults ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver estos resultados.")]);
        }

        $election = Election::with([
            'questions.candidates.votes'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //dd($companies);
        return view('pdfs.resultsPDF', compact('election'));
    }

    public function resultsPDF($id){

        //POLICIES
        $canListThisElectionResults = auth()->user()->can('viewResults', [Election::class] );
        //END POLICIES

        if(! $canListThisElectionResults ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver estos resultados.")]);
        }

        $election = Election::with([
            'questions.candidates.votes'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $election_id = $id;
        $group = $election->group;
        $poll = $election->poll;
        $group_id = $group->id;

        $total_electors = $group->electors()->count();
        $votesCount = \App\Elector::with(['groups'])
            ->whereHas("polls", function ($q) use ($election_id) {
                $q
                    ->whereHas("elections", function ($q) use ($election_id) {
                        $q->where("id", $election_id);
                    })
                    ->where("elector_poll.state", \App\Poll::VOTED);
            })
            ->whereHas("groups", function ($q) use ($group_id) {
                $q->where("id", $group_id);
            })
            ->count();

        $pdf = PDF::loadView('pdfs.resultsPDF', compact('election', 'poll', 'total_electors', 'votesCount'));


        UserActivityLog::create([
            'user_id' => auth()->user()->id,
            'action_id' => Action::RESULTS_ELECTION_DOWNLOADED,
            'description' => 'El usuario '.auth()->user()->name.' descargó el resultado de la votación.',
            'model_modified' => 'Election',
            'model_id' => $election->id,
        ]);

        // download PDF file with download method
        return $pdf->download('resultados-de-votacion.pdf');

    }

    public function electionElectors($id){

        //POLICIES
        $canListThisElectionElectors = auth()->user()->can('viewElectors', [Election::class] );
        //END POLICIES

        if(! $canListThisElectionElectors ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver la lista de electores.")]);
        }

        $election = Election::with([
            'poll',
            'group'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $group = $election->group;
        $group_id = $group->id;


        $poll = Poll::with([])
            ->where('id',$election->poll_id)
            ->orderBy('id', 'desc')
            ->first();

        $electors = $poll->electors()
            ->orderBy('id', 'desc')
            ->whereHas("groups", function ($q) use ($group_id) {
                $q->where("id", $group_id);
            })
            ->paginate(25);


        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $election->poll->name;
        $bread_2->route = route('polls.info',$election->poll->id);
        $bread_arr[]=$bread_2;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = "Lista de votaciones";
        $bread_2->route = route('polls.elections',$election->poll->id);
        $bread_arr[]=$bread_2;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = "Detalle de votación";
        $bread_2->route = route('elections.info',$election->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Lista de Electores";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;
        //dd($electors);
        return view('elections.electionElectors', compact('election','electors', 'poll','bread_arr'));
    }

    public function electionVotes($id){

        //POLICIES
        $canListThisElectionVotes = auth()->user()->can('viewVotes', [Election::class] );
        //END POLICIES

        if(! $canListThisElectionVotes ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver los votos de esta elección.")]);
        }

        $votes =  Vote::with([
            'candidate.question.election'
        ])
            ->whereHas("candidate", function ($q) use ($id) {
                $q->whereHas("question", function ($q) use ($id) {
                    $q->where("election_id", $id);
                });
            })
            ->orderBy('id', 'desc')
            ->paginate(25);

        $election = Election::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //dd($votes);


        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $election->poll->name;
        $bread_2->route = route('polls.info',$election->poll->id);
        $bread_arr[]=$bread_2;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = "Lista de votaciones";
        $bread_2->route = route('polls.elections',$election->poll->id);
        $bread_arr[]=$bread_2;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = "Detalle de votación";
        $bread_2->route = route('elections.info',$election->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Detalle de votos";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;
        return view('elections.electionVotes', compact('votes', 'election', 'bread_arr'));
    }

    public function filter(Request $request, Election $elections){

        //POLICIES
        $canListElections = auth()->user()->can('view', [Election::class] );
        $canListAllElections = auth()->user()->can('viewAny', [Election::class] );
        //END POLICIES

        if(! ($canListElections || $canListAllElections) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite listar las elecciones.")]);
        }

        $name = $request->has('name') ? $request->input('name'): null;

        $elections = $elections->newQuery();
        //$proformas = $proformas::with([]);

        if ($name) {
            $elections->where("name",'like','%'.$name.'%');
        }


        $elections = $elections->orderBy('id', 'desc')->paginate(25);

        //dd($movements);
        return view('elections.list', compact('elections'));

    }


    public function create($id){

        $election = new Election();

        $poll = Poll::where('id',$id)->first();

        //POLICIES
        $canCreateElection = auth()->user()->can('create', [Election::class] );
        $canAddQuestionsAnymore = auth()->user()->can('updateAnymore', [Poll::class, $poll] );
        if(! $canCreateElection ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite agregar elecciones.")]);
        }
        if(! $canAddQuestionsAnymore ){
            return redirect()->route('polls.elections', $poll->id)->with('modalMessage',['Aviso', __("Ya no se puede modificar la información en el grupo de votaciones, porque ya empezó la elección.")]);
        }
        //END POLICIES

        $groups = Group::orderBy('id', 'desc')->take(100)->get();

        $btnText = __("Crear elección");


        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $poll->name;
        $bread_2->route = route('polls.info',$poll->id);
        $bread_arr[]=$bread_2;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = "Lista de votaciones";
        $bread_2->route = route('polls.elections',$poll->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Agregar Votación";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;
        return view('elections.create', compact('election','btnText', 'poll', 'groups', 'bread_arr'));
    }

    public function store(ElectionRequest $electionRequest){

        $poll = Poll::where('id',$electionRequest->input('poll_id'))->first();

        //POLICIES
        $canCreateElection = auth()->user()->can('create', [Election::class] );
        $canCreateElectionAnymore = auth()->user()->can('updateAnymore', [Poll::class, $poll] );
        //END POLICIES
        if(! $canCreateElection ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite agregar elecciones.")]);
        }
        if(! $canCreateElectionAnymore ){
            return redirect()->route('polls.elections', $poll->id)->with('modalMessage',['Aviso', __("Ya no se puede modificar la información en el grupo de votaciones, porque ya empezó la elección.")]);
        }
        //END POLICIES

        //OBTENEMOS EL ID DEL GRUPO DE ELECTORES ELEGIDO
        $group_id = $electionRequest->input('group_id');

        //INTENTAMOS OBTENER EL MODELO DEL GRUPO DE ELECTORES
        try {
            $group = Group::where('id',$group_id)->first();
            //$electors_from_group = $group->electors->pluck('id');
        } catch(\Exception $e){
            return back()->with('message',['danger',
                __("Hubo un error obteniendo los electores del grupo de votación. 
                Por favor verifique que el grupo esté correctamente llenado, luego inténtelo de nuevo.")]);
        }

        //INTENTAMOS CREAR LA ELECCIÓN, AÑADIENDO EL ID DEL USUARIO QUE CREÓ
        try {
            $electionRequest->merge(['user_id' => auth()->user()->id ]);
            $election = Election::create($electionRequest->input());
        } catch(\Illuminate\Database\QueryException $e){
            return back()->with('message',['danger',
                __("Hubo un error agregando la elección,
                por favor verifique que está colocando los datos requeridos, luego inténtelo de nuevo.")]);
        }

        //AÑADIMOS LOS ELECTORES A LA TABLA PIVOTE DEL GRUPO DE VOTACIONES CON LOS ELECTORES
        try {
            //JOB ACTION
            $current_poll_electors = $poll->electors()->count();
            $jobActivityLog = UserActivityLog::create([
                'user_id' => auth()->user()->id,
                'action_id' => Action::ADDING_ELECTORS_TO_POLL,
                'description' => $current_poll_electors,
                'model_modified' => 'Election',
                'model_id' => $election->id,
            ]);
            AddElectorsToPoll::dispatch($group, $election, $jobActivityLog)->onQueue('high');;
            //$election->poll->electors()->syncWithoutDetaching($electors_from_group);
        } catch(\Illuminate\Database\QueryException $e){
            $election->delete();

            return back()->with('message',['danger',
                __("Hubo un error agregando la elección,
                por favor verifique que está colocando los datos requeridos.")]);

        }

        UserActivityLog::create([
            'user_id' => auth()->user()->id,
            'action_id' => Action::ELECTION_ADDED,
            'description' => 'El usuario '.auth()->user()->name.' añadió la elección.',
            'model_modified' => 'Election',
            'model_id' => $election->id,
        ]);

        return redirect()->route('elections.addQuestions', $election->id)->with('groupMessage',[$election->id,$jobActivityLog->id]);


    }


    public function syncElectorsToPoll(Group $group, Election $election, UserActivityLog $jobActivityLog){

        try {
            //JOB ACTION
            $jobActivityLog->update([
                'action_id'=> Action::SYNCING_ELECTORS_TO_POLL,
            ]);

            $electors_from_group = $group->electors->pluck('id');
            //INSERTING ELECTORS INTO PIVOT DATA WITH GROUPS
            $election->poll->electors()->syncWithoutDetaching($electors_from_group);

            //JOB ACTION
            $jobActivityLog->update([
                'action_id'=> Action::ELECTORS_ADDED_TO_POLL,
                'description'=> $group->electors()->count(),
            ]);
            return true;
        } catch(\Throwable $e){
            $message= "Ocurrió un error al sincronizar los electores. Por favor, intente creando la elección nuevamente. Error: ".$e->getMessage();
            //JOB ACTION
            $jobActivityLog->update([
                'action_id'=> Action::ERROR_ADDING_ELECTORS_TO_POLL,
                'description' => $message,
            ]);
            $election->delete();
            return false;
        }

    }

    public function electorsSyncStatusAjax(Request $request){

        //POLICIES
        $canListElectors = auth()->user()->can('viewAny', [Elector::class] );
        //END POLICIES

        if(! $canListElectors ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver la lista de electores.")]);
        }

        $user_activity_log = $request->has('user_activity_log') ? $request->input('user_activity_log'): null;

        $message = "";
        $action = "continue";

        if(!$user_activity_log){
            $message = "Ocurrió un error actualizando el estado del grupo de electores, por favor verifique en los electores de la votación si están subidos correctamente.";
            $action = "break";
            return response()->json([
                'message' => $message,
                'action' => $action,
            ]);
        }

        $user_activity_log = UserActivityLog::where('id',$user_activity_log)->first();
        $election = Election::where('id', $user_activity_log->model_id)->first();

        $group_count = $election->group->electors()->count();
        $poll_electors = $election->poll->electors()->count();

        if ($user_activity_log->action_id == Action::ADDING_ELECTORS_TO_POLL){
            $message.= "Los electores se están agregando a las votaciones, por favor no cierre esta ventana o la página hasta que terminen de cargar al sistema.";
        }else if($user_activity_log->action_id == Action::ELECTORS_ADDED_TO_POLL){
            $message.= "Se agregaron con éxito ".$group_count." electores a las votaciones. La página se actualizará en breve, por favor aún no cierre esta ventana.";
            $action = "break";
        }else if($user_activity_log->action_id == Action::ERROR_ADDING_ELECTORS_TO_POLL){
            $message = $user_activity_log->description;
            $action = "break";
        }else if($user_activity_log->action_id == Action::SYNCING_ELECTORS_TO_POLL){

            $sync_electors = $poll_electors - $user_activity_log->description;
            $message.= "Electores sincronizados: ".$sync_electors." de ".$group_count.".<br>";
            $message.= "Los electores se están sincronizando, por favor no cierre esta ventana o la página hasta que terminen de cargar al sistema.";
        }

        return response()->json([
            'message' => $message,
            'action' => $action,
        ]);
    }

    public function addQuestions($id){

        $election = Election::with([
            'questions.candidates.votes'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $poll = $election->poll;

        //POLICIES
        $canAddQuestions = auth()->user()->can('addQuestions', [Election::class] );
        $canAddQuestionsAnymore = auth()->user()->can('updateAnymore', [Poll::class, $poll] );
        //END POLICIES

        if(! $canAddQuestions ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite agregar preguntas a esta elección.")]);
        }

        if(! $canAddQuestionsAnymore ){
            return redirect()->route('polls.elections', $poll->id)->with('modalMessage',['Aviso', __("Ya no se puede modificar la información en el grupo de votaciones, porque ya empezó la elección.")]);
        }

        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $poll->name;
        $bread_2->route = route('polls.info',$poll->id);
        $bread_arr[]=$bread_2;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = "Lista de votaciones";
        $bread_2->route = route('polls.elections',$poll->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Agregar Votación";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;
        //dd($companies);
        return view('elections.addQuestions', compact('election', 'bread_arr'));
    }
/*
    public function addElectors($id){

        $election = Election::with([
            'questions.candidates.votes'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //POLICIES
        $canAddElectorsAnymore = auth()->user()->can('addQuestions', [Election::class, $election->poll] );
        $canAddElectors = auth()->user()->can('addQuestionsAnymore', [Election::class] );
        //END POLICIES

        if(! $canAddElectors ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite agregar electores a esta elección.")]);
        }

        if(! $canAddElectorsAnymore ){
            return redirect('/')->with('modalMessage',['Aviso', __("Ya no se puede agregar electores a esta elección.")]);
        }

        //dd($companies);
        return view('elections.addElectors', compact('election'));
    }
*/
    public function sendNotificationMails($id){

        //POLICIES
        $canNotifyElectors = auth()->user()->can('sendEmails', [Election::class] );
        //END POLICIES

        if(! $canNotifyElectors ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite notificar a los electores de esta elección.")]);
        }


        //SENDING MAILS FOR EACH ELECTOR IN EECTION
        $election = Election::where('id',$id)->first();
        $electors = $election->electors;
        foreach ($electors as $electors_mail){

            /*
            $attempts = 0;
            $sent_all = true;
            do {
                try{
                    \Mail::to($electors_mail)->send(new ElectionNotificationMail($electors_mail, $election));
                    $sent_all = true;
                } catch (\Exception $e) {
                    $attempts++;
                    sleep(1+1*$attempts);
                    $sent_all = false;
                    continue;
                }
                break;
            } while($attempts < 5);
            */
            \Mail::to($electors_mail)->send(new ElectionNotificationMail($electors_mail, $election));
        }


        $election->update(array('state' => Poll::MAILS_SENDED));
        //dd($companies);
        return redirect()->route('elections.info', $election->id)->with('modalMessage',['Aviso', __("Se enviaron los correos exitosamente.")]);
    }

    public function sendRecordatoryMails($id){

        //POLICIES
        $canNotifyElectors = auth()->user()->can('sendEmails', [Poll::class] );
        //END POLICIES

        if(! $canNotifyElectors ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite notificar a los electores de esta elección.")]);
        }

        //SENDING MAILS FOR EACH ELECTOR IN EECTION
        $election = Election::where('id',$id)->first();
        $group_id = $election->group->id;
        $poll = $election->poll;
        $electors = $poll
            ->electors()
            ->where('state', Poll::NOT_VOTED)->get();


        $electorsNotVoted = Elector::with(['groups'])
            ->whereHas("polls", function ($q) use ($id) {
                $q
                    ->whereHas("elections", function ($q) use ($id) {
                        $q->where("id", $id);
                    })
                    ->where("elector_poll.state", Poll::NOT_VOTED);
            })
            ->whereHas("groups", function ($q) use ($group_id) {
                $q->where("id", $group_id);
            })
            ->orderBy('id', 'desc')
            ->get();

        //dd($electorsNotVoted);

        foreach ($electorsNotVoted as $electors_mail){
            $action = Action::EMAIL_RECORDATORY_BULK;
            $attempt = 0;
            SendRecordatoryEmail::dispatch($electors_mail, $poll, $action, $attempt);
        }

        //$poll->update(array('state' => Poll::MAILS_RE_SENDED));
        //dd($companies);
        return redirect()->route('elections.info', $id)->with('modalMessage',['Aviso', __("Se enviaron los correos exitosamente.")]);
    }

    public function sendRecordatoryMail($election_id, $elector_id ){

        //POLICIES
        $canNotifyElectors = auth()->user()->can('sendEmails', [Election::class] );
        //END POLICIES

        if(! $canNotifyElectors ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite notificar a los electores de esta elección.")]);
        }

        //SENDING MAILS FOR EACH ELECTOR IN EECTION
        $election = Election::where('id',$election_id)->first();
        $electors = $election->electors()->where('elector_id', $elector_id)->get();

        foreach ($electors as $electors_mail){
            \Mail::to($electors_mail)->send(new ElectionRecordatoryMail($electors_mail, $election->poll));
        }


        $election->update(array('state' => Poll::MAILS_SENDED));
        //dd($companies);
        return back()->with('modalMessage',['Aviso', __("Se envió el correo exitosamente.")]);
    }


}
