<?php

namespace App\Http\Controllers;

use App\Action;
use App\Election;
use App\Elector;
use App\EmailActivityLog;
use App\Http\Requests\PollRequest;
use App\Jobs\SendNotificationEmail;
use App\Jobs\SendRecordatoryEmail;
use App\Mail\ElectionNotificationMail;
use App\Mail\ElectionRecordatoryMail;
use App\Poll;
use App\UserActivityLog;
use App\Vote;
use Aws\Exception\AwsException;
use Aws\Sdk;
use Aws\Sns\SnsClient;
use Illuminate\Http\Request;
use Psr\Log\NullLogger;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Str;
use AWS;

class PollController extends Controller
{
    public function list(){

        //POLICIES
        $canListPolls = auth()->user()->can('view', [Poll::class] );
        $canListAllPolls = auth()->user()->can('viewAny', [Poll::class] );
        //END POLICIES

        if(! ($canListPolls || $canListAllPolls) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite listar los grupos de elecciones.")]);
        }

        $polls = Poll::
        orderBy('end_date','desc')
            ->paginate(25);

        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Lista";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;

        //dd($elections);
        return view('polls.list', compact('polls', 'bread_arr') );
    }
    public function info($id){

        //POLICIES
        $canListThisPoll = auth()->user()->can('view', [Poll::class] );
        //END POLICIES

        if(! $canListThisPoll ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver este grupo de votaciones.")]);
        }

        $poll = Poll::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //$poll = $poll->elections()->electors()->paginate(25);
        //dd($electors);

        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Detalle de grupo de votaciones";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;

        return view('polls.info', compact('poll', 'bread_arr'));
    }
    public function elections($id){

        //POLICIES
        $canListElectionsFromPoll = auth()->user()->can('viewElections', [Poll::class] );
        $canListAllElections = auth()->user()->can('viewAny', [Election::class] );
        //END POLICIES

        if(! ($canListElectionsFromPoll || $canListAllElections) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite listar las elecciones.")]);
        }

        $elections = Election::with([
            'group'
        ])
            ->where("poll_id", $id)
            ->orderBy('id', 'desc')
            ->paginate(25);

        $poll = Poll::where('id',$id)->first();


        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $poll->name;
        $bread_2->route = route('polls.info',$poll->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Lista de Votaciones";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;

        return view('polls.elections', compact('elections', 'poll', 'bread_arr') );
    }
    public function electionsAjax($poll_id){

        //POLICIES
        $canListElectionsFromPoll = auth()->user()->can('viewElections', [Poll::class] );
        $canListAllElections = auth()->user()->can('viewAny', [Election::class] );
        //END POLICIES

        if(! ($canListElectionsFromPoll || $canListAllElections) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite listar las elecciones.")]);
        }

        $elections = Election::with([
            'group'
        ])
            ->where("poll_id", $poll_id)
            ->orderBy('id', 'desc')
            ->paginate(25);

        $poll = Poll::with([])
            ->where('id',$poll_id)
            ->orderBy('id', 'desc')
            ->first();

        $poll_id = $poll->id;

        $electorsPollCount = Elector::with([])
            ->whereHas("polls", function ($q) use ($poll_id) {
                $q->where("id", $poll_id);
            })
            ->orderBy('id', 'desc')
            ->count();

        $votesPollCount = Elector::with([])
            ->whereHas("polls", function ($q) use ($poll_id) {
                $q->where("id", $poll_id)->where("elector_poll.state", Poll::VOTED);
            })
            ->orderBy('id', 'desc')
            ->count();


        return view('partials.polls.elections', compact('elections', 'poll' , 'electorsPollCount', 'votesPollCount') );
    }

    public function pollElectors($id){

        //POLICIES
        $canListThisElectionElectors = auth()->user()->can('viewElectors', [Poll::class] );
        //END POLICIES

        if(! $canListThisElectionElectors ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver la lista de electores.")]);
        }

        $poll = Poll::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $electors = $poll->electors()
            ->orderBy('id', 'desc')
            ->paginate(25);

        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $poll->name;
        $bread_2->route = route('polls.info',$poll->id);
        $bread_arr[]=$bread_2;

        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Lista de Electores";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;

        return view('polls.pollElectors', compact('poll','electors', 'bread_arr'));
    }

    public function pollElectorsAjax(Request $request, $poll_id){

        //POLICIES
        $canListThisElectionElectors = auth()->user()->can('viewElectors', [Poll::class] );
        //END POLICIES

        if(! $canListThisElectionElectors ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver la lista de electores.")]);
        }

        $search_input = $request->has('search_input') ? $request->input('search_input'): null;

        //dd($search_input);
        $poll = Poll::with([])
            ->where('id',$poll_id)
            ->orderBy('id', 'desc')
            ->first();

        $electors = $poll->electors();

        $search_input_expanded = str_replace(" ", "%", $search_input);

        if($search_input){
            $words = explode(" ", $search_input);
            //dd($words);
            $electors = $electors
                ->where(function($query) use ($search_input, $search_input_expanded){
                    $query
                        ->where('fathers_last_name','LIKE', '%'.$search_input.'%')
                        ->orWhere('mothers_last_name','LIKE', '%'.$search_input.'%')
                        ->orWhereRaw("concat(name, ' ', fathers_last_name, ' ', mothers_last_name) like '%$search_input_expanded%' ")
                        ->orWhere('name','LIKE', '%'.$search_input.'%')
                        ->orWhere('document_number', $search_input)
                        ->orWhere('email', $search_input);
                });
        }

        $electors = $electors
            ->orderBy('id', 'desc')
            ->paginate(25);


        return view('partials.polls.electors', compact('electors', 'poll'));
    }

    public function pollVotes($id){

        //POLICIES
        $canListThisPollVotes = auth()->user()->can('viewVotes', [Poll::class] );
        //END POLICIES

        if(! $canListThisPollVotes ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver los votos de este grupo de votación.")]);
        }

        $votes =  Vote::with([
            'candidate.question.election'
        ])
            ->whereHas("candidate", function ($q) use ($id) {
                $q->whereHas("question", function ($q) use ($id) {
                    $q->whereHas("election", function ($q) use ($id) {
                        $q->where("poll_id", $id);
                    });
                });
            })
            ->orderBy('id', 'desc')
            ->paginate(25);

        $poll = Poll::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //dd($votes);
        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $poll->name;
        $bread_2->route = route('polls.info',$poll->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Detalle de votos";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;

        return view('polls.pollVotes', compact('votes', 'poll', 'bread_arr'));
    }
    public function pollVotesAjax($id){

        //POLICIES
        $canListThisPollVotes = auth()->user()->can('viewVotes', [Poll::class] );
        //END POLICIES

        if(! $canListThisPollVotes ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver los votos de este grupo de votación.")]);
        }

        $votes =  Vote::with([
            'candidate.question.election'
        ])
            ->whereHas("candidate", function ($q) use ($id) {
                $q->whereHas("question", function ($q) use ($id) {
                    $q->whereHas("election", function ($q) use ($id) {
                        $q->where("poll_id", $id);
                    });
                });
            })
            ->orderBy('id', 'desc')
            ->paginate(25);

        $poll = Poll::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        return view('partials.polls.votes', compact('votes', 'poll'));
    }


    public function sendNotificationMails($id){
        //POLICIES
        $canNotifyElectors = auth()->user()->can('sendEmails', [Poll::class] );
        //END POLICIES

        if(! $canNotifyElectors ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite notificar a los electores de esta elección.")]);
        }

        //SENDING MAILS FOR EACH ELECTOR IN EECTION
        $poll = Poll::where('id',$id)->first();
        $electors = $poll->electors;
        foreach ($electors as $electors_mail){
            $token_mail = Str::random(22);
            $electors_mail->update(['token_mail'=> $token_mail]);
            $action = Action::EMAIL_NOTIFICATION_BULK;
            $attempt = 0;
            SendNotificationEmail::dispatch($electors_mail, $poll, $action, $attempt);
        }
        $poll->update(array('state' => Poll::MAILS_SENDED));
        //dd($companies);
        return redirect()->route('polls.info', $poll->id)->with('modalMessage',['Aviso', __("Los correos se estan procesarando exitosamente.")]);
    }

    public function sendNotificationMailsAjax($id){
        //POLICIES
        $canNotifyElectors = auth()->user()->can('sendEmails', [Poll::class] );
        //END POLICIES

        if(! $canNotifyElectors ){
            //FORBIDDEN
            return response()->json([
                'code' => 401,
                'message' => "No puede enviar mensajes",
            ]);
        }

        try{
            $poll = Poll::where('id',$id)->first();
            $electors = $poll->electors;
        } catch (\Throwable $e) {
            //FORBIDDEN
            return response()->json([
                'code' => 400,
                'message' => "Hubo un error obteniendo los electores del grupo de votaciones. Por favor, verifique que hay electores, o vuelva a cargar la página y envíe de nuevo los correos.",
            ]);
        }


        //SENDING MAILS FOR EACH ELECTOR IN POLL
        foreach ($electors as $electors_mail){
            //TOKENS CREATED
            $token_mail = Str::random(22);
            $token_cellphone = mt_rand(100000,999999);
            $action = Action::EMAIL_NOTIFICATION_BULK;
            $attempt = 0;

            $attempts_dispatching = 0;
            //DISPATCH JOB WITH RETRYS
            do {
                try{
                    $electors_mail->update([
                        'token_mail'=> $token_mail,
                        'token_cellphone'=> $token_cellphone,
                    ]);
                    SendNotificationEmail::dispatch($electors_mail, $poll, $action, $attempt);
                } catch (\Throwable $e) {
                    $attempts_dispatching++;
                    sleep(0.1+0.1*$attempts_dispatching);
                    continue;
                }
                break;
            } while($attempts_dispatching < 5);

        }

        $attempts_updating = 0;
        $response_code = 500;
        $response_message = "Se enviaron los correos, sin embargo, no se pudo actualizar el estado del grupo de votaciones";
        //DISPATCH JOB WITH RETRYS
        do {
            try{
                $poll->update(array('state' => Poll::MAILS_SENDED));
                $response_code = 200;
                $response_message = 'Se procesaron los correos correctamente. Puede observar el estado de envío de cada correo en la pestaña "Detalle de correos".';
            } catch (\Throwable $e) {
                $attempts_updating++;
                sleep(0.1+0.1*$attempts_updating);
                continue;
            }
            break;
        } while($attempts_updating < 5);


        //TO VIEW POLLS.INFO
        return response()->json([
            'code' => $response_code,
            'message' => $response_message,
        ]);

    }
    public function sendRecordatoryMailsAjax($id){
        //POLICIES
        $canNotifyElectors = auth()->user()->can('sendEmails', [Poll::class] );
        //END POLICIES

        if(! $canNotifyElectors ){
            //FORBIDDEN
            return response()->json([
                'code' => 401,
                'message' => "No puede enviar mensajes",
            ]);
        }

        try{
            $poll = Poll::where('id',$id)->first();
            $electors = $poll->electors()->where('state', Poll::NOT_VOTED)->get();;
        } catch (\Throwable $e) {
            //FORBIDDEN
            return response()->json([
                'code' => 400,
                'message' => "Hubo un error obteniendo los electores del grupo de votaciones. Por favor, verifique que hay electores, o vuelva a cargar la página y envíe de nuevo los correos.",
            ]);
        }


        //SENDING MAILS FOR EACH ELECTOR IN POLL
        foreach ($electors as $electors_mail){
            $action = Action::EMAIL_RECORDATORY_BULK;
            $attempt = 0;

            $attempts_dispatching = 0;
            //DISPATCH JOB WITH RETRYS
            do {
                try{
                    SendRecordatoryEmail::dispatch($electors_mail, $poll, $action, $attempt);
                } catch (\Throwable $e) {
                    $attempts_dispatching++;
                    sleep(0.1+0.1*$attempts_dispatching);
                    continue;
                }
                break;
            } while($attempts_dispatching < 5);

        }

        $attempts_updating = 0;
        $response_code = 500;
        $response_message = "Se enviaron los correos, sin embargo, no se pudo actualizar el estado del grupo de votaciones";
        //DISPATCH JOB WITH RETRYS
        do {
            try{
                $poll->update(array('state' => Poll::MAILS_RE_SENDED));
                $response_code = 200;
                $response_message = 'Se procesaron los correos correctamente. Puede observar el estado de envío de cada correo en la pestaña "Detalle de correos".';
            } catch (\Throwable $e) {
                $attempts_updating++;
                sleep(0.1+0.1*$attempts_updating);
                continue;
            }
            break;
        } while($attempts_updating < 5);


        //TO VIEW POLLS.INFO
        return response()->json([
            'code' => $response_code,
            'message' => $response_message,
        ]);

    }

    public function sendRecordatoryMails($id){

        //POLICIES
        $canNotifyElectors = auth()->user()->can('sendEmails', [Poll::class] );
        //END POLICIES

        if(! $canNotifyElectors ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite notificar a los electores de esta elección.")]);
        }

        //SENDING MAILS FOR EACH ELECTOR IN EECTION
        $poll = Poll::where('id',$id)->first();
        $electors = $poll->electors()->where('state', Poll::NOT_VOTED)->get();;

        //dd($electors);

        foreach ($electors as $electors_mail){
            $action = Action::EMAIL_RECORDATORY_BULK;
            $attempt = 0;
            SendRecordatoryEmail::dispatch($electors_mail, $poll, $action, $attempt);
        }


        $poll->update(array('state' => Poll::MAILS_RE_SENDED));
        //dd($companies);
        return redirect()->route('polls.info', $poll->id)->with('modalMessage',['Aviso', __("Los correos se estan procesarando exitosamente.")]);
    }

    public function sendRecordatoryMail($poll_id, $elector_id ){

        //POLICIES
        $canNotifyElectors = auth()->user()->can('sendEmails', [Poll::class] );
        //END POLICIES

        if(! $canNotifyElectors ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite notificar a los electores de esta elección.")]);
        }

        //SENDING MAILS FOR EACH ELECTOR IN EECTION
        $poll = Poll::where('id',$poll_id)->first();
        $electors = $poll->electors()->where('elector_id', $elector_id)->get();

        foreach ($electors as $electors_mail){

            $message = "Se procesó el correo exitosamente.";

            $action = Action::EMAIL_RECORDATORY;
            $attempt = 0;
            //\Mail::to($electors_mail)->send(new ElectionRecordatoryMail($electors_mail,$poll));
            SendRecordatoryEmail::dispatch($electors_mail, $poll, $action, $attempt);

        }

        return back()->with('modalMessage',['Aviso', $message]);
    }

    public function filter(Request $request, Poll $polls){

        //POLICIES
        $canListPolls = auth()->user()->can('view', [Poll::class] );
        $canListAllPolls = auth()->user()->can('viewAny', [Poll::class] );
        //END POLICIES

        if(! ($canListPolls || $canListAllPolls) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite listar los grupos de votaciones.")]);
        }

        //$company = $company->newQuery();
        $polls = $polls::with([]);

        $name = $request->has('name') ? $request->input('name'): null;
        $document_date = $request->has('document_date_filter') ? $request->input('document_date_filter'): null;


        // Search for a user based on their name.
        if ($document_date) {
            $polls->whereDate('created_at', $document_date);
        }

        // Search for a user based on their name.
        if ($name) {
            $polls->where('name','LIKE', '%'.$name.'%');
        }

        $polls = $polls->orderBy('id', 'desc')->paginate(25);
        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Filtro de búsqueda";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;
        //dd($movements);
        return view('polls.list', compact('polls', 'bread_arr') );

    }

    public function electionFilter(Request $request, Election $elections){

        //POLICIES
        $canListElectionsFromPoll = auth()->user()->can('viewElections', [Poll::class] );
        $canListAllElections = auth()->user()->can('viewAny', [Election::class] );
        //END POLICIES

        if(! ($canListElectionsFromPoll || $canListAllElections) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite listar las elecciones.")]);
        }

        $name = $request->has('name') ? $request->input('name'): null;
        $poll_id = $request->has('name') ? $request->input('poll_id'): null;

        $elections = $elections->newQuery();
        //$proformas = $proformas::with([]);

        if (!$poll_id) {
            return back()->with('message',['danger',
                __("Hubo un error. Por favor, busque de nuevo.")]);
        }
        if ($name) {
            $elections = $elections->where("name",'like','%'.$name.'%');
        }

        $elections = $elections->with([
            'group'
        ])
            ->where("poll_id", $poll_id)
            ->orderBy('id', 'desc')
            ->paginate(25);

        $poll = Poll::where('id',$poll_id)->first();
        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $poll->name;
        $bread_2->route = route('polls.info',$poll->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Filtro de votaciones";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;
        return view('polls.elections', compact('elections', 'poll', 'bread_arr') );
    }

    public function results($id){

        //POLICIES
        $canListThisElectionResults = auth()->user()->can('viewResults', [Poll::class] );
        //END POLICIES

        if(! $canListThisElectionResults ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver estos resultados.")]);
        }

        $elections = Election::with([
            'questions.candidates.votes'
        ])
            ->where('poll_id',$id)
            ->orderBy('id', 'desc')
            ->get();

        $poll = Poll::where('id',$id)->first();
        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $poll->name;
        $bread_2->route = route('polls.info',$poll->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Resultados en grupo de votaciones";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;
        //dd($companies);

        $total_electores = $poll->electors()->count();
        $votesCount = \App\Elector::
            whereHas("polls", function ($q) use ($id) {
                $q
                    ->where("id", $id)
                    ->where("elector_poll.state", \App\Poll::VOTED);
            })
            ->count();

        return view('polls.results', compact('elections', 'poll', 'total_electores', 'votesCount', 'bread_arr'));
    }

    public function publicResults($slug){

        $poll = Poll::with([
            /*
            'elections.questions.candidates.votes' => function ($q){
                $q->select(array('candidate_id',\DB::raw("SUM(weight) as votes_count")))->groupBy('candidate_id');
            },*/
            'elections' => function ($q){
                $q
                    ->with([
                        'questions.candidates.votes' => function ($q){
                            $q->select(array('candidate_id',\DB::raw("SUM(weight) as votes_count")))->groupBy('candidate_id');
                        },
                    ])
                    ->orderBy('id', 'desc');
            },

        ])
            ->where('slug',$slug)
            ->orderBy('id', 'desc')
            ->first();

        $poll_id = $poll->id;

        $electorsPollCount = Elector::with([])
            ->whereHas("polls", function ($q) use ($poll_id) {
                $q->where("id", $poll_id);
            })
            ->orderBy('id', 'desc')
            ->count();

        $votesPollCount = Elector::with([])
            ->whereHas("polls", function ($q) use ($poll_id) {
                $q->where("id", $poll_id)->where("elector_poll.state", Poll::VOTED);
            })
            ->orderBy('id', 'desc')
            ->count();


        $poll_json = $poll->toJson();
        $json_string = json_encode($poll_json, JSON_PRETTY_PRINT);

        //dd($poll->elections[0]->questions[0]->candidates[0]->votes[0]->votes_count);



        return view('polls.publicResults', compact('poll', 'electorsPollCount', 'votesPollCount'));
    }

    public function resultsPDF($id){

        //POLICIES
        $canListThisPollResults = auth()->user()->can('viewResults', [Poll::class] );
        //END POLICIES

        if(! $canListThisPollResults ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver estos resultados.")]);
        }


        $poll = Poll::with(['elections.questions.candidates.votes'])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $elections = $poll->elections;


        $total_electores = $poll->electors()->count();
        $votesCount = \App\Elector::
        whereHas("polls", function ($q) use ($id) {
            $q
                ->where("id", $id)
                ->where("elector_poll.state", \App\Poll::VOTED);
        })
            ->count();



        //return view('pdfs.resultsPollPdf', compact('poll', 'elections', 'total_electores', 'votesCount'));
        try{
            $pdf = PDF::loadView('pdfs.resultsPollPdf', compact('poll', 'elections', 'total_electores', 'votesCount'));
        }catch (\Throwable $t){
            $pdf = PDF::loadView('pdfs.resultsPollPDF', compact('poll', 'elections', 'total_electores', 'votesCount'));
        }



        UserActivityLog::create([
            'user_id' => auth()->user()->id,
            'action_id' => Action::RESULTS_ELECTION_DOWNLOADED,
            'description' => 'El usuario '.auth()->user()->name.' descargó el resultado de la elección.',
            'model_modified' => 'Poll',
            'model_id' => $id,
        ]);

        // download PDF file with download method
        return $pdf->download('resultados-de-votacion.pdf');

    }

    public function pollElectorsPDF($id){

        //POLICIES
        $canListThisPollResults = auth()->user()->can('viewResults', [Poll::class] );
        //END POLICIES

        if(! $canListThisPollResults ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver estos resultados.")]);
        }


        $poll = Poll::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $total_electores = $poll->electors()->count();
        $votesCount = \App\Elector::
        whereHas("polls", function ($q) use ($id) {
            $q
                ->where("id", $id)
                ->where("elector_poll.state", \App\Poll::VOTED);
        })
            ->count();


        $electors = $poll->electors()
            ->orderBy('elector_poll.state', 'desc')
            ->orderBy('name', 'asc')
            ->get();
        $pdf = PDF::loadView('pdfs.pollElectors', compact('poll', 'electors', 'total_electores', 'votesCount'));


        UserActivityLog::create([
            'user_id' => auth()->user()->id,
            'action_id' => Action::ELECTORS_PDF_DOWNLOADED,
            'description' => 'El usuario '.auth()->user()->name.' descargó la lista de electores en PDF.',
            'model_modified' => 'Poll',
            'model_id' => $id,
        ]);

        // download PDF file with download method
        return $pdf->download('estado-de-electores.pdf');

    }


    public function create(){
        $poll = new Poll();

        //POLICIES
        $canCreatePolls = auth()->user()->can('create', [Poll::class] ); true;
        //END POLICIES

        if(! $canCreatePolls){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite crear grupos de votaciones.")]);
        }

        $btnText = __("Crear grupo de votación");



        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de votaciones";
        $bread_1->route = route('polls.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Agregar";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;


        return view('polls.create', compact('poll','btnText', 'bread_arr'));
    }

    public function store(PollRequest $pollRequest){

        //POLICIES
        $canCreatePolls = auth()->user()->can('create', [Poll::class] ); true;
        //END POLICIES

        if(! $canCreatePolls){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite crear grupos de votaciones.")]);
        }

        try {

            $poll_pre_slug = $pollRequest->input('name')." ".Str::random(6);
            $poll_slug = Str::slug($poll_pre_slug);
            $pollRequest->merge([
                'slug' => $poll_slug,
                'state' => Poll::CREATING,
                'user_id' => auth()->user()->id
            ]);
            $poll = Poll::create($pollRequest->input());

            UserActivityLog::create([
                'user_id' => auth()->user()->id,
                'action_id' => Action::POLL_ADDED,
                'description' => 'El usuario '.auth()->user()->name.' añadió el grupo de votaciones.',
                'model_modified' => 'Poll',
                'model_id' => $poll->id,
            ]);

            return redirect()->route('polls.info', $poll->id);
        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);
            return back()->with('message',['danger',
                __("Hubo un error agregando el grupo de votación,
                por favor verifique que está colocando los datos requeridos.")]);

        }
    }


    public function sendNotificationSMS($phone, $code){

        $SnSclient = AWS::createClient('sns');
        $message = 'Tu código para votar es: '.$code;

        try {
            /** Few setting that you should not forget */
            $result = $SnSclient->publish([
                'MessageAttributes' => array(
                    'AWS.SNS.SMS.SenderID' => array(
                        'DataType' => 'String',
                        'StringValue' => 'Votaciones'
                    ),
                    'AWS.SNS.SMS.SMSType' => array(
                        'DataType' => 'String',
                        'StringValue' => 'Transactional'
                    )
                ),
                'Message' => $message,
                'PhoneNumber' => $phone,
            ]);
            /** Dump the output for debugging */
            dd($result);
        } catch (AwsException $e) {
            // output error message if fails
            dd($e->getMessage());
        }

    }
}
