<?php

namespace App\Http\Controllers;

use App\Elector;
use App\Poll;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

use Browser;

class TestController extends Controller
{
    public function browser()
    {
        /*
        #Fetch all the failed jobs
        $jobs = \DB::table('jobs')->select()->orderBy('id','DESC')->get();
        #Loop through all the failed jobs and format them for json printing
        foreach ($jobs as $job) {
            $jsonpayload = json_decode($job->payload);
            $job->payload = $jsonpayload;
            $jsonpayload->data->command = unserialize($jsonpayload->data->command);
            dd($job->payload);
        }
        return response()->json($jobs);
        */

        $browser = Browser::browserFamily();
        $browser = Browser::isChrome();
        $browser = Browser::detect();

        $mac = substr(exec('getmac'), 0, 17);
        $ip = self::getIp();
        return view('tests.browser', compact('mac', 'ip'));

    }
    public function reniec($dni = null)
    {
        //REQUEST DATA
        $dni = "18005852";
        //$dni = "70681083""18005852";
        $get_url = 'https://apiperu.dev/api/dni/'.$dni;
        $token = "eca4c5903408636a68a6b1194cb9901197814511033dbb8309006a50598d6886";
        //REQUEST
        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$token,
        ])->get($get_url);
        //RESPONSE IN JSON
        $response_json = $response->json();
        //RESPONSE STATUS
        $response_code = $response_json["success"];
        //RESPONSE DATA
        $response_data = $response_json["data"];
        $verification_code = $response_data["codigo_verificacion"];

        $birthday = $response_data["fecha_nacimiento"];
        if($birthday){
            $birthday_formatted = Carbon::parse($birthday);
            $birthday_day = $birthday_formatted->day;
            $birthday_month = $birthday_formatted->month;
            $birthday_year = $birthday_formatted->year;
        }

        dd($verification_code);

        return view('tests.reniec', compact('mac', 'ip'));
    }
    public function pollElectorsPDF($id){

        $poll = Poll::with(['electors'])
            ->where('id',$id)
            ->orderBy('id', 'asc')
            ->first();

        $electors = $poll->electors()
            ->orderBy('elector_poll.state', 'desc')
            ->orderBy('name', 'asc')
            ->paginate(25);

        $total_electores = $poll->electors()->count();
        $votesCount = \App\Elector::
        whereHas("polls", function ($q) use ($id) {
            $q
                ->where("id", $id)
                ->where("elector_poll.state", \App\Poll::VOTED);
        })
            ->count();


        // download PDF file with download method
        return view('tests.pollElectorsPDF', compact('electors', 'poll', 'total_electores', 'votesCount'));

    }
    public function results(){

        $poll_id = 6;

        $poll = Poll::with([
            /*
            'elections.questions.candidates.votes' => function ($q){
                $q->select(array('candidate_id',\DB::raw("SUM(weight) as votes_count")))->groupBy('candidate_id');
            },*/
            'elections' => function ($q){
                $q
                    ->with([
                        'questions.candidates.votes' => function ($q){
                            $q->select(array('candidate_id',\DB::raw("SUM(weight) as votes_count")))->groupBy('candidate_id');
                        },
                    ])
                    ->orderBy('id', 'desc');
            },

        ])
            ->where('id',$poll_id)
            ->orderBy('id', 'desc')
            ->first();

        $poll_id = $poll->id;

        $electorsPollCount = Elector::with([])
            ->whereHas("polls", function ($q) use ($poll_id) {
                $q->where("id", $poll_id);
            })
            ->orderBy('id', 'desc')
            ->count();

        $votesPollCount = Elector::with([])
            ->whereHas("polls", function ($q) use ($poll_id) {
                $q->where("id", $poll_id)->where("elector_poll.state", Poll::VOTED);
            })
            ->orderBy('id', 'desc')
            ->count();


        $poll_json = $poll->toJson();
        $json_string = json_encode($poll_json, JSON_PRETTY_PRINT);

        //dd($poll->elections[0]->questions[0]->candidates[0]->votes[0]->votes_count);



        return view('tests.results', compact('poll', 'electorsPollCount', 'votesPollCount'));
    }
}
