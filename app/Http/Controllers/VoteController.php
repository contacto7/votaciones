<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Election;
use App\Elector;
use App\Http\Requests\VoteRequest;
use App\Poll;
use App\Question;
use App\Vote;
use Aws\Exception\AwsException;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Psr\Log\NullLogger;
use AWS;

class VoteController extends Controller
{

    public function insertDni($poll_id_elector_id_token_mail ){
        $sep = \App\Vote::DATA_SEPARATOR;
        $poll_id_elector_id_token_mail_decrypted = \Crypt::decryptString($poll_id_elector_id_token_mail);
        $auth_factor = explode($sep, $poll_id_elector_id_token_mail_decrypted)[3];

        $error_validating = \Session::get('error_validating');
        if ($error_validating){
            $new_try_at = $error_validating['new_try_at'];
            $secondsToVoteAgain = now()->diffInSeconds($new_try_at, false);
        }else{
            $secondsToVoteAgain = 0;
        }

        return view('votes.insertDni', compact('poll_id_elector_id_token_mail','auth_factor', 'secondsToVoteAgain'));
        //dd($electors);
    }

    public function checkDni(Request $request, $poll_id_elector_id_token_mail){
        $sep = \App\Vote::DATA_SEPARATOR;
        $poll_id_elector_id_token_mail_decrypted = \Crypt::decryptString($poll_id_elector_id_token_mail);

        $poll_id_decrypted = explode($sep, $poll_id_elector_id_token_mail_decrypted)[0];
        $elector_id_decrypted= explode($sep, $poll_id_elector_id_token_mail_decrypted)[1];
        $token_mail_decrypted= explode($sep, $poll_id_elector_id_token_mail_decrypted)[2];
        $auth_factor = explode($sep, $poll_id_elector_id_token_mail_decrypted)[3];

        //DATABASE DATA OF ELECTOR
        $elector = Elector::where('id', $elector_id_decrypted)->first();
        $document_number_db = $elector->document_number;
        $token_mail_db = $elector->token_mail;

        //REQUEST DOCUMENT NUMBER
        $document_number = $request->input('document_number');

        //CHECK IF HAS NOT VOTED YET
        //TO: PREVENT VOTING MORE THAN ONCE
        $elector_elections = $elector->polls()->find($poll_id_decrypted);
        if( ($elector_elections->pivot->state == Poll::VOTED) && ($document_number_db == $document_number) ){
            return back()->with('modalMessage',['Aviso', __("Usted ya realizó la votación. No puede votar más de una vez.")]);
        }

        //TOKENS AND DOCUMENT NUMBER VALIDATION -> DATABASE DATA VS REQUEST DATA
        //TO: VALIDATE IDENTITY OF VOTE
        if ( $token_mail_decrypted != $token_mail_db || $document_number_db != $document_number || $token_mail_db == NULL){
            return back()->with('modalMessage',['Aviso', __("No se puede validar su identidad. Por favor ingrese nuevamente su documento de indentidad.")]);
        }

        $poll = Poll::where('id',$poll_id_decrypted)->first();

        //CHECK IF SMS HAS TO BE SENT
        //ONLY FOR POLLS WITH TWO FACTOR ENABLED
        if($poll->auth_factors == Poll::TWO_FACTOR){
            $sms_sending_response = $this->sendNotificationSMS($elector);

            $sms_status = $sms_sending_response["status"];
            $sms_response = $sms_sending_response["response"];

            //IF FAILS
            if ($sms_status != "200"){
                $message = "No se pudo enviar el SMS con su código de votación a su celular. Por favor intente de nuevo, o comuníquese con el administrador del sistema de votaciones.";
                $message.=("<br>Error description: ".$sms_response);
                return back()->with('modalMessage',['Aviso', $message]);
            }
        }

        //VALIDATE RENIEC DATA
        //ONLY FOR POLLS WITH RENIEC VALIDATION ENABLED
        if($poll->auth_factors == Poll::RENIEC_VALIDATION){
            $verification_digit_input = $request->input('verification_digit');
            $try_number=0;
            $validateVerificationCode = $this->validateVerificationCode($document_number, $verification_digit_input);
            //dd($validateVerificationCode["verify"]);
            $isVerified = $validateVerificationCode["verify"];
            if(!$isVerified){
                $error_validating = \Session::get('error_validating');
                if(!$error_validating){
                    $number_errors = 0;
                }else{
                    $number_errors = $error_validating['number_errors'];
                }
                $number_errors++;
                $penalization_minutes = 1;
                if ($number_errors == 1){
                    $penalization_minutes = 1;
                }elseif($number_errors == 2){
                    $penalization_minutes = 5;
                }else{
                    $penalization_minutes = 30;
                }
                $new_try_at = now()->addMinutes($penalization_minutes);
                \Session::put([
                    'error_validating'=>[
                        'number_errors'=> $number_errors,
                        'new_try_at'=> $new_try_at,
                    ]
                ]);
                return back()->with([
                    'modalMessage'=> ['Aviso', __("No se puede validar su identidad.")],
                ]);

            }
        }


        \Session::forget('elector');

        \Session::put([
            'elector'=>[
                'document_number'=> $document_number,
                'poll_id_elector_id_token_mail'=> $poll_id_elector_id_token_mail,
                'created_at'=> now(),
                'expires_at'=> now()->addMinutes(5),
            ]
        ]);

        return redirect()->route('votes.vote');

    }

    public function vote(){

        //GET ALL DATA FROM SESSION
        $elector_session = \Session::get('elector');
        $document_number = $elector_session['document_number'];
        $poll_id_elector_id_token_mail = $elector_session['poll_id_elector_id_token_mail'];
        $created_at = $elector_session['created_at'];
        $expires_at = $elector_session['expires_at'];

        //DECRYPTYING AND GETTING DATA
        $sep = \App\Vote::DATA_SEPARATOR;
        $poll_id_elector_id_token_mail_decrypted = \Crypt::decryptString($poll_id_elector_id_token_mail);
        $poll_id_decrypted = explode($sep, $poll_id_elector_id_token_mail_decrypted)[0];
        $elector_id_decrypted= explode($sep, $poll_id_elector_id_token_mail_decrypted)[1];
        $token_mail_decrypted= explode($sep, $poll_id_elector_id_token_mail_decrypted)[2];
        $auth_factor = explode($sep, $poll_id_elector_id_token_mail_decrypted)[3];

        //DATABASE DATA OF ELECTOR
        $elector = Elector::where('id', $elector_id_decrypted)->first();
        $document_number_db = $elector->document_number;
        $token_mail_db = $elector->token_mail;


        $elector = Elector::where('id', $elector_id_decrypted)->first();
        $elector_elections = $elector->polls()->find($poll_id_decrypted);

        //CHECK IF HAS ALREADY VOTED
        if($elector_elections->pivot->state == Poll::VOTED){
            \Session::forget('elector');
            $message = "Usted ya votó para esta elección.";
            return view('votes.hasVoted', compact('poll_id_decrypted', 'message'));
        }

        //TOKENS AND DOCUMENT NUMBER VALIDATION -> DATABASE DATA VS REQUEST DATA
        //TO: VALIDATE IDENTITY OF VOTE
        if ( $token_mail_decrypted != $token_mail_db || $document_number_db != $document_number || $token_mail_db == NULL){
            \Session::forget('elector');
            return redirect()->route('votes.insertDni', compact('poll_id','elector_id', 'token_mail'))
                ->with('modalMessage',['Aviso', __("No se puede validar su identidad. Por favor ingrese nuevamente su documento de indentidad.")]);
        }


        $poll = Poll::with([])
            ->where('id',$poll_id_decrypted)
            ->orderBy('id', 'desc')
            ->first();
        $elections = Election::with([])
            ->whereHas("group", function ($q) use ($elector_id_decrypted ) {
                $q->whereHas("electors", function ($q) use ($elector_id_decrypted ) {
                    $q->where("id", $elector_id_decrypted);
                });
            })
            ->where('poll_id',$poll_id_decrypted)
            ->orderBy('id', 'desc')
            ->get();

        //CHECK IF CAN VOTE STILL
        //TO: PREVENT VOTING OUT OF DATES
        $start_date = (new Carbon)->parse($poll->start_date);
        $end_date = (new Carbon)->parse($poll->end_date);

        $currentTime = Carbon::now();

        $electionOpen = $currentTime->between($start_date, $end_date);


        if ($electionOpen){
            $secondsToCloseSession = $expires_at->diffInSeconds(now());
            return view('votes.vote', compact('elections', 'poll','poll_id_elector_id_token_mail', 'document_number', 'secondsToCloseSession'));
        }else{
            \Session::forget('elector');
            return view('votes.voteNotOpen', compact('elections', 'poll','start_date', 'end_date'));
        }

        //dd($electors);
    }

    public function store(VoteRequest $voteRequest, $poll_id_elector_id_token_mail){

        //dd($voteRequest->input());
        //DECRYPTYING AND GETTING DATA
        $sep = \App\Vote::DATA_SEPARATOR;
        $poll_id_elector_id_token_mail_decrypted = \Crypt::decryptString($poll_id_elector_id_token_mail);
        $poll_id_decrypted = explode($sep, $poll_id_elector_id_token_mail_decrypted)[0];
        $elector_id_decrypted= explode($sep, $poll_id_elector_id_token_mail_decrypted)[1];
        $token_mail_decrypted= explode($sep, $poll_id_elector_id_token_mail_decrypted)[2];

        //DATABASE DATA OF ELECTOR
        $elector = Elector::where('id', $elector_id_decrypted)->first();
        $document_number_db = $elector->document_number;
        $token_mail_db = $elector->token_mail;
        $poll = Poll::where('id',$poll_id_decrypted)->first();

        //REQUEST DOCUMENT NUMBER
        $document_number = $voteRequest->input('document_number');
        //REQUEST UUID DEVICE
        $device_uuid = $voteRequest->input('device_uuid');
        //GET USER IP
        $ip_registered = self::getIp();


        //TOKENS AND DOCUMENT NUMBER VALIDATION -> DATABASE DATA VS REQUEST DATA
        //TO: VALIDATE IDENTITY OF VOTE
        if ( $token_mail_decrypted != $token_mail_db || $document_number_db != $document_number || $token_mail_db == NULL){
            \Session::forget('elector');
            $message = "No se puede validar su identidad.";
            return view('votes.hasVoted', compact('poll_id_decrypted', 'message'));
        }

        //VALIDATE CELLPHONE TOKEN
        //ONLY FOR POLLS WITH TWO FACTOR ENABLED
        if($poll->auth_factors == Poll::TWO_FACTOR){
            $token_cellphone_input = $voteRequest->input('token_cellphone');
            $token_cellphone_db = $elector->token_cellphone;

            if ( $token_cellphone_input != $token_cellphone_db){
                \Session::forget('elector');
                $message = "El código de votación no es válido. Verifique el código que llegó a su celular e ingrese de nuevo a través del link de votación.";
                return view('votes.hasVoted', compact('poll_id_decrypted', 'message'));
            }
        }

        //CHECK IF HAS NOT VOTED YET
        //TO: PREVENT VOTING MORE THAN ONCE
        $elector_elections = $elector->polls()->find($poll_id_decrypted);
        if($elector_elections->pivot->state == Poll::VOTED){
            \Session::forget('elector');
            $message = "Usted ya realizó la votación. No puede votar más de una vez.";
            return view('votes.hasVoted', compact('poll_id_decrypted', 'message'));
        }

        //GET ALL THE SELECTED CANDIDATES ON THE VOTE, STORED IN ARRAY QUESTIONS
        //$inputs = $voteRequest->except('_method', '_token', 'document_number', 'token_cellphone');
        $candidates_ids_of_vote = $voteRequest->input('question');
        //FIRST, CHECK IF THERE IS ANY CANDIDATE SELECTED
        if( !isset($candidates_ids_of_vote) ){
            return back()->with('modalMessage',['Aviso', __("Seleccione al menos una opción.")]);
        }

        //CHECK IF ALL QUESTIONS ARE ANSWERED, ONLY ONCE
        //TO: PREVENT VOTING FOR ONE CANDIDATE MORE THAN ONCE
        $votes_check = $this->checkVotesForThisElection($candidates_ids_of_vote, $elector_id_decrypted, $poll_id_decrypted);

        $answered_all = $votes_check["status"];
        $candidates_verified = $votes_check["candidates_verified"];

        if(!$answered_all){
            return back()->with('modalMessage',['Aviso', __("Tiene que votar para todas las preguntas.")]);
        }

        try {
            //ADD VOTES

            //dd($candidates_verified);
            foreach ($candidates_verified as $candidate_verified) {
                $candidate_id = $candidate_verified["candidate_id"];
                $weight = $candidate_verified["weight"];
                Vote::create([
                    'candidate_id' => $candidate_id,
                    'weight' => $weight,
                ]);
            };

            //UPDATE SATE OF ELECTION ELECTORS PIVOT TABLE
            $dt = new \DateTime();
            $voted_at = $dt->format('y-m-d H:00:00');
            $elector_elections->pivot->update([
                'state' => Poll::VOTED,
                'voted_at' => $voted_at,
                'device_uuid' => $device_uuid,
                'ip_registered' => $ip_registered,
            ]);
            //ANOTHER WAY TO UPDATE A PIVOT
            //$elector = Elector::where('id', $elector_id)->first();
            //$elector->elections()->updateExistingPivot($election_id, array('state' => Poll::VOTED, 'voted_at' => now()), false);

            //UPDATE TOKEN MAIL OF ELECTOR
            $elector->update([
                'token_mail'=>null,
                'token_cellphone'=>null,
            ]);

            \Session::forget('elector');
            $message = "Su voto se registró correctamente.";
            return view('votes.hasVoted', compact('poll_id_decrypted', 'message'));
        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);
            return back()->with('modalMessage',['Aviso', __("No se pudo agregar su voto. Por favor, inténtelo de nuevo. Error: ".$e->getCode())]);

        }
    }

    public function checkVotesForThisElection($candidates_ids_of_vote, $elector_id, $poll_id){

        //LIST ALL QUESTIONS TO VALIDATE USER ANSWERED ALL QUESTIONS
        //$questions = Question::where('election_id', $election_id)->get();

        $questions = Question::with([
            'election.group.electors' => function ($query) use ($elector_id){
                $query->where('id', $elector_id);

            }
        ])
            ->whereHas("election", function ($q) use ($elector_id , $poll_id) {
                $q
                    ->whereHas("group", function ($q) use ($elector_id , $poll_id) {
                        $q->whereHas("electors", function ($q) use ($elector_id , $poll_id) {
                            $q->where("id", $elector_id);
                        });
                    })
                    ->where('poll_id', $poll_id);
            })
            ->get();

        //INITIALIZE "ANSWER ALL" VALIDATION VARIABLE AS TRUE
        $answer_all = true;

        //ARRAY TO STORE ONLY THE CANDIDATES VERIFIED
        $candidates_verified = array();

        //CHECK QUESTIONS BY QUESTION IF IS SELECTED ONLY ONE CANDIDATE
        foreach ($questions as $question){
            $question_id = $question->id;
            $weight_from_elector_on_group = $question->election->group->electors->first()->pivot->weight;

            //GET ALL CANDIDATES IN A QUESTION
            $candidates_ids_of_question = Candidate::where('question_id', $question_id)->pluck('id')->toArray();

            //CHECK IF A CANDIDATE VOTED BELONGS TO CANDIDATES ON THIS QUESTION
            $intersection_of_ids=array_intersect($candidates_ids_of_question,$candidates_ids_of_vote);

            //GET NUMBER OF CANDIDATES VOTED ON THIS QUESTION
            $number_of_intersections = count($intersection_of_ids);

            //IF VOTED 0 CANDIDATES OR MORE THAN 1 IN THIS QUESTIONS, RETURN AN ERROR
            if ($number_of_intersections != 1){
                $answer_all = false;
            }else{
                //ADD VERIFIED CANDIDATES TO "VERIFIED ARRAY"
                //////GET ONLY THE VALUE FROM THE INTERSECTION
                $candidate_verified_id = array_values($intersection_of_ids)[0];

                //ADD CANDIDATE_ID WITH ITS ELECTOR WEIGHT FROM ELECTOR_GROUP
                //TO VERIFIED CANDIDATES
                $candidate_verified = [
                    "candidate_id" =>$candidate_verified_id,
                    "weight"=>$weight_from_elector_on_group
                ];

                array_push($candidates_verified, $candidate_verified);

            }

        }

        //dd($candidates_verified);

        $check = [
            "status" =>$answer_all,
            "candidates_verified"=>$candidates_verified
        ];

        return $check;


    }

    public function error($error_code){

        return view('votes.error', compact('error_code'));
        //dd($electors);
    }

    public function sendNotificationSMS($elector){

        $phone_code = $elector->country_code;
        $phone_number = $elector->cellphone;
        $phone = "+".$phone_code.$phone_number;
        //$phone = "+";

        $code = $elector->token_cellphone;

        $SnSclient = AWS::createClient('sns');
        $message = 'Tu código para votar es: '.$code;

        $status = "200";
        $response = "message sent";

        $attempts_messaging = 0;
        do {
            try{
                /** Few setting that you should not forget */
                $result = $SnSclient->publish([
                    'MessageAttributes' => array(
                        'AWS.SNS.SMS.SenderID' => array(
                            'DataType' => 'String',
                            'StringValue' => 'Votaciones'
                        ),
                        'AWS.SNS.SMS.SMSType' => array(
                            'DataType' => 'String',
                            'StringValue' => 'Transactional'
                        )
                    ),
                    'Message' => $message,
                    'PhoneNumber' => $phone,
                ]);
            } catch (\Throwable $e) {
                $attempts_messaging++;
                sleep(0.3+0.3*$attempts_messaging);

                // output error message if fails
                $status = $e->getAwsErrorCode();
                $response = $e->getAwsErrorMessage();

                continue;
            }
            break;
        } while($attempts_messaging < 4);


        return [
          "status" => $status,
          "response" =>  $response,
        ];


    }

    public function validateVerificationCode($dni, $verification_digit_input){
        //$dni = "70681083""18005852";
        $get_url = 'https://apiperu.dev/api/dni/'.$dni;
        $verify = true;
        //REQUEST

        $attempts_requesting = 0;
        //TRYING TO GET RESPONSE AND JSON WELL FORMED
        do {
            try{
                //GETTING RESPONSE
                $response = Http::withHeaders([
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.Vote::API_TOKEN,
                ])->get($get_url);
                //RESPONSE IN JSON
                $response_json = $response->json();
                //NULL RESPONSE -> NOT GETTING THE CORRECT RESPONSE
                if (!$response_json){
                    $message_response = "response-invalid";
                    $attempts_requesting++;
                    sleep(0.5*$attempts_requesting);
                    //IF ATTEMPTS MAX
                    //RETURNING FAILED VERIFICATION
                    if ($attempts_requesting == Vote::MAX_ATTEMPTS){
                        return [
                            "status" => "failed",
                            "verify" =>  $verify,
                            "message_response" =>  $message_response,
                        ];
                    }
                    //TRYING AGAIN TO GET WELL FORMED RESPONSE
                    continue;
                }

            } catch (\Throwable $e) {
                $message_response = "host-invalid";
                $attempts_requesting++;
                sleep(0.5*$attempts_requesting);
                if ($attempts_requesting == Vote::MAX_ATTEMPTS){
                    return [
                        "status" => "failed",
                        "verify" =>  $verify,
                        "message_response" =>  $message_response,
                    ];
                }
                //TRYING AGAIN TO GET RESPONSE FROM HOST
                continue;
            }
            break;
        } while($attempts_requesting < Vote::MAX_ATTEMPTS);

        //RESPONSE STATUS
        $response_success = $response_json["success"];

        //IF RESPONSE IS SUCCESSFUL
        //BUT NO DNI INFO GOT
        if($response_success == false){
            $message = $response_json["message"];
            if($message == "Unauthenticated"){
                $message_response = "token-invalid";
            }else{
                $message_response = "dni-invalid";
            }
            return [
                "status" => "failed",
                "verify" =>  $verify,
                "message_response" =>  $message_response,
            ];
        }else{
            //RESPONSE DATA
            $message_response = "dni-valid";
            $response_data = $response_json["data"];
            $verification_code = $response_data["codigo_verificacion"];
        }

        if($verification_code != $verification_digit_input){
            $verify = false;
        }
        return [
            "status" => "success",
            "verify" =>  $verify,
            "message_response" =>  $message_response,
        ];

    }
}
