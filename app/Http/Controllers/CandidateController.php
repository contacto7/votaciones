<?php

namespace App\Http\Controllers;

use App\Action;
use App\Candidate;
use App\Http\Requests\CandidateRequest;
use App\Poll;
use App\UserActivityLog;
use Illuminate\Http\Request;

class CandidateController extends Controller
{

    public function store(CandidateRequest $candidateRequest){
        $question_id = $candidateRequest->input('question_id');

        $poll = Poll::whereHas("elections", function ($q) use ($question_id) {
            $q->whereHas("questions", function ($q) use ($question_id) {
                $q->where("id", $question_id);
            });
        })->first();

        //POLICIES
        $canCreateCandidates = auth()->user()->can('create', [Candidate::class] );;
        $canCreateCandidatesAnymore = auth()->user()->can('updateAnymore', [Poll::class, $poll] );
        //END POLICIES

        if(! $canCreateCandidates){
            $code = 403;
            $message = "Tu rol no te permite crear candidatos.";
            return response()->json([
                'code' => $code,
                'message' => $message,
                'action' =>'crear pregunta'
            ]);
        }
        if(! $canCreateCandidatesAnymore){
            $code = 403;
            $message = "Ya no puedes crear candidatos, porque ya comenzó la elección.";
            return response()->json([
                'code' => $code,
                'message' => $message,
                'action' =>'crear pregunta'
            ]);
        }


        try {
            $candidate = Candidate::create($candidateRequest->input());
            $code = 200;
            $message = "Se creó el candidato.";

            UserActivityLog::create([
                'user_id' => auth()->user()->id,
                'action_id' => Action::CANDIDATE_ADDED,
                'description' => 'El usuario '.auth()->user()->name.' añadió el candidato.',
                'model_modified' => 'Candidate',
                'model_id' => $candidate->id,
            ]);

        } catch(\Illuminate\Database\QueryException $e){
            $code = $e->getCode();

            if($code == 23000){
                $message = "Ocurrió un error creando el candidato. Por favor verifique que no está repitiendo la lista para dos candidatos en una misma pregunta. Si el problema persiste, actualice la página.";
            }else{
                $message = "Ocurrió un error creando el candidato. Código de error: ".$e->getCode();
            }

        }
        return response()->json([
            'code' => $code,
            'message' => $message,
            'action' =>'crear candidato'
        ]);



    }
}
