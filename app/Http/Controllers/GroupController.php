<?php

namespace App\Http\Controllers;

use App\Action;
use App\Election;
use App\Elector;
use App\Group;
use App\Http\Requests\ElectorsFromElectionRequest;
use App\Http\Requests\GroupRequest;
use App\Jobs\ProcessGroupDoc;
use App\Jobs\SyncElectors;
use App\Rules\CountryCode;
use App\Rules\PhoneNumber;
use App\UserActivityLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Kfirba\QueryGenerator;
use AWS;

class GroupController extends Controller
{
    public function list(){

        //POLICIES
        $canListGroups = auth()->user()->can('view', [Group::class] );
        $canListAllGroups = auth()->user()->can('viewAny', [Group::class] );
        //END POLICIES

        if(! ($canListGroups || $canListAllGroups) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite listar los grupos de electores.")]);
        }

        $groups = Group::with('user')->orderBy('id','desc')->paginate(25);

        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de electores";
        $bread_1->route = route('groups.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Lista";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;

        //dd($elections);
        return view('groups.list', compact('groups', 'bread_arr') );
    }
    public function info($id){

        //POLICIES
        $canListThisGroup = auth()->user()->can('view', [Group::class] );
        //END POLICIES

        if(! $canListThisGroup ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver la lista de electores.")]);
        }

        $group = Group::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $electors = $group->electors();
        $electors_count = $electors->count();

        $electors = $electors->paginate(25);

        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de electores";
        $bread_1->route = route('groups.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Detalle de grupo de electores";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;
        //dd($electors);
        return view('groups.info', compact('electors', 'electors_count', 'group', 'bread_arr'));
    }

    public function filter(Request $request, Group $groups){

        //POLICIES
        $canListGroups = auth()->user()->can('view', [Group::class] );
        $canListAllGroups = auth()->user()->can('viewAny', [Group::class] );
        //END POLICIES

        if(! ($canListGroups || $canListAllGroups) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite listar los grupos de electores.")]);
        }

        //$company = $company->newQuery();
        $groups = $groups::with([]);

        $name = $request->has('name') ? $request->input('name'): null;
        $document_date = $request->has('document_date_filter') ? $request->input('document_date_filter'): null;


        // Search for a user based on their name.
        if ($document_date) {
            $groups->whereDate('created_at', $document_date);
        }

        // Search for a user based on their name.
        if ($name) {
            $groups->where('name','LIKE', '%'.$name.'%');
        }

        $groups = $groups->orderBy('id', 'desc')->paginate(25);

        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de electores";
        $bread_1->route = route('groups.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Filtro de búsqueda";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;

        //dd($movements);
        return view('groups.list', compact('groups', 'bread_arr') );

    }

    public function electorsFilter(Request $request, Elector $electors){

        //$company = $company->newQuery();
        $electors = $electors::with([]);

        $search_input = $request->has('name_search') ? $request->input('name_search'): null;
        $group_id_search = $request->has('group_id_search') ? $request->input('group_id_search'): null;

        //POLICIES
        $canListElectors = auth()->user()->can('viewAny', [Elector::class] );
        //END POLICIES

        if(! ($canListElectors) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite listar los electores.")]);
        }



        // Search for a user based on their name.
        if (!$group_id_search) {
            return back()->with('modalMessage',['Aviso', __("Hubo un error agregando el grupo de votación, por favor inténtelo de nuevo.")]);
        }

        $search_input_expanded = str_replace(" ", "%", $search_input);

        $group = Group::with([])
            ->where('id',$group_id_search)
            ->orderBy('id', 'desc')
            ->first();

        $electors = $group->electors();
        $electors_count = $electors->count();

        // Search for a user based on their name.
        if ($search_input) {
            $electors
                ->where(function($query) use ($search_input, $search_input_expanded){
                    $query
                        ->where('fathers_last_name','LIKE', '%'.$search_input.'%')
                        ->orWhere('mothers_last_name','LIKE', '%'.$search_input.'%')
                        ->orWhereRaw("concat(name, ' ', fathers_last_name, ' ', mothers_last_name) like '%$search_input_expanded%' ")
                        ->orWhere('name','LIKE', '%'.$search_input.'%')
                        ->orWhere('document_number', $search_input)
                        ->orWhere('email', $search_input);
                });
        }

        $electors =  $electors->paginate(25);

        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de electores";
        $bread_1->route = route('groups.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $group->name;
        $bread_2->route = route('groups.info',$group->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Filtro de electores";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;
        //dd($movements);
        return view('groups.info', compact('electors', 'group', 'electors_count', 'bread_arr'));

    }

    public function create(){

        //POLICIES
        $canCreateGroups = auth()->user()->can('create', [Group::class] );
        //END POLICIES

        if(! $canCreateGroups ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite agregar grupo de electoress.")]);
        }


        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Grupos de electores";
        $bread_1->route = route('groups.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Agregar grupo de electores";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;
        //dd($companies);
        return view('groups.create', compact('bread_arr'));
    }

    public function storeAnt(GroupRequest $groupRequest){
        //POLICIES
        $canCreateGroups = auth()->user()->can('create', [Group::class] );
        //END POLICIES

        if(! $canCreateGroups){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite añadir grupos de electores.")]);
        }
        try {
            $groupRequest->merge(['user_id' => auth()->user()->id ]);
            $group = Group::create($groupRequest->input());
        } catch(\Illuminate\Database\QueryException $e){
            if($e->getCode() == 23000){
                return back()->with('modalMessage',['Aviso', __("Ese nombre ya está registrado, por favor elija un nombre diferente.")]);
            }
            return back()->with('modalMessage',['Aviso', __("Hubo un error agregando el grupo de votación, por favor inténtelo de nuevo.")]);

        }

        try {
            if(!$groupRequest->hasFile('electors_list')){

                $group->delete();
                return back()->with('modalMessage',['Aviso',"Por favor seleccione al menos un archivo."]);
            }

            $files = $groupRequest->file('electors_list');
            // Get uploaded CSV file

            $document_number_arr = array();
            $data = array();

            foreach ($files as $file) {

                $fileOriginalName = $file->getClientOriginalName();
                $data_current_row = 0;

                //ARRAY TO RETRIEVE ALL THE IDS FROM DNI
                //SO THEN INSERT INTO PIVOT ELECTOR ELECTION

                $header = null;

                $delimiter = $this->getFileDelimiter($file);
                if (($csvFile = fopen($file, 'r')) !== false){
                    //SKIP THE FIRST LINE
                    fgetcsv($csvFile);
                    $now = Carbon::now('utc')->toDateTimeString();

                    while(($line = fgetcsv($csvFile,0,$delimiter)) !== FALSE) {
                        $data_current_row++;

                        $row_elector = $this->addCsvRowToArray($line, $now);
                        $row_document_number = $row_elector["document_number"];

                        //ADD ELECTORS ONCE, IF REPEATED SKIP
                        if (!in_array($row_document_number, $document_number_arr)){

                            if($messageFail = $this->validateElectorRow($row_elector, $data_current_row, $fileOriginalName)){
                                if ($messageFail == "stop"){
                                    //TERMINATE ADDING ELECTORS FROM CSV
                                    break;
                                }else{
                                    //BACK WITH MESSAGE
                                    $group->delete();
                                    return back()->with('modalMessage',['Aviso',$messageFail]);
                                }
                            }else{
                                $data[] = $row_elector;
                                $document_number_arr[] = $row_document_number;

                            }

                        }

                    }


                }
            }

            //dd($data);

            $excludedColumnsFromUpdate = ['created_at'];

            //SOLUTION FROM:
            //https://laracasts.com/discuss/channels/eloquent/performant-way-to-mass-update-or-create
            //https://github.com/kfirba/import-query-generator
            //INSERTING OR UPDATING THE DATA
            $table = 'electors';

            $queryObject = (new QueryGenerator)->generate($table, $data, $excludedColumnsFromUpdate);
            $query = \DB::statement($queryObject->getQuery(), $queryObject->getBindings());

            //INSERTING ELECTORS INTO PIVOT DATA WITH GROUPS
            $electors_for_election = Elector::whereIn('document_number', $document_number_arr)->get();
            $group->electors()->sync($electors_for_election);


            UserActivityLog::create([
                'user_id' => auth()->user()->id,
                'action_id' => Action::GROUP_ADDED,
                'description' => 'El usuario '.auth()->user()->name.' añadió el grupo de electores.',
                'model_modified' => 'Group',
                'model_id' => $group->id,
            ]);
            $msg = "Se registraron correctamente ".count($data)." electores en el grupo de electores.";

            return redirect()->route('groups.info', $group->id)->with('modalMessage',['Aviso', $msg]);

        } catch(\Illuminate\Database\QueryException $e){
            $group->delete();
            $message= "Ocurrió un error al ingresar los datos, por favor verifique que los datos de la tabla son correctos. Código de error: ";
            return back()->with('modalMessage',['Aviso', $message.$e->getCode()]);

        }


    }

    public function store(GroupRequest $groupRequest){
        //POLICIES
        $canCreateGroups = auth()->user()->can('create', [Group::class] );
        //END POLICIES

        if(! $canCreateGroups){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite añadir grupos de electores.")]);
        }
        try {
            $groupRequest->merge(['user_id' => auth()->user()->id ]);
            $group = Group::create($groupRequest->input());
        } catch(\Illuminate\Database\QueryException $e){
            if($e->getCode() == 23000){
                return back()->with('modalMessage',['Aviso', __("Ese nombre ya está registrado, por favor elija un nombre diferente.")]);
            }
            return back()->with('modalMessage',['Aviso', __("Hubo un error agregando el grupo de votación, por favor inténtelo de nuevo.")]);

        }

        try {
            if(!$groupRequest->hasFile('electors_list')){

                $group->delete();
                return back()->with('modalMessage',['Aviso',"Por favor seleccione al menos un archivo."]);
            }

            $files = $groupRequest->file('electors_list');
            // Get uploaded CSV file

            $document_number_arr = array();
            $data = array();
            $filenames_arr = array();

            $files_data = array();

            foreach ($files as $file) {
                $new_filename = Str::random(22).".".$file->getClientOriginalExtension();
                $fileOriginalName = $file->getClientOriginalName();

                $files_data[] = [
                    'filename' => $new_filename,
                    'file_original_name' => $fileOriginalName,
                ];

                //Move Uploaded File
                //To S3 if productions
                //locally if local
                $environment_status = \config('app.env');

                if($environment_status=="local"){
                    $destinationPath = storage_path("app/public/group_docs/");
                    $file->move($destinationPath,$new_filename);
                }elseif($environment_status=="production"){
                    $path = \Storage::disk('s3')->putFileAs('app/public/group_docs/', $file, $new_filename);
                }

            }

            //USER ACTION
            UserActivityLog::create([
                'user_id' => auth()->user()->id,
                'action_id' => Action::GROUP_ADDED,
                'description' => 'El usuario '.auth()->user()->name.' añadió el grupo de electores.',
                'model_modified' => 'Group',
                'model_id' => $group->id,
            ]);
            //JOB ACTION
            $jobActivityLog = UserActivityLog::create([
                'user_id' => auth()->user()->id,
                'action_id' => Action::ADDING_ELECTORS_TO_GROUP,
                'description' => '0',
                'model_modified' => 'Group',
                'model_id' => $group->id,
            ]);


            ProcessGroupDoc::dispatch($group, $jobActivityLog,$files_data)->onQueue('high');


            return redirect()->route('groups.list')->with('groupMessage',[$group->id,$jobActivityLog->id]);

        } catch(\Throwable $e){
            dd($e);
            $group->delete();
            $message= "Ocurrió un error al ingresar los datos, por favor verifique que los datos de la tabla son correctos. Código de error: ";
            return back()->with('modalMessage',['Aviso', $message.$e->getCode()]);

        }


    }

    public function processFile(Group $group, UserActivityLog $jobActivityLog, $files_data){

        $data_global_count = 0;
        $environment_status = \config('app.env');

        try {
            // Get uploaded CSV file
            $document_number_arr = array();
            $data = array();
            $weight_arr = array();
            //$filenames_arr = [ 'pe.csv', 'pa.csv'];

            foreach ($files_data as $file_data) {

                if($environment_status=="local"){
                    $file_path = storage_path("app/public/group_docs/" . $file_data["filename"]);
                }elseif($environment_status=="production"){
                    $bucket = env('AWS_BUCKET');
                    $client_s3 = AWS::createClient('s3');
                    $client_s3->registerStreamWrapper();
                    $file_path = "s3://{$bucket}/app/public/group_docs/". $file_data["filename"];
                }

                //$file_path = storage_path("app/public/group_docs/" . $file_data["filename"]);

                $file = fopen($file_path, "r");

                $data_current_row = 0;

                //ARRAY TO RETRIEVE ALL THE IDS FROM DNI
                //SO THEN INSERT INTO PIVOT ELECTOR ELECTION

                $header = null;

                $delimiter = $this->getFileDelimiterFromStoredCSV($file_path);

                if ($file !== false) {
                    //SKIP THE FIRST LINE
                    fgetcsv($file);
                    $now = Carbon::now('utc')->toDateTimeString();

                    while (($line = fgetcsv($file, 0, $delimiter)) !== FALSE) {
                        $data_current_row++;

                        $row_elector = $this->addCsvRowToArray($line, $now);
                        //GET WEIGHT, IF NOT SET IS 1
                        $weight_elector = isset($line[5]) ? utf8_encode($line[5]) : 1;

                        $row_document_number = $row_elector["document_number"];

                        //ADD ELECTORS ONCE, IF REPEATED SKIP
                        if (!in_array($row_document_number, $document_number_arr)) {

                            if ($messageFail = $this->validateElectorRow($row_elector, $data_current_row, $file_data["file_original_name"])) {
                                if ($messageFail == "stop") {
                                    //TERMINATE ADDING ELECTORS FROM CSV
                                    break;
                                } else {
                                    //BACK WITH MESSAGE

                                    //JOB ACTION
                                    $jobActivityLog->update([
                                        'action_id'=> Action::ERROR_ADDING_ELECTORS_TO_GROUP,
                                        'description' => $messageFail
                                    ]);
                                    $group->delete();

                                    fclose($file);
                                    unlink($file_path);
                                    return false;
                                }
                            } else {
                                $data_global_count++;

                                $data[] = $row_elector;
                                $document_number_arr[] = $row_document_number;
                                $weight_arr[$row_document_number] = $weight_elector;

                                //JOB ACTION
                                $jobActivityLog->update([
                                    'description'=> $data_global_count,
                                ]);

                            }

                        }

                    }


                }

                fclose($file);
                unlink($file_path);
            }

            $jobActivityLog->update(['action_id'=> Action::SYNCING_ELECTORS_TO_GROUP]);

            $excludedColumnsFromUpdate = ['created_at'];

            //$jobActivityLog->update(['description'=> "aaaaa"]);
            //SOLUTION FROM:
            //https://laracasts.com/discuss/channels/eloquent/performant-way-to-mass-update-or-create
            //https://github.com/kfirba/import-query-generator
            //INSERTING OR UPDATING THE DATA
            $table = 'electors';

            //FOR LARGE AMOUNT OF ELECTORS
            $data_chunked_arr = array_chunk($data,1000);

            foreach ($data_chunked_arr as $key=>$data_chunked) {
                $queryObject = (new QueryGenerator)->generate($table, $data_chunked, $excludedColumnsFromUpdate);
                $query = \DB::statement($queryObject->getQuery(), $queryObject->getBindings());
            }
            //INSERTING ELECTORS INTO PIVOT DATA WITH GROUPS
            //FROM: https://stackoverflow.com/questions/27230672/laravel-sync-how-to-sync-an-array-and-also-pass-additional-pivot-fields

            $electors_for_election = Elector::whereIn('document_number', $document_number_arr)->get();
            $electors_id_array = array();

            foreach ($electors_for_election as $elector_for_election) {
                $elector_id = $elector_for_election->id;
                $elector_document_number = $elector_for_election->document_number;
                $weight_elector = $weight_arr[$elector_document_number];
                $electors_id_array[$elector_id] = ['weight' => $weight_elector];
            }

            //FOR LARGE ELECTOR GROUPS
            //SyncElectors::dispatch($group, $electors_id_array_chunked)->onQueue('high');
            $group->electors()->sync($electors_id_array);

            //JOB ACTION
            $jobActivityLog->update([
                'action_id'=> Action::ELECTORS_ADDED_TO_GROUP,
            ]);

            return true;

        } catch(\Throwable $e){
            $message= "Ocurrió un error al ingresar los datos, por favor intente de nuevo o verifique que los datos ingresados son correctos. Error: ".substr($e->getMessage(), 0, 100);


            //JOB ACTION
            $jobActivityLog->update([
                'action_id'=> Action::ERROR_ADDING_ELECTORS_TO_GROUP,
                'description' => $message
            ]);
            $group->delete();

            return false;

        }


    }



    public function uploadStatusAjax(Request $request){

        //POLICIES
        $canListElectors = auth()->user()->can('viewAny', [Elector::class] );
        //END POLICIES

        if(! $canListElectors ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver la lista de electores.")]);
        }

        $user_activity_log = $request->has('user_activity_log') ? $request->input('user_activity_log'): null;

        $message = "";
        $status = "normal";
        $action = "continue";

        if(!$user_activity_log){
            $message = "Ocurrió un error actualizando el estado del grupo de electores, por favor entre en el grupo de electores y verifique que se han subido correctamente.";
            $status = "error";
            $action = "break";
            return response()->json([
                'message' => $message,
                'status' => $status,
                'action' => $action,
            ]);
        }

        $user_activity_log = UserActivityLog::where('id',$user_activity_log)->first();

        if ($user_activity_log->action_id == Action::ADDING_ELECTORS_TO_GROUP){
            $message.= "Electores subidos: ".$user_activity_log->description.".<br>";
            $message.= "Los electores se están creando, por favor no cierre esta ventana o la página hasta que terminen de cargar al sistema.";
        }else if($user_activity_log->action_id == Action::ELECTORS_ADDED_TO_GROUP){
            $message.= "Se subieron con éxito ".$user_activity_log->description." electores.";
            $action = "break";
        }else if($user_activity_log->action_id == Action::ERROR_ADDING_ELECTORS_TO_GROUP){
            $message = $user_activity_log->description;
            $status = "error";
            $action = "break";
        }else if($user_activity_log->action_id == Action::SYNCING_ELECTORS_TO_GROUP){
            $group = Group::where('id', $user_activity_log->model_id)->first();
            $group_electors = $group->electors()->count();

            $message.= "Electores sincronizados: ".$group_electors." de ".$user_activity_log->description.".<br>";
            $message.= "Los electores se están sincronizando, por favor no cierre esta ventana o la página hasta que terminen de cargar al sistema.";
        }

        return response()->json([
            'message' => $message,
            'status' => $status,
            'action' => $action,
        ]);
    }

    public function addCsvRowToArray($line, $now){
        $fathers_last_name  = isset($line[0]) ? utf8_encode($line[0]) : null;
        $mothers_last_name  = isset($line[1]) ? utf8_encode($line[1]) : null;
        $name               = isset($line[2]) ? utf8_encode($line[2]) : null;
        $document_number    = isset($line[3]) ? utf8_encode($line[3]) : null;
        $mail               = isset($line[4]) ? utf8_encode($line[4]) : null;
        $country_code       = isset($line[6]) ? utf8_encode($line[6]) : null;
        $cellphone          = isset($line[7]) ? utf8_encode($line[7]) : null;

        $row_elector = [
            "name" => $name,
            "fathers_last_name" => $fathers_last_name,
            "mothers_last_name" => $mothers_last_name,
            "email" => $mail,
            "document_number" => $document_number,
            "country_code" => $country_code,
            "cellphone" => $cellphone,
            'created_at'=> $now,
            'updated_at'=> $now
        ];

        return $row_elector;
    }

    public function validateElectorRow($row_elector, $data_current_row, $filename){

        $message = null;
        $validator = \Validator::make($row_elector, [
            'name' => 'required|min:2',
            'fathers_last_name' => 'required|min:2',
            'mothers_last_name' => 'required|min:2',
            'email' => 'required|email',
            'document_number' => 'required|min:5',
            'weight' => 'nullable|numeric',
            //CELLPHONE STARTS WITH 9 FOLLOWED BY 8 NUMBERS: 9########
            'country_code' => ['nullable', new CountryCode()],
            'cellphone' => ['nullable', new PhoneNumber()],
        ]);
        if ($validator->fails()){

            //IF NOT EMPTY
            if ($row_elector["fathers_last_name"] != ""){
                $message="Hubo un error al ingresar la lista del archivo ".$filename." en la fila ".($data_current_row + 1).":";
                foreach ($validator->messages()->getMessages() as $field_name => $messages){
                    $message.=("<br>-".$messages[0].": ".$row_elector[$field_name]);
                }
            }else{
                $message = "stop";
            }
        }

        return $message;
    }



}
