<?php

namespace App\Http\Controllers;

use App\Action;
use App\Candidate;
use App\Election;
use App\Http\Requests\QuestionRequest;
use App\Poll;
use App\Question;
use App\UserActivityLog;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function create(){
        $election = new Question();

        //POLICIES
        $canCreateElections = true;
        //END POLICIES

        if(! $canCreateElections){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite crear elecciones.")]);
        }

        $btnText = __("Crear elección");

        return view('elections.create', compact('election','btnText'));
    }

    public function store(QuestionRequest $questionRequest){
        $election_id = $questionRequest->input('election_id');

        $poll = Poll::whereHas("elections", function ($q) use ($election_id) {
            $q->where("id", $election_id);
        })->first();

        //POLICIES
        $canCreateQuestions = auth()->user()->can('create', [Question::class] );
        $canCreateQuestionsAnymore = auth()->user()->can('updateAnymore', [Poll::class, $poll] );

        //END POLICIES

        if(! $canCreateQuestions){
            $code = 403;
            $message = "Tu rol no te permite crear preguntas.";
            return response()->json([
                'code' => $code,
                'message' => $message,
                'action' =>'crear pregunta'
            ]);
        }
        if(! $canCreateQuestionsAnymore){
            $code = 403;
            $message = "Ya no puedes crear preguntas, porque la votación ya comenzó.";
            return response()->json([
                'code' => $code,
                'message' => $message,
                'action' =>'crear pregunta'
            ]);
        }



        try {
            $question = Question::create($questionRequest->input());

            UserActivityLog::create([
                'user_id' => auth()->user()->id,
                'action_id' => Action::QUESTION_ADDED,
                'description' => 'El usuario '.auth()->user()->name.' añadió la pregunta.',
                'model_modified' => 'Question',
                'model_id' => $question->id,
            ]);

            Candidate::create([
                "name" => "Voto en blanco",
                "order" => 250,
                "list_number" => 250,
                "list_name" => "Voto en blanco",
                "question_id" => $question->id,
            ]);
            Candidate::create([
                "name" => "Voto viciado",
                "order" => 251,
                "list_number" =>251,
                "list_name" => "Voto viciado",
                "question_id" => $question->id,
            ]);
            $code = 200;
            $message = "Se creó la pregunta.";

        } catch(\Illuminate\Database\QueryException $e){
            $code = 500;
            $message = "Ocurrió un error creando la pregunta. Código de error: ".$e->getCode();

        }
        return response()->json([
            'code' => $code,
            'message' => $message,
            'action' =>'crear pregunta'
        ]);



    }
}
