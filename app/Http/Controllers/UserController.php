<?php

namespace App\Http\Controllers;

use App\Action;
use App\EmailActivityLog;
use App\Http\Requests\UserRequest;
use App\Mail\NewUserMail;
use App\User;
use App\UserActivityLog;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function list(){

        //POLICIES
        $canListUsers = auth()->user()->can('viewAny', [User::class] );
        //END POLICIES

        if(! ($canListUsers) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite listar los usuarios.")]);
        }

        $users = User::
        orderBy('id','desc')
            ->paginate(25);


        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Usuarios";
        $bread_1->route = route('users.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Lista";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;

        //dd($elections);
        return view('users.list', compact('users', 'bread_arr') );
    }

    public function info($id){

        //POLICIES
        $canListThisUser = auth()->user()->can('view', [User::class] );
        //END POLICIES

        if(! $canListThisUser ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver este usuario.")]);
        }

        $user = User::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();



        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Usuarios";
        $bread_1->route = route('users.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Detalle de usuario";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;
        //dd($companies);
        return view('users.info', compact('user', 'bread_arr'));
    }

    public function filter(Request $request, User $users){

        //POLICIES
        $canListUsers = auth()->user()->can('viewAny', [User::class] );
        //END POLICIES

        if(! ($canListUsers) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite listar los usuarios.")]);
        }

        //$company = $company->newQuery();
        $users = $users::with([
        ]);

        $search_input = $request->has('name_search') ? $request->input('name_search'): null;

        $search_input_expanded = str_replace(" ", "%", $search_input);

        // Search for a user based on their name.
        if ($search_input) {
            $users
                ->where(function($query) use ($search_input, $search_input_expanded){
                    $query
                        ->where('fathers_last_name','LIKE', '%'.$search_input.'%')
                        ->orWhere('mothers_last_name','LIKE', '%'.$search_input.'%')
                        ->orWhereRaw("concat(name, ' ', fathers_last_name, ' ', mothers_last_name) like '%$search_input_expanded%' ")
                        ->orWhere('name','LIKE', '%'.$search_input.'%')
                        ->orWhere('document_number', $search_input)
                        ->orWhere('email', $search_input);
                });
        }

        $users =  $users->orderBy('id', 'desc')->paginate(25);



        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Usuarios";
        $bread_1->route = route('users.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Filtro de búsqueda";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;
        //dd($movements);
        return view('users.list', compact('users', 'bread_arr'));

    }

    public function userActivityLog($id){

        //POLICIES
        $canListThisUserActivityLog = true;
        //END POLICIES

        if(! $canListThisUserActivityLog ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver las actividades del usuario.")]);
        }

        $userActivityLog = User::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //dd($companies);
        return view('users.userActivityLog', compact('userActivityLog'));
    }

    public function create(){
        $user = new User();

        //POLICIES
        $canCreateUsers = auth()->user()->can('create', [User::class] );
        //END POLICIES

        if(! $canCreateUsers){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite crear usuarios.")]);
        }

        $btnText = __("Crear Usuario");

        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Usuarios";
        $bread_1->route = route('users.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Agregar usuario";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;

        return view('users.create', compact('user','btnText', 'bread_arr'));
    }

    public function store(UserRequest $userRequest){

        //POLICIES
        $canCreateUsers = auth()->user()->can('create', [User::class] );
        //END POLICIES

        if(! $canCreateUsers){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite crear usuarios.")]);
        }

        $token_mail = Str::random(22);

        $userRequest->merge(['password' => $token_mail ]);
        $userRequest->merge(['token_mail' => $token_mail ]);
        $userRequest->merge(['state' => User::FOR_ACTIVATION ]);
        $userRequest->merge(['user_id' => auth()->user()->id ]);

        try {
            $user = User::create($userRequest->input());

            \Mail::to($user)->send(new NewUserMail($user));

            $mail_local_log = EmailActivityLog::create([
                'email' => $user->email,
                'action_id' => Action::USER_ADDED,
                'description' => 'correo enviado a '.$user->name." para confirmar usuario y generar contraseña.",
                'state' => EmailActivityLog::RECEIVED,
                'model_received' => EmailActivityLog::MODEL_USER,
                'model_id' => $user->id,
                'sent_at' => now(),
            ]);


            UserActivityLog::create([
                'user_id' => auth()->user()->id,
                'action_id' => Action::USER_ADDED,
                'description' => 'El usuario '.auth()->user()->name.' añadió el usuario.',
                'model_modified' => 'User',
                'model_id' => $user->id,
            ]);

            return back()->with('modalMessage',['Aviso',
                __("Se agregó correctamente el usuario.")]);
        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('modalMessage',['Aviso',
                __("Hubo un error agregando el usuario,
                por favor verifique que está colocando los datos requeridos.")]);

        }
    }

    public function edit($id){

        $user = User::with([
            'role',
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //POLICIES
        $canEditUsers = auth()->user()->can('update', [User::class, $user] );
        //END POLICIES

        if(! ($canEditUsers) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite editar este usuario.")]);
        }

        $btnText = __("Actualizar usuario");


        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Usuarios";
        $bread_1->route = route('users.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "active";
        $bread_2->text = "Editar usuario";
        $bread_2->route = "#";
        $bread_arr[]=$bread_2;
        //dd($accounting);
        return view('users.create', compact('user','btnText', 'bread_arr'));
    }

    public function update(UserRequest $userRequest, $id){
        $user = User::where('id',$id)->first();

        //POLICIES
        $canEditUsers = auth()->user()->can('update', [User::class, $user] );
        //END POLICIES

        if(! ($canEditUsers) ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite editar este usuario.")]);
        }

        if ($user->state == User::FOR_ACTIVATION){
            $userRequest->merge(['state' => User::FOR_ACTIVATION ]);
        }

        try {
            //$user->update($userRequest->input());

            $user->update([
                //USER
                'role_id' => $userRequest->role_id,
                'name' => $userRequest->name,
                'fathers_last_name' => $userRequest->fathers_last_name,
                'mothers_last_name' => $userRequest->mothers_last_name,
                'document_number' => $userRequest->document_number,
                'cellphone' => $userRequest->cellphone,
                'state' => $userRequest->state,
            ]);

            UserActivityLog::create([
                'user_id' => auth()->user()->id,
                'action_id' => Action::USER_MODIFIED,
                'description' => 'El usuario '.auth()->user()->name.' modificó el usuario.',
                'model_modified' => 'User',
                'model_id' => $user->id,
            ]);

            return back()->with('modalMessage',['Aviso',
                __("Se actualizó correctamente el usuario.")]);

        } catch(\Illuminate\Database\QueryException $e){
            return back()->with('modalMessage',['Aviso',
                __("Hubo un error actualizando el usuario,
                por favor verifique que está colocando los datos requeridos.")]);

        }

        //dd($customerRequest);
    }

    public function confirmMail($user_id, $token_mail){
        auth()->logout();

        return view('users.setInitialPassword', compact('user_id', 'token_mail'));

        //dd($electors);
    }

    public function storeInitialPassword($user_id, $token_mail,Request $request){

        $user_id_decrypted = \Crypt::decryptString($user_id);
        $token_mail_decrypted = \Crypt::decryptString($token_mail);

        $user = User::where('id',$user_id_decrypted)->first();

        $message="Se estableció correctamente su contraseña.";



        //Check if passwords matches the rules
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'password_confirmation' => 'required|min:6|same:password',
        ]);
        if ($validator->fails()){
            $message="No se pudo crear su contraseña:";
            foreach ($validator->messages()->getMessages() as $field_name => $messages){
                $message.=("<br>-".$messages[0]);
            }
            return back()->with('modalMessage',['Aviso',$message]);
        }

        //Check if the mail is already confirmed
        if($user->email_verified_at){
            $message="El usuario ya ha sido verificado. Si no recuerda su contraseña";
            return redirect('login')->with('modalMessage',['Aviso',$message]);
        }

        //Check if the token given is different than the user token
        if($user->token_mail != $token_mail_decrypted){
            $message="No se puede confirmar el correo.";
            return redirect('login')->with('modalMessage',['Aviso',$message]);
        }

        $user->update([
            'state'=> User::ACTIVE,
            'email_verified_at' => now(),
            'password' => $request->password,
            'token_mail'=> ''
        ]);


        UserActivityLog::create([
            'user_id' => $user->id,
            'action_id' => Action::PASSWORD_SET,
            'description' => 'El usuario '.$user->name.' confirmó su correo y estableció su contraseña .',
            'model_modified' => 'User',
            'model_id' => $user->id,
        ]);

        return redirect('login')->with('modalMessage',['Aviso',$message]);

        //dd($electors);
    }


}
