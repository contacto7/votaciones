<?php

namespace App\Http\Controllers\Auth;

use App\Action;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\UserActivityLog;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected function resetPassword($user, $password)
    {
        $user->password = $password;

        $user->setRememberToken(\Str::random(60));

        $user->save();

        event(new PasswordReset($user));


        UserActivityLog::create([
            'user_id' => $user->id,
            'action_id' => Action::PASSWORD_CHANGE,
            'description' => 'El usuario '.$user->name.' cambió su contraseña.',
            'model_modified' => 'User',
            'model_id' => $user->id,
        ]);

        $this->guard()->login($user);
    }

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
}
