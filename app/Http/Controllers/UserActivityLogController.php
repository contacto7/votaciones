<?php

namespace App\Http\Controllers;

use App\Action;
use App\Elector;
use App\User;
use App\UserActivityLog;
use Illuminate\Http\Request;

class UserActivityLogController extends Controller
{
    public function list($id){

        //POLICIES
        $canListUserActivityLog = auth()->user()->can('audit', [User::class] );
        //END POLICIES

        if(! $canListUserActivityLog ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver los registros de usuarios.")]);
        }

        $userActivityLogs = UserActivityLog::with([
            'action'
        ])
            ->where('user_id',$id)
            ->orderBy('id', 'desc')
            ->paginate(25);


        $user = User::where('id',$id)->first();

        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Usuarios";
        $bread_1->route = route('users.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $user->name;
        $bread_2->route = route('users.info',$user->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Registro de actividades";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;

        return view('userActivityLogs.list', compact('userActivityLogs', 'user' ,'bread_arr'));
    }

    public function filter(Request $request, UserActivityLog $userActivityLogs){

        //POLICIES
        $canListUserActivityLog = auth()->user()->can('audit', [User::class] );
        //END POLICIES

        if(! $canListUserActivityLog ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver los registros de usuarios.")]);
        }

        $userActivityLogs = $userActivityLogs::with([]);

        $user_id = $request->has('user_id') ? $request->input('user_id'): null;
        $action_id_search = $request->has('action_id_search') ? $request->input('action_id_search'): null;

        // Search for a user based on their name.
        if (!$user_id) {
            return back()->with('modalMessage',['Aviso', "Ocurrió un error. Por favor ingrese de nuevo al registro del usuario."]);
        }else{
            $user = User::where('id',$user_id)->first();
        }

        // Search for a user based on their name.
        if ($action_id_search) {
            $userActivityLogs = $userActivityLogs->where('action_id', $action_id_search);
        }


        $userActivityLogs =
            $userActivityLogs
                ->with([
                    'action'
                ])
                ->where('user_id',$user_id)
                ->orderBy('id', 'desc')
                ->paginate(25);


        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Usuarios";
        $bread_1->route = route('users.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $user->name;
        $bread_2->route = route('users.info',$user->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Filtro de Registro de actividades";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;
        //dd($movements);
        return view('userActivityLogs.list', compact('userActivityLogs', 'user', 'bread_arr'));

    }

    public function listUpdatesToUser($id){

        //POLICIES
        $canListUserActivityLog = auth()->user()->can('audit', [User::class] );
        //END POLICIES

        if(! $canListUserActivityLog ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver los registros de usuarios.")]);
        }

        $userActivityLogs = UserActivityLog::with([
            'action'
        ])
            ->where('model_modified','User')
            ->where('model_id',$id)
            ->orderBy('id', 'desc')
            ->paginate(25);


        $user = User::where('id',$id)->first();


        $bread_arr = array();
        $bread_1 = new \stdClass();
        $bread_1->type = "link";
        $bread_1->text = "Usuarios";
        $bread_1->route = route('users.list');
        $bread_arr[]=$bread_1;
        $bread_2 = new \stdClass();
        $bread_2->type = "link";
        $bread_2->text = $user->name;
        $bread_2->route = route('users.info',$user->id);
        $bread_arr[]=$bread_2;
        $bread_3 = new \stdClass();
        $bread_3->type = "active";
        $bread_3->text = "Registro de modificaciones a usuario";
        $bread_3->route = "#";
        $bread_arr[]=$bread_3;

        return view('userActivityLogs.listUpdatesToUser', compact('userActivityLogs', 'user', 'bread_arr'));
    }

    public function listUpdatesToElector($id){

        //POLICIES
        $canListUpdatesToElector = auth()->user()->can('audit', [User::class] );
        //END POLICIES

        if(! $canListUpdatesToElector ){
            return redirect('/')->with('modalMessage',['Aviso', __("Su rol no le permite ver los registros de electores.")]);
        }

        $userActivityLogs = UserActivityLog::with([
            'action'
        ])
            ->where('model_modified','Elector')
            ->where('model_id',$id)
            ->orderBy('id', 'desc')
            ->paginate(25);


        $elector = Elector::where('id',$id)->first();

        return view('userActivityLogs.listUpdatesToElector', compact('userActivityLogs', 'elector'));
    }



}
