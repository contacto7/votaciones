<?php

namespace App\Http\Middleware;

use Closure;

class CheckVoteSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $elector = \Session::get('elector');
        if (!isset($elector)) {
            $error_code = 'err-not-ses';
            return redirect()->route('votes.error', compact('error_code'));
        }
        if ($elector['expires_at'] < now() ){
            $error_code = 'err-tim-exp';
            return redirect()->route('votes.error', compact('error_code'));
        }

        return $next($request);
    }
}
