<?php

namespace App\Http\Middleware;

use Closure;
use Browser;

class AcceptedBrowsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowedBrowser = Browser::isChrome();

        if(!$allowedBrowser){
            return redirect()->route('errors.invalidBrowser');
        }

        return $next($request);
    }
}
