<?php

namespace App\Http\Middleware;

use App\ElectorPoll;
use App\Http\Controllers\Controller;
use App\Poll;
use Closure;

class DeviceValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //DEVIDE UUID
        $devide_uuid = $request->input('device_uuid');
        //IP DEVICE
        $controller = new Controller();
        $ip_registered = $controller->getIp();
        //POLL ID
        $sep = \App\Vote::DATA_SEPARATOR;
        $poll_id_elector_id_token_mail = $request->route('poll_id_elector_id_token_mail');
        $poll_id_elector_id_token_mail_decrypted = \Crypt::decryptString($poll_id_elector_id_token_mail);
        $poll_id = explode($sep, $poll_id_elector_id_token_mail_decrypted)[0];

        $poll = Poll::where('id',$poll_id)->first();
        $poll_electors= $poll->electors();
        $poll_electors_same_device = $poll_electors->where('device_uuid',$devide_uuid);
        $poll_electors_same_device_and_ip = $poll_electors_same_device->where('ip_registered',$ip_registered);
        $poll_electors_same_device_and_ip_count = $poll_electors_same_device_and_ip->count();

        if($poll_electors_same_device_and_ip_count > 0){
            return redirect()->route('errors.forbiddenDevice');
        }

        return $next($request);
    }
}
