<?php

namespace App\Http\Middleware;

use Closure;

class PenalizationTimeValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //POLL ID
        $sep = \App\Vote::DATA_SEPARATOR;
        $poll_id_elector_id_token_mail = $request->route('poll_id_elector_id_token_mail');

        $error_validating = \Session::get('error_validating');
        $secondsToVoteAgain = 0;
        if ($error_validating){
            $new_try_at = $error_validating['new_try_at'];
            $secondsToVoteAgain = now()->diffInSeconds($new_try_at, false);
        }

        if ($secondsToVoteAgain > 0){
            return redirect()
                ->route('votes.insertDni', ['poll_id_elector_id_token_mail' => $poll_id_elector_id_token_mail])
                ->with('modalMessage',['Aviso', __("Aún no puede accesar a la votación")]);
        }

        return $next($request);
    }
}
