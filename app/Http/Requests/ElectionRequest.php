<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ElectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
            case 'POST':
                return [
                    //USER
                    'name' => 'required|min:3',
                    'poll_id' => [
                        'required',
                        Rule::exists('polls','id')
                    ],
                    'group_id' => [
                        'required',
                        Rule::exists('groups','id')
                    ],
                ];
        }
    }
}
