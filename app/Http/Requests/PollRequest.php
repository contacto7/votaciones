<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PollRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
            case 'POST':
                return [
                    //USER
                    'name' => 'required|min:3',
                    'start_date' => 'required|date_format:Y-m-d H:i',
                    'end_date' => 'required|date_format:Y-m-d H:i',
                    'auth_factors' => 'required|between:1,2',
                    'visibility' => 'required|between:1,2',
                ];
        }
    }
}
