<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
                return [
                    //USER
                    'role_id' => [
                        'required',
                        Rule::exists('roles','id')
                    ],
                    'name' => 'required|min:2',
                    'fathers_last_name' => 'required|min:2',
                    'mothers_last_name' => 'required|min:2',
                    'document_number' => 'required|min:5',
                    'cellphone' => 'required|min:3',
                    'state' => 'numeric|min:1|max:2',
                ];
            case 'POST':
                return [
                    //USER
                    'role_id' => [
                        'required',
                        Rule::exists('roles','id')
                    ],
                    'name' => 'required|min:2',
                    'fathers_last_name' => 'required|min:2',
                    'mothers_last_name' => 'required|min:2',
                    'document_number' => 'required|min:5',
                    'cellphone' => 'required|min:3',
                    'email' => 'required|email',
                ];
        }
    }
}
