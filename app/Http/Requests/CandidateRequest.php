<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CandidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
            case 'POST':
                return [
                    //USER
                    'name' => 'required|min:3',
                    'order' => 'required|numeric',
                    'list_number' => 'required|numeric',
                    'list_name' => 'required|min:1',
                    'question_id' => [
                        'required',
                        Rule::exists('questions','id')
                    ],
                ];
        }
    }
}
