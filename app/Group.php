<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Group
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Elector[] $electors
 * @property-read int|null $electors_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Group newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Group newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Group query()
 * @mixin \Eloquent
 */
class Group extends Model
{
    protected $fillable = [
        'name', 'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function electors(){
        return $this
            ->belongsToMany(Elector::class)
            ->withPivot('weight')
            ->withTimestamps();
    }
}
