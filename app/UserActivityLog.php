<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UserActivityLog
 *
 * @property-read \App\Action $action
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserActivityLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserActivityLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserActivityLog query()
 * @mixin \Eloquent
 */
class UserActivityLog extends Model
{
    protected $fillable = [
        'user_id', 'action_id', 'description', 'model_modified', 'model_id',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function action(){
        return $this->belongsTo(Action::class);
    }
    public function getModel(){
        $model = $this->model_modified;
        $className = 'App\\' . $model;
        $model = new $className;
        $model = $model->where('id', $this->model_id)->first();
        return $model;
    }

}
