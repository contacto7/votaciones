<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Role
 *
 * @property int $id
 * @property string $name nombre del rol del usuario
 * @property string $description descripción del rol del usuario
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereName($value)
 * @mixin \Eloquent
 * @property string $slug enlace para nombre de usuario
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereSlug($value)
 */
class Role extends Model
{
    const DEVELOPER = 1;
    const ADMIN = 2;
    const ELECTION_CREATOR = 3;
    const AUDITOR = 4;
    const STAFF = 5;


    public $timestamps = false;

    public function users(){
        return $this->hasMany(User::class);
    }
}
