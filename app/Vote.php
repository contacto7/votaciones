<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Vote
 *
 * @property int $id
 * @property int $candidate_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Candidate $candidate
 * @method static \Illuminate\Database\Eloquent\Builder|Vote newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vote newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vote query()
 * @method static \Illuminate\Database\Eloquent\Builder|Vote whereCandidateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vote whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vote whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vote whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Vote extends Model
{
    const DATA_SEPARATOR = "-";
    const MAX_ATTEMPTS = 3;
    const API_TOKEN = "eca4c5903408636a68a6b1194cb9901197814511033dbb8309006a50598d6886";

    const ERRORES = [
        'err-not-ses' => 'No se puede obtener sus datos, por favor ingrese de nuevo desde el link del correo.',
        'err-tim-exp' => 'Su tiempo de votación expiró, por favor ingrese de nuevo desde el link del correo.',
    ];

    protected $fillable = [
        'candidate_id','weight',
    ];

    public function candidate(){
        return $this->belongsTo(Candidate::class);
    }
}
