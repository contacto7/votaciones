<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\EmailActivityLog
 *
 * @property-read \App\Action $action
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|EmailActivityLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailActivityLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailActivityLog query()
 * @mixin \Eloquent
 */
class EmailActivityLog extends Model
{
    const RECEIVED = 1;
    const ERROR = 2;

    const MODEL_USER = 'User';
    const MODEL_ELECTOR = 'Elector';

    protected $fillable = [
        'email', 'action_id', 'description', 'state', 'model_received','model_id','sent_at','poll_id',
    ];


    public function user(){
        return $this->belongsTo(User::class);
    }
    public function action(){
        return $this->belongsTo(Action::class);
    }
    public function electorReceived(){
        return $this->belongsTo(Elector::class, 'model_id');
    }
}
